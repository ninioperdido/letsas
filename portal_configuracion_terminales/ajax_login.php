<?php
// Copyright (C) 2007                                                                                                                                                               
//                                                                                                                                                                                  
// This program is free software; you can redistribute it and/or modify                                                                                                             
// it under the terms of the GNU General Public License as published                                                                                                                
// by the Free Software Foundation; version 2 only.                                                                                                                                 
//                                                                                                                                                                                  
// This program is distributed in the hope that it will be useful,                                                                                                                  
// but WITHOUT ANY WARRANTY; without even the implied warranty of                                                                                                                   
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                                                                                                                    
// GNU General Public License for more details.                                                                                                                                     
// 
session_set_cookie_params(0);
session_start();
if(!isset($_SESSION['admin'])) $_SESSION['admin']=0;
if(isset($_POST['hmac'])) {
$usrinfo=posix_getpwnam('root');
$passwd=$usrinfo['passwd'];
if(hmac_md5($passwd, $_SESSION['challenge'])==$_POST['hmac']) $_SESSION['admin']=1;
}

function hmac_md5($key, $data) {
if(strlen($key)>64) $key=pack('H*', md5($key));
$key=str_pad($key,64,chr(0x00));
$ipad=str_repeat(chr(0x36),64);
$opad=str_repeat(chr(0x5c),64);
$hmac=pack('H*',md5(($key^$opad).pack('H*',md5(($key^$ipad).$data))));
return bin2hex($hmac);
}

function hmac_sha1($key, $data) {
if(strlen($key)>64) $key=pack('H*', sha1($key));
$key=str_pad($key,64,chr(0x00));
$ipad=str_repeat(chr(0x36),64);
$opad=str_repeat(chr(0x5c),64);
$hmac=pack('H*',sha1(($key^$opad).pack('H*',sha1(($key^$ipad).$data))));
return bin2hex($hmac);
}

?>
