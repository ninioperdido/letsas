<?php
// Copyright (C) 2007                                                                                                                                                               
//                                                                                                                                                                                  
// This program is free software; you can redistribute it and/or modify                                                                                                             
// it under the terms of the GNU General Public License as published                                                                                                                
// by the Free Software Foundation; version 2 only.                                                                                                                                 
//                                                                                                                                                                                  
// This program is distributed in the hope that it will be useful,                                                                                                                  
// but WITHOUT ANY WARRANTY; without even the implied warranty of                                                                                                                   
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                                                                                                                    
// GNU General Public License for more details.                                                                                                                                     
// 
require('init.inc.php');
if($_SESSION['admin']==1) {

	if(!empty($_POST)) {
		$_POST['log_comments']=rtrim(removeNL($_POST['log_comments']));
		writeLogistica($_POST);
		/* releemos las nuevos valores */
		logisticinfo();
		$msg='<div class="avisook"><div><span>&nbsp;Datos de inventario almacenados correctamente.</span></div></div>';
	}else{
		$logisticinfo['log_term_serial']=$sysinfo['serial'];
	}

?>
<h1>Logistica</h1>
<div class="divcnt5"><span class="contbout">Datos logisticos</span></div> 
<br />
<?php echo $msg; ?>
<form id="form_logistic" name="form_logistic" method="post" action="" onSubmit="return parseForm(this, 'pane_logistic');">
<fieldset>
<legend>Red</legend>
<label>Nombre del terminal:</label>
<?php echo $sysinfo['hostname']; ?>
<br /><br />
</fieldset>
<fieldset>
<legend>Terminal</legend>
<label>Numero de serie:</label>
<input class="inputlogistic" readonly="true" ype="text" id="log_term_serial" name="log_term_serial" value="<?php echo $logisticinfo['log_term_serial']; ?>" />
<label>Marca y modelo:</label>
<input class="inputlogistic" type="text" id="log_term_model" name="log_term_model" value="<?php echo $logisticinfo['log_term_model']; ?>" />
<label>Numero de inventario:</label>
<input class="inputlogistic" type="text" id="log_term_inven" name="log_term_inven" value="<?php echo $logisticinfo['log_term_inven']; ?>" />
</fieldset>
<fieldset>
<legend>Monitor</legend>
<label>Numero de serie:</label>
<input class="inputlogistic" type="text" id="log_monitor_serial" name="log_monitor_serial" value="<?php echo $logisticinfo['log_monitor_serial']; ?>" />
<label>Marca y modelo:</label>
<input class="inputlogistic" type="text" id="log_monitor_model" name="log_monitor_model" value="<?php echo $logisticinfo['log_monitor_model']; ?>" />
<label>Numero de inventario:</label>
<input class="inputlogistic" type="text" id="log_monitor_inven" name="log_monitor_inven" value="<?php echo $logisticinfo['log_monitor_inven']; ?>" />
</fieldset>
<fieldset>
<legend>Lector SC</legend>
<label>Numero de serie:</label>
<input class="inputlogistic" type="text" id="log_sc_serial" name="log_sc_serial" value="<?php echo $logisticinfo['log_sc_serial']; ?>" />
<label>Marca y modelo:</label>
<input class="inputlogistic" type="text" id="log_sc_model" name="log_sc_model" value="<?php echo $logisticinfo['log_sc_model']; ?>" />
<label>Numero de inventario:</label>
<input class="inputlogistic" type="text" id="log_sc_inven" name="log_sc_inven" value="<?php echo $logisticinfo['log_sc_inven']; ?>" />
</fieldset>
<fieldset>
<legend>Teclado</legend>
<label>Numero de serie:</label>
<input class="inputlogistic" type="text" id="log_keyboard_serial" name="log_keyboard_serial" value="<?php echo $logisticinfo['log_keyboard_serial']; ?>" />
<label>Marca y modelo:</label>
<input class="inputlogistic" type="text" id="log_keyboard_model" name="log_keyboard_model" value="<?php echo $logisticinfo['log_keyboard_model']; ?>" />
<label>Numero de inventario:</label>
<input class="inputlogistic" type="text" id="log_keyboard_inven" name="log_keyboard_inven" value="<?php echo $logisticinfo['log_keyboard_inven']; ?>" />
</fieldset>
<fieldset>
<legend>Impresora</legend>
<label>Numero de serie:</label>
<input class="inputlogistic" type="text" id="log_printer_serial" name="log_printer_serial" value="<?php echo $logisticinfo['log_printer_serial']; ?>" />
<label>Marca y modelo:</label>
<input class="inputlogistic" type="text" id="log_printer_model" name="log_printer_model" value="<?php echo $logisticinfo['log_printer_model']; ?>" />
<label>Numero de inventario:</label>
<input class="inputlogistic" type="text" id="log_printer_inven" name="log_printer_inven" value="<?php echo $logisticinfo['log_printer_inven']; ?>" />
</fieldset>
<fieldset class="minifieldset">
<legend>Datos administrativos</legend>
<label>Ubicacion fisica:</label>
<input class="inputlogistic" type="text" id="log_location" name="log_location" value="<?php echo $logisticinfo['log_location']; ?>" />
<br /><br />
<label>Organizacion:</label>
<input class="inputlogistic" type="text" id="log_organization" name="log_organization" value="<?php echo $logisticinfo['log_organization']; ?>" />
<br /><br />
<label>Departamento:</label>
<input class="inputlogistic" type="text" id="log_department" name="log_department" value="<?php echo $logisticinfo['log_department']; ?>" />
<br /><br />
<label>Edificio:</label>
<input class="inputlogistic" type="text" id="log_building" name="log_building" value="<?php echo $logisticinfo['log_building']; ?>" />
<br /><br />
<label>Centro:</label>
<input class="inputlogistic" type="text" id="log_center" name="log_center" value="<?php echo $logisticinfo['log_center']; ?>" />
<br /><br />
<label>Provincia:</label>
<input class="inputlogistic" type="text" id="log_province" name="log_province" value="<?php echo $logisticinfo['log_province']; ?>" />
<br />
</fieldset>
<fieldset class="minifieldset">
<legend>Datos de uso</legend>
<label>Asignado a:</label>
<input class="inputlogistic" type="text" id="log_assigned" name="log_assigned" value="<?php echo $logisticinfo['log_assigned']; ?>" />
<br /><br />
<label>ID de usuario:</label>
<input class="inputlogistic" type="text" id="log_userid" name="log_userid" value="<?php echo $logisticinfo['log_userid']; ?>" />
<br /><br />
<label>Telefono:</label>
<input class="inputlogistic" type="text" id="log_phone" name="log_phone" value="<?php echo $logisticinfo['log_phone']; ?>" />
<br /><br />
<label>Fecha instalacion:</label>
<input class="inputlogistic" type="text" id="log_inst_date" name="log_inst_date" value="<?php echo $logisticinfo['log_inst_date']; ?>" />
<br /><br />
<label>Fecha ultima modificacion:</label>
<input class="inputlogistic" type="text" id="log_mod_date" name="log_mod_date" value="<?php echo $logisticinfo['log_mod_date']; ?>" />
<br />
</fieldset>
<fieldset class="minifieldset">
<legend>Datos de soporte</legend>
<label>Soportado por:</label>
<input class="inputlogistic" type="text" id="log_supportedby" name="log_supportedby" value="<?php echo $logisticinfo['log_supportedby']; ?>" />
<br /><br />
<label>Telefono soporte:</label>
<input class="inputlogistic" type="text" id="log_support_phone" name="log_support_phone" value="<?php echo $logisticinfo['log_support_phone']; ?>" />
<br /><br />
<label>Observaciones:</label>
<textarea id="log_comments" name="log_comments">
<?php echo $logisticinfo['log_comments']; ?>
</textarea>
<br />
</fieldset>
<br /><br />
<div class="clear"></div>
<button type="submit" id="chlogistic" name="chlogistic">Aplicar</button>
<br /><br />
</form>
<br />
<?php } else {
require('unauthorized.inc.php');
} ?>
