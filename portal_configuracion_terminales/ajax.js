// Copyright (C) 2007 Programado por Jorge Herrera Baeza
// <jorge.herrera.exts@juntadeandalucia.es>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published
// by the Free Software Foundation; version 2 only.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//

var xmlhttp=false;
var box=false;
var htmlloading='<h1><img src="images/loading.gif" />&nbsp;Cargando......</h1>';
var htmlapplying='<h1><img src="images/loading.gif" />&nbsp;Aplicando configuracion......</h1>';
var htmlerror='<h1><img src="images/loading.gif" />&nbsp;Error......</h1>';

function ajax_get_URL(url, method, async, vars) {
try {
xmlhttp=new ActiveXObject('Msxml2.XMLHTTP');
} catch (e1) {
try {
xmlhttp=new ActiveXObject('Microsoft.XMLHTTP');
} catch (e2) {
xmlhttp=false;
}
}
if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
xmlhttp=new XMLHttpRequest();
}
if(xmlhttp) {
xmlhttp.open(method, url, async);
if(method=='POST') xmlhttp.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
xmlhttp.onreadystatechange=rschange;
xmlhttp.send(vars);
}
}

function rschange() {
if (xmlhttp.readyState==4) {
if (xmlhttp.status==200) {
res=xmlhttp.responseText;
if(box) document.getElementById(box).innerHTML=res;
} else {
document.getElementById(box).innerHTML=htmlerror;
}
}
}

function parseForm(objform, pane) {
var vars='dummy=0';
var elements=objform.childNodes;
vars+=parseElements(elements);
box=pane;
document.getElementById(box).innerHTML=htmlapplying;
ajax_get_URL(pane + '.inc.php', 'POST', true, vars);
return false;
}

function parseElements(elements) {
var retvar='';
for(var i=0; i<elements.length; i++) {
var element=elements[i];
if(element.nodeType!=1) continue;
switch(element.nodeName) {
case 'SELECT':
retvar+='&' + element.name + '=' + element.options[element.selectedIndex].value;
break;
case 'TEXTAREA':
retvar+='&' + element.name + '=' + element.value;
break;
case 'INPUT':
if(element.type=='checkbox') retvar+='&' + element.name + '=' + element.checked;
else retvar+='&' + element.name + '=' + element.value;
break;
case 'BUTTON':
retvar+='&' + element.name + '=' + element.value;
break;
case 'FIELDSET':
retvar+=parseElements(element.childNodes);
}
}
return retvar;
}

function activatePane(paneId, activeTab) {
box=paneId;
document.getElementById(box).innerHTML=htmlloading;
ajax_get_URL(paneId + '.inc.php', 'GET', true, '');
return showPane(paneId, activeTab);
}

function validateIP(obj, msgbox) {
var ip=document.getElementById(obj).value;
if(ip=='') return false;
box=msgbox;
document.getElementById(box).innerHTML='Comprobando...';
ajax_get_URL('ajax_validate_IP.php', 'POST', true, 'ip=' + ip);
}

function validateIPHost(obj, msgbox) {
var iphost=document.getElementById(obj).value;
if(iphost=='') return false;
box=msgbox;
document.getElementById(box).innerHTML='Comprobando...';
ajax_get_URL('ajax_validate_IP_host.php', 'POST', true, 'iphost=' + iphost);
}

function validateHost(obj, msgbox) {
var host=document.getElementById(obj).value;
if(host=='') return false;
box=msgbox;
document.getElementById(box).innerHTML='Comprobando...';
ajax_get_URL('ajax_validate_host.php', 'POST', true, 'host=' + host);
}

function validateMask(obj, msgbox) {
var mask=document.getElementById(obj).value;
if(mask=='') return false;
box=msgbox;
document.getElementById(box).innerHTML='Comprobando...';
ajax_get_URL('ajax_validate_mask.php', 'POST', true, 'mask=' + mask);
}

function change_dhcp() {
var dhcp=document.getElementById('dhcp').selectedIndex;
if(dhcp=='0') {
document.getElementById('ifip').disabled=false;
document.getElementById('ifmask').disabled=false;
document.getElementById('ifgw').disabled=false;
} else {
document.getElementById('ifip').disabled=true;
document.getElementById('ifmask').disabled=true;
document.getElementById('ifgw').disabled=true;
}
}

function change_service() {
var active=0;
var service=document.getElementById('service').selectedIndex;
document.getElementById('allowvnc').disabled=true;
switch(service) {
case 0:
active=document.getElementById('service_network').value;
break;
case 1:
active=document.getElementById('service_printer').value;
break;
case 2:
active=document.getElementById('service_vnc').value;
document.getElementById('allowvnc').disabled=false;
break;
case 3:
active=document.getElementById('service_altiris').value;
break;
case 4:
active=document.getElementById('service_xinetd').value;
break;
case 5:
active=document.getElementById('service_terminal').value;
break;
case 6:
active=document.getElementById('service_wdm').value;
break;
}
if(active==1) {
document.getElementById('start').disabled=true;
document.getElementById('stop').disabled=false;
document.getElementById('restart').disabled=false;
} else {
document.getElementById('start').disabled=false;
document.getElementById('stop').disabled=true;
document.getElementById('restart').disabled=true;
}
if(service==0 || service==6) document.getElementById('stop').disabled=true;
}

function change_storage() {
var store=document.getElementById('store').selectedIndex;
if(store=='1') {
//document.getElementById('store').selectedIndex=0;
alert("Atencion. Esta opcion no puede ser seleccionada,\nsin estar disponible los servicios de Autentificacion LDAP\ny/o los recursos de Almacenamiento Remotos.");
document.getElementById('smb_type').disabled=false;
document.getElementById('smb_user').disabled=false;
document.getElementById('smb_pass').disabled=false;
document.getElementById('smb_dom').disabled=false;
document.getElementById('smb_ip').disabled=false;
document.getElementById('smb_path').disabled=false;
document.getElementById('usb').disabled=true;
} else {
document.getElementById('smb_type').disabled=true;
document.getElementById('smb_user').disabled=true;
document.getElementById('smb_pass').disabled=true;
document.getElementById('smb_dom').disabled=true;
document.getElementById('smb_ip').disabled=true;
document.getElementById('smb_path').disabled=true;
document.getElementById('usb').disabled=false;
}
return false;
}

function login(msgbox) {
var salt=document.getElementById('salt').value;
var challenge=document.getElementById('challenge').value;
var passwd=document.getElementById('passwd').value;
var crypt=crypt_md5(passwd, salt, '$1$');
var hmac=hex_hmac_md5(crypt, challenge);
box=msgbox;
document.getElementById(box).innerHTML=htmlapplying;
ajax_get_URL('ajax_login.php', 'POST', false, 'hmac=' + hmac);
window.location.href=window.location.href;
}

function logout(msgbox) {
box=msgbox;
document.getElementById(box).innerHTML=htmlapplying;
ajax_get_URL('ajax_logout.php', 'GET', false, '');
window.location.href=window.location.href;
}

function change_prn_type() {
var prn=document.getElementById('prn').selectedIndex;
switch(prn) {
case 2:
document.getElementById('serverip').disabled=false;
document.getElementById('spool').disabled=true;
break;
case 3:
document.getElementById('serverip').disabled=false;
document.getElementById('spool').disabled=false;
break;
default:
document.getElementById('serverip').disabled=true;
document.getElementById('spool').disabled=true;
}
}

function delete_printer(msgbox, selection) {
document.getElementById('selected_printer').value=selection;
document.getElementById('printer_action').value='1';
//document.getElementById('form_printers').submit();
parseForm(document.getElementById('form_printers'), 'pane_printers');
}

//AJAB 
function add_printer(msgbox) {
//document.getElementById('selected_printer').value=selection;
//document.getElementById('printer_action').value='1';
//document.getElementById('form_printers').submit();
parseForm(document.getElementById('form_printers'), 'pane_printers');
}

function init_service(msgbox, action) {
document.getElementById('service_action').value=action;
service=document.getElementById('service').selectedIndex;
if(service==5 && action==1) if(!confirm('Va a proceder a apagar el terminal, perderá la conexión y deberá iniciarlo manualmente.')) return false;
if(service==5 && action==2) if(!confirm('Va a proceder a reiniciar el terminal, refresque la paginá pasados unos minutos para volver recuperar la conexión.')) return false;
//document.getElementById('form_services').submit();
parseForm(document.getElementById('form_services'), 'pane_services');
}
