<?php
// Copyright (C) 2007                                                                                                                                                               
//                                                                                                                                                                                  
// This program is free software; you can redistribute it and/or modify                                                                                                             
// it under the terms of the GNU General Public License as published                                                                                                                
// by the Free Software Foundation; version 2 only.                                                                                                                                 
//                                                                                                                                                                                  
// This program is distributed in the hope that it will be useful,                                                                                                                  
// but WITHOUT ANY WARRANTY; without even the implied warranty of                                                                                                                   
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                                                                                                                    
// GNU General Public License for more details.                                                                                                                                     
// 
require('init.inc.php');
?>
<h1>Informacion del sistema</h1>
<div class="ctablesinfo">
<div class="cinfol">
<table>
<tr><th class="infospant" colspan="2">BIOS</th></tr>
<tr><th class="infot">Fabricante:</th><td>&nbsp;<?php echo $sysinfo['vendor']; ?></td></tr>
<tr><th class="infot">Version:</th><td>&nbsp;<?php echo $sysinfo['version']; ?></td></tr>
<tr><th class="infot">Fecha:</th><td>&nbsp;<?php echo $sysinfo['date']; ?></td></tr>
<tr><th class="infospant" colspan="2">Sistema</th></tr>
<tr><th class="infot">Fabricante:</th><td>&nbsp;<?php echo $sysinfo['manufacturer']; ?></td></tr>
<tr><th class="infot">Modelo:</th><td>&nbsp;<?php echo $sysinfo['product']; ?></td></tr>
<tr><th class="infot">Numero de serie:</th><td>&nbsp;<?php echo $sysinfo['serial']; ?></td></tr>
<tr><th class="infospant" colspan="2">Procesador y memoria</th></tr>
<tr><th class="infot">Procesador:</th><td>&nbsp;<?php echo $sysinfo['cpu'] . ' (' . $sysinfo['speed'] . ' Mhz)'; ?></td></tr>
<tr><th class="infot">Memoria RAM:</th><td>&nbsp;<?php echo $sysinfo['ram']; ?></td></tr>
<tr><th class="infot">Memoria flash:</th><td>&nbsp;<?php echo $sysinfo['flash']; ?></td></tr>
<tr><th class="infospant" colspan="2">Sistema operativo</th></tr>
<tr><th class="infot">Sistema operativo y version:</th><td>&nbsp;<?php echo $sysinfo['os']; ?></td></tr>
<tr><th class="infospant" colspan="2">Informacion de red</th></tr>
<tr><th class="infot">Nombre del terminal:</th><td>&nbsp;<?php echo $sysinfo['hostname']; ?></td></tr>
<tr><th class="infot">Direccion IP:</th><td>&nbsp;<?php echo $sysinfo['ip']; ?></td></tr>
<tr><th class="infot">Mascara de red:</th><td>&nbsp;<?php echo $sysinfo['mask']; ?></td></tr>
<tr><th class="infot">Puerta de enlace:</th><td>&nbsp;<?php echo $sysinfo['gw']; ?></td></tr>
<tr><th class="infot">Direccion MAC:</th><td>&nbsp;<?php echo $sysinfo['mac']; ?></td></tr>
</table>
</div>
<div class="cinfor">
<table>
<tr><th class="infospant" colspan="2">Identidad</th></tr>
<tr><th class="infot">Numero de serie del terminal:</th><td>&nbsp;<?php if (!empty($sysinfo['serial'])) echo $sysinfo['serial']; else echo $logisticinfo['log_term_serial'];?></td></tr>
<tr><th class="infot">Marca / modelo del terminal:</th><td>&nbsp;<?php echo $logisticinfo['log_term_model']; ?></td></tr>
<tr><th class="infot">Numero de inventario del terminal:</th><td>&nbsp;<?php echo $logisticinfo['log_term_inven']; ?></td></tr>
<tr><th class="infot">Numero de serie del monitor:</th><td>&nbsp;<?php echo $logisticinfo['log_monitor_serial']; ?></td></tr>
<tr><th class="infot">Marca / modelo del monitor:</th><td>&nbsp;<?php echo $logisticinfo['log_monitor_model']; ?></td></tr>
<tr><th class="infot">Numero de inventario del monitor:</th><td>&nbsp;<?php echo $logisticinfo['log_monitor_inven']; ?></td></tr>
<tr><th class="infot">Numero de serie del lector SC:</th><td>&nbsp;<?php echo $logisticinfo['log_sc_serial']; ?></td></tr>
<tr><th class="infot">Marca / modelo del lector SC:</th><td>&nbsp;<?php echo $logisticinfo['log_sc_model']; ?></td></tr>
<tr><th class="infot">Numero de inventario del lector SC:</th><td>&nbsp;<?php echo $logisticinfo['log_sc_inven']; ?></td></tr>
<tr><th class="infot">Numero de serie del teclado:</th><td>&nbsp;<?php echo $logisticinfo['log_keyboard_serial']; ?></td></tr>
<tr><th class="infot">Marca / modelo del teclado:</th><td>&nbsp;<?php echo $logisticinfo['log_keyboard_model']; ?></td></tr>
<tr><th class="infot">Numero de inventario del teclado:</th><td>&nbsp;<?php echo $logisticinfo['log_keyboard_inven']; ?></td></tr>
<tr><th class="infot">Numero de serie de la impresora:</th><td>&nbsp;<?php echo $logisticinfo['log_printer_serial']; ?></td></tr>
<tr><th class="infot">Marca / modelo de la impresora:</th><td>&nbsp;<?php echo $logisticinfo['log_printer_model']; ?></td></tr>
<tr><th class="infot">Numero de inventario de la impresora:</th><td>&nbsp;<?php echo $logisticinfo['log_printer_inven']; ?></td></tr>
<tr><th class="infospant" colspan="2">Administrativos</th></tr>
<tr><th class="infot">Ubicacion fisica:</th><td>&nbsp;<?php echo $logisticinfo['log_location']; ?></td></tr>
<tr><th class="infot">Organizacion:</th><td>&nbsp;<?php echo $logisticinfo['log_organization']; ?></td></tr>
<tr><th class="infot">Departamento:</th><td>&nbsp;<?php echo $logisticinfo['log_department']; ?></td></tr>
<tr><th class="infospant" colspan="2">Soporte</th></tr>
<tr><th class="infot">Soportado por:</th><td>&nbsp;<?php echo $logisticinfo['log_supportedby']; ?></td></tr>
<tr><th class="infot">Telefono soporte:</th><td>&nbsp;<?php echo $logisticinfo['log_support_phone']; ?></td></tr>
</table>
</div>
</div>
<div class="infofooter"></div>
