<?php
// Copyright (C) 2007                                                                                                                                                               
//                                                                                                                                                                                  
// This program is free software; you can redistribute it and/or modify                                                                                                             
// it under the terms of the GNU General Public License as published                                                                                                                
// by the Free Software Foundation; version 2 only.                                                                                                                                 
//                                                                                                                                                                                  
// This program is distributed in the hope that it will be useful,                                                                                                                  
// but WITHOUT ANY WARRANTY; without even the implied warranty of                                                                                                                   
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                                                                                                                    
// GNU General Public License for more details.                                                                                                                                     
// 
require('init.inc.php');
if(isset($_SESSION['admin']) && $_SESSION['admin']==1) {
	$printers = new Impresoras(PRINTCAP);
	$nprinters=$printers->getNprinters();
	$names=$printers->getPrinterNames();
	$ports=$printers->getPorts();
	$spools=$printers->getSpoolDirs();
	
	$add=FALSE;

	if (isset($_POST['prn']) && $_POST['printer_action']==0){
		$prn_selected=$_POST['prn'];
		$error=FALSE;
		$alias=rtrim($_POST['alias']);
		$desc=rtrim($_POST['desc']);
		$serverip=rtrim($_POST['serverip']);
		$netspool=rtrim($_POST['spool']);
		/* Campo obligatorio */
		$name=rtrim($_POST['name']);
		if (isValidSpoolName($name)==false || duplicateName($name,$names)){
			$error=TRUE;
		}
		
		if (!$error){ /* Nombre valido */
		$impresora_elegida=$_POST['prn'];
		switch ($impresora_elegida) {
			/* paralelo */
			case '0':
				/*Comprobar si ya existe definido el puerto lp0*/
				/* 
				 * AJAB 31/05/07 Se elimina esta opci�n, dado que es posible crear distintas colas para el mismo dispositivo, 
				 * siempre y cuando no tengan el mismo nombre (comprobacion realizada) 
				*/ 
				//if ($printers->isDefinedLp0()===FALSE){ 
				/*Creamos la impresora*/ 
				$port=$printers->getDevLp0(); 
				$spool=$printers->buildSpoolDir($name); 
				$printers->addPort($port); 
				$printers->addSpoolDir($spool); 
				$printers->addNameAliasDesc($name,$alias,$desc); 
				$printers->addHasFilter('lp'); 
				/*Escribimos fichero*/ 
				$printers->writePrinters(); 
				$add=TRUE; 
				//}else{ 
				//	$error=TRUE; 
				//	$msg='<div class="aviso"><div><span>&nbsp;La impresora de puerto paralelo (LPT0) ya se encuentra definida.</span></div></div>'; 
				//}
				break;
			/*usb*/
			case '1':
				/* 
				 * AJAB 31/05/07 Se restringe el n�mero de puertos usb a 1 (usblp0).
				 */ 

				/*Comprobar si es posible crear mas colas USB*/ 
				//$usb_ocupados=array_intersect($printers->getUsbPorts(),$ports); 
				//if (count($usb_ocupados) == 6 ){ 
				//	$error=TRUE; 
				//	$msg='<div class="avisowarn"><div><span>&nbsp;No esposible a&ntilde;adir m&aacute;s impresoras USB. (M&aacute;x. 6)</span></div></div>'; 
				//}else{ 
					//$port=$printers->getNetxUsbPort($usb_ocupados); 
					$port=$printers-> getDevUsb0();
					$spool=$printers->buildSpoolDir($name); 
					$printers->addPort($port); 
					$printers->addSpoolDir($spool); 
					$printers->addNameAliasDesc($name,$alias,$desc); 
					$printers->addHasFilter('usb'); 
					/*Escribimos fichero*/ 
					$printers->writePrinters(); 
					$add=TRUE; 
					//}
				break;
			/*red*/
			case '2':
				/*Obligatorios name y serverip*/
				if (Validate_IP($serverip) || validate_host($serverip)){
					$port="$name@$serverip";
					$spool=$printers->buildSpoolDir($name);
					$printers->addPort($port);
					$printers->addSpoolDir($spool);
					$printers->addNameAliasDesc($name,$alias,$desc);
					$printers->addHasFilter('net');
					/*Escribimos fichero*/
					$printers->writePrinters();
					$add=TRUE;
				}else{
					$error=TRUE;
					$msg='<div class="avisowarn"><div><span>&nbsp;Direcci&oacute;n IP o nombre de host no v&aacute;lido.</span></div></div>';
				}
				break;
			/*LPR*/
			case '3':
				/*obligatorios name, serverip y netspool */
				$validIp=false;
				$validSpool=false;
				
				
				if (Validate_IP($serverip) || validate_host($serverip)) $validIp=true;
				if (isValidSpoolName($netspool)) $validSpool=true;
				
				if ($validIp && $validSpool){
					$port="$netspool@$serverip";
					$spool=$printers->buildSpoolDir($name);
					$printers->addPort($port);
					$printers->addSpoolDir($spool);
					$printers->addNameAliasDesc($name,$alias,$desc);
					$printers->addHasFilter('lpr');
					/*Escribimos fichero*/
					$printers->writePrinters();
					$add=TRUE;
					
				}else{
					$error=TRUE;
					$msg='<div class="avisowarn"><div><span>&nbsp;Direcci&oacute;n IP, nombre de host o <br />&nbsp;nombre de cola de impresi&oacute;n no v&aacute;lidos.</span></div></div>';
				}
				break;
		} /* switch */
		if ($add) $msg='<div class="avisook"><div><span>&nbsp;Impresora '.$nameprinter.' a&ntilde;adida.<br />&nbsp;No olvide reiniciar el servicio IMPRESI&Oacute;N para que<br />&nbsp;los cambios tengan efecto.</span></div></div>';
		}else{ /* Nombre no valido */
			$error=TRUE;
			$msg='<div class="avisowarn"><div><span>&nbsp;Nombre de impresora no v&aacute;lido: '.$name.'<br />&nbsp;El nombre ya existe o contiene car&aacute;cteres no v&aacute;lidos.</span></div></div>';
		}
	}else{
		if ($_POST['printer_action']==1){ /*eliminar impresora*/
			$index=$_POST['selected_printer'];
			$nameprinter=getName($names[$index],1);
			$printers->deletePrinter($index);
			$ok=$printers->writePrinters();
			if ($ok) $msg='<div class="avisook"><div><span>&nbsp;Impresora '.$nameprinter.' eliminada.<br />&nbsp;No olvide reiniciar el servicio IMPRESI&Oacute;N para que<br />&nbsp;los cambios tengan efecto.</span></div></div>';
		}
	}
?>
<h1>Impresoras</h1>
<div class="divcnt5"><span class="contbout">Configuracion de impresoras</span></div>
<?php echo  $msg;?>
<form id="form_printers" name="form_printers" method="post" action="">
<br />
<div id="prnlist"></div>
<?php  
	 if (isset($_POST['prn'])){
	         $names=$printers->getPrinterNames();
	         $ports=$printers->getPorts();
	         $spools=$printers->getSpoolDirs();
		 $nprinters=$printers->getNprinters();
	 }
	 if ($nprinters > 0) { 
?>
<div class="ctables">
	<table>
	<tr>	
	<th width="15%">Nombre</th>
	<th width="20%">Alias</th>
	<th width="35%">Descripci&oacute;n</th>
	<th width="20%">Puerto</th>
	<th width="5%">Tipo</th>
	<th width="5%">Eliminar</th>
	</tr>
                                                                                                        
<?php 
	for ($i=0; $i < $nprinters; $i++){
		echo "<tr>\n";
		$list_nombre=$printers->getPrinterName($names[$i]);
		echo "<td>$list_nombre</td>\n";
		$list_alias=is_null($printers->getPrinterAlias($names[$i])) ? "&nbsp;" : $printers->getPrinterAlias($names[$i]);
		echo "<td>$list_alias</td>\n";
		$list_description=is_null($printers->getPrinterDescription($names[$i])) ? "&nbsp;" : $printers->getPrinterDescription($names[$i]);
		echo "<td>$list_description</td>\n";
		$list_port=$printers->getPort($ports[$i]);
		echo "<td>$list_port</td>\n";
		if (strstr($list_port, '@')==TRUE) {
			echo '<td><img alt="Red" title="Red" src="images/remota.gif" /></td>'."\n";
		}else{
			echo '<td><img alt="Local" title="Local" src="images/local.gif" /></td>'."\n";
		}
		echo '<td><button class="deleteprinter" id="prn'.$i.'" name="prn'.$i.'" onClick="delete_printer(\'pane_printers\', '.$i.');">'."\n".'<img alt="Eliminar" title="Eliminar" src="images/tbldel.gif" /></button></td>';
		echo "</tr>\n";
	}
?>
	</table>
	</div>
<?php
}else{
	echo $msg='<div class="avisowarn"><div><span>&nbsp;No existen impresoras configuradas.</span></div></div>';
}
?>
<br />
<label>Tipo de impresora:</label>
<select id="prn" name="prn" onChange="change_prn_type()">
<option value="0" <?php if ($prn_selected==0) echo 'selected="true"';?>>Impresora local conectada a puerto paralelo</option>
<option value="1" <?php if ($prn_selected==1) echo 'selected="true"';?>>Impresora local conectada a puerto USB</option>
<option value="2" <?php if ($prn_selected==2) echo 'selected="true"';?>>Impresora de red</option>
<option value="3" <?php if ($prn_selected==3) echo 'selected="true"';?>>Impresora LPR</option>
</select>
<br />
<label>&nbsp;</label>
<span class="comentario">Pulse <a href="#" onclick="window.open('impresoras.html','ListaImpresoras','width=700,height=550,toolbar=0,status=0,resizable=1');">aqu&iacute;</a> para ver la lista de impresoras soportadas</span>
<input type="hidden" id="selected_printer" name="selected_printer" value="0" />
<input type="hidden" id="printer_action" name="printer_action" value="0" />
<br /><br />
<label>Nombre de la impresora:</label>
<input type="text" id="name" name="name" value="<?php echo ($error==TRUE)? $name: ""; ?>" maxlength="20"/>
<br /><br />
<label>Alias:</label>
<input type="text" id="alias" name="alias" value="<?php echo ($error==TRUE)? $alias: ""; ?>" maxlength="20"/>
<br /><br />
<label>Descripcion:</label>
<input type="text" id="desc" name="desc" value="<?php echo ($error==TRUE)? $desc: ""; ?>" maxlength="30"/>
<br /><br />
<label>Direcci&oacute;n IP:</label>
<input <?php echo ($prn_selected==2 || $prn_selected==3) ? '':'disabled="disabled"';?> type="text" id="serverip" name="serverip" value="<?php echo ($error==TRUE)? $serverip : ""; ?>" onblur="validateIPHost('serverip', 'msgifip')"/>
<div class="divmsg" id="msgifip"></div>
<br /><br />
<label>Cola de impresion:</label>
<input <?php echo ($prn_selected==3) ? '':'disabled="disabled"';?> type="text" id="spool" name="spool" value="<?php echo ($error==TRUE)? $netspool: ""; ?>" />
<br /><br />
<button id="chprn" name="chprn" onclick="add_printer('pane_printers');">Aplicar</button>
<br /><br />
<?php } else {
require('unauthorized.inc.php');
} ?>
