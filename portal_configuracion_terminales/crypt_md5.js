// Copyright (C) 2007 Programado por Jorge Herrera Baeza
// <jorge.herrera.exts@juntadeandalucia.es>
//
// Uses md5.js by Paul Johnston http://pajhome.org.uk/crypt/md5
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published
// by the Free Software Foundation; version 2 only.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//


function crypt_md5(password, salt, magic) {
var table1='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';
var table2='./0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
var hash1=password+magic+salt;
var hash2=password+salt+password;
var step1=str_md5(hash2);
var step2;
var step3='$1$'+salt+'$';
for(var i=password.length; i>0; i-=16)
if(i>16) hash1+=step1.substring(0,16); else hash1+=step1.substring(0,i);
for(var i=password.length; i>0; i>>= 1) {
if((i & 1)!=0) hash1+="\0";
else hash1+=password.substring(0, 1);
}
step1=str_md5(hash1);
for(var i=0; i<1000; i++) {
var crypted=(i & 1) ? password : step1;
if ( i % 3) crypted = crypted + salt;
if ( i % 7) crypted = crypted + password;
crypted+=(i & 1) ? step1 : password;
step1=str_md5(crypted);
}
step2=step1.charAt(0)+step1.charAt(6)+step1.charAt(12)+step2;
step2=step1.charAt(1)+step1.charAt(7)+step1.charAt(13)+step2;
step2=step1.charAt(2)+step1.charAt(8)+step1.charAt(14)+step2;
step2=step1.charAt(3)+step1.charAt(9)+step1.charAt(15)+step2;
step2=step1.charAt(4)+step1.charAt(10)+step1.charAt(5)+step2;
step2="\0\0"+step1.charAt(11)+step2;
step2=binl2b64(str2binl(step2));
for (var i = 23 ; i != 1 ; i--) step3+=table2.charAt(table1.indexOf(step2.charAt(i)));
return step3;
}
