<?php
// Copyright (C) 2007                                                                                                                                                               
//                                                                                                                                                                                  
// This program is free software; you can redistribute it and/or modify                                                                                                             
// it under the terms of the GNU General Public License as published                                                                                                                
// by the Free Software Foundation; version 2 only.                                                                                                                                 
//                                                                                                                                                                                  
// This program is distributed in the hope that it will be useful,                                                                                                                  
// but WITHOUT ANY WARRANTY; without even the implied warranty of                                                                                                                   
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                                                                                                                    
// GNU General Public License for more details.                                                                                                                                     
// 
require('init.inc.php');
$ip=$_POST['ip'];
if(validate_IP($ip)){ /* ip valida */
	$hostname=gethostbyaddr($ip);
	if ($hostname==$ip){
		echo '<span style="color: #360;">La direccion IP es correcta.</span>'; 
	}else{
		echo '<span style="color: #360;">La direccion IP es correcta. Host: '.$hostname.'</span>';
	}
}else{
	echo '<span style="color: #f00;">Direcci&oacute;n IP no valida.</span>';
}
?>
