<?php
// Copyright (C) 2007                                                                                                                                                               
//                                                                                                                                                                                  
// This program is free software; you can redistribute it and/or modify                                                                                                             
// it under the terms of the GNU General Public License as published                                                                                                                
// by the Free Software Foundation; version 2 only.                                                                                                                                 
//                                                                                                                                                                                  
// This program is distributed in the hope that it will be useful,                                                                                                                  
// but WITHOUT ANY WARRANTY; without even the implied warranty of                                                                                                                   
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                                                                                                                    
// GNU General Public License for more details.                                                                                                                                     
// 
require('init.inc.php');
if($_SESSION['admin']==1) {

if(!empty($_POST)) {
	$ntp_error=false;
	$altiris_error=false;
	$altiris=$_POST['altiris'];
	$altirisp= empty($_POST['altirisp']) ? 402 : $_POST['altirisp'];
	$ntp=$_POST['ntp'];
	$msg__altiris="";
	$msg_ntp="";
	/* Configuracion NTP */
	if (empty($ntp)){
		/* Eliminamos la configuracion ntp*/
		$ntp_error=true;
		$cmd=LETNTP_SH." REMOVE";
		$ok=lanzaLetScript($cmd,$output);
		$sysinfo['ntp']="";
		
	}else{
		if (validate_host($ntp_srv)){
			/* Aplicamos el servidor ntp */
			$cmd=LETNTP_SH." $ntp";
			$ok=lanzaLetScript($cmd,$output);
			if ($ok==0){
				if (strstr($output,"FAILED")) {
					$msg_ntp='<div class="avisowarn"><div><span>&nbsp;Configuraci&oacute;n NTP:&nbsp;Configuraci&oacute;n realizada, pero el servicio NTP no se ha podido iniciar.&nbsp;Por favor compruebe que su servidor de hora (NTP) funciona correctamente.</span></div></div>';
				}else{
					$msg_ntp='<div class="avisook"><div><span>&nbsp;Configuraci&oacute;n NTP:<br />&nbsp;Configuraci&oacute;n realizada correctamente.</span></div></div>';
				}
				$sysinfo['ntp']=$ntp;
			}else{
				$msg_ntp='<div class="aviso"><div><span>&nbsp;Configuraci&oacute;n NTP:<br />&nbsp;Ha ocurrido un problema al ejecutar el proceso: '.$cmd.'</span></div></div>';
			}
		}else{
			$msg_ntp='<div class="avisowarn"><div><span>&nbsp;Configuraci&oacute;n NTP:<br />&nbsp;Por favor introduzca una direcci&oacute;n IP v&aacute;lida.</span></div></div>';
		}
	}
	
	/* Configuracion ALTIRIS */
	if (empty($altiris)) { 
		$altiris_error=true;
	}else{
		if (validate_host($altiris)){
			if (validPort($altirisp)){
				/* Aplicamos el cambio configuracion altiris */	
				$cmd=LETALTIRIS_SH." $altiris $altirisp";
				$ok=lanzaLetScript($cmd);
				if ($ok==0){
					$sysinfo['altiris']=$altiris;
					$sysinfo['altirisp']=$altirisp;
					$msg_altiris='<div class="avisook"><div><span>&nbsp;Configuraci&oacute;n Altiris: Cambio realizado correctamente.<br />&nbsp;La nueva configuraci&oacute;n se aplicar&aacute; una vez<br />&nbsp;reiniciado el terminal o servicio de Altiris.</span></div></div>';
				}else{
					$msg_altiris='<div class="aviso"><div><span>&nbsp;Configuraci&oacute;n Altiris:<br />&nbsp;Ha ocurrido un problema al ejecutar el proceso: '.$cmd.'</span></div></div>';
				}
			}else{
				$msg_altiris='<div class="avisowarn"><div><span>&nbsp;Configuraci&oacute;n Altiris:<br />&nbsp;Por favor introduzca un puerto v&aacute;lido [1-65535].</span></div></div>';
			}
		}else{
			$msg_altiris='<div class="avisowarn"><div><span>&nbsp;Configuraci&oacute;n Altiris:<br />&nbsp;Por favor introduzca una direcci&oacute;n IP v&aacute;lida.</span></div></div>';
		}
	}
}

?>
<h1>Servicios</h1>
<div class="divcnt5"><span class="contbout">Configurar servicios varios</span></div>
<?php if (!empty($msg_altiris)) echo $msg_altiris; ?>
<?php if (!empty($msg_ntp)) echo $msg_ntp; ?>
<br />
<form id="form_misc" name="form_misc" method="post" action="" onSubmit="return parseForm(this, 'pane_misc');">
<label>Servidor Altiris (Puerto):</label>
<input type="text" name="altiris" id="altiris" value="<?php echo $sysinfo['altiris']; ?>" onBlur="validateIPHost('altiris', 'msgaltiris')" />
(
<input class="inputp" type="text" name="altirisp" id="altirisp" value="<?php echo $sysinfo['altirisp']; ?>" />
)
<div class="divmsg" id="msgaltiris"></div>
<br /><br />
<label>Servidor NTP:</label>
<input type="text" name="ntp" id="ntp" value="<?php echo $sysinfo['ntp'];?>" onBlur="validateIPHost('ntp', 'msgntp')" />
<div class="divmsg" id="msgntp"></div>
<br /><br />
<button type="submit" id="chmisc" name="chmisc">Aplicar</button>
</form>
<br />
<?php } else {
require('unauthorized.inc.php');
} ?>
