<?php
// Copyright (C) 2007                                                                                                                                                               
//                                                                                                                                                                                  
// This program is free software; you can redistribute it and/or modify                                                                                                             
// it under the terms of the GNU General Public License as published                                                                                                                
// by the Free Software Foundation; version 2 only.                                                                                                                                 
//                                                                                                                                                                                  
// This program is distributed in the hope that it will be useful,                                                                                                                  
// but WITHOUT ANY WARRANTY; without even the implied warranty of                                                                                                                   
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                                                                                                                    
// GNU General Public License for more details.                                                                                                                                     
// 
require('init.inc.php');
if($_SESSION['admin']==1) {

if(!empty($_POST)){
	$error=FALSE;
	$hostname=rtrim($_POST['hostname']);
	$hostname_actual=rtrim($sysinfo['hostname']);
	$dhcp=$_POST['dhcp'];
	$ifip=$_POST['ifip'];
	$ifmask=$_POST['ifmask'];
	$ifgw=$_POST['ifgw'];
	$dns1=$_POST['dns1'];
	$dns2=$_POST['dns2'];
	
/*dhcp: 1 dhcp */
/*      0 static */
	if (!empty($hostname) && validate_host($hostname)==TRUE){
		if ($dhcp=='0'){ /* static ip */
			/*valores obligatorios ifip, ifmask, ifgw */
			if(!empty($ifip) && !empty($ifmask) && !empty($ifgw)){
				$action='static';
				$sysinfo['dhcp']=0;
				$sysinfo['ifip']=$ifip;
				$sysinfo['ifmask']=$ifmask;
				$sysinfo['ifgw']=$ifgw;
			}else{
				/* STATIC IP campos obligatorios vacios */
				$error=TRUE;
			}
		}else{ /* dhcp */
			$action='dhcp';
			$sysinfo['dhcp']=1;
		}
	}else{
		/* hostname vacio */
		$error=TRUE;
	}
	$dns=FALSE;
	$hostchange=FALSE;
	if ($hostname != $hostname_actual){
		$sysinfo['hostname']=$hostname;
		$hostchange=TRUE;
	}
	if (!empty($dns1) || !empty($dns2)){
		$sysinfo['dns1']=$dns1;
		$sysinfo['dns2']=$dns2;
		$dns=TRUE;
	}
	
	if ($error==FALSE){
		$cmd_hostname=LETNETWORK_SH." hostname $hostname";
		$cmd_dhcp=LETNETWORK_SH." dhcp";
		$cmd_static=LETNETWORK_SH." static $ifip $ifmask $ifgw";
		$cmd_dns=LETNETWORK_SH." dns $dns1 $dns2";
		$cmds=array('dhcp'=>$cmd_dhcp,'static'=>$cmd_static,'hostname'=>$cmd_hostname,'dns'=>$cmd_dns);
		$keys=array();
		$keys[]=$action;
		if ($dns) $keys[]='dns'; 
		if ($hostchange) $keys[]='hostname';
		foreach ($keys as $action){
			$ok=lanzaLetScript($cmds[$action]);
			if ($ok==0) { 
				$msg='<div class="avisook"><div><span>&nbsp;Cambios realizados correctamente.<br />&nbsp;La nueva configuraci&oacute;n se aplicar&aacute; una vez<br />&nbsp;reiniciado el terminal o servicio de red.</span></div></div>';
			}else{
				$msg='<div class="aviso"><div><span>&nbsp;Ha ocurrido un problema al ejecutar el proceso '.$cmds[$action].'</span></div></div>';
			}
		}
		if ($hostchange) $msg_hostchange='<div class="avisowarn"><div><span>&nbsp;IMPORTANTE: Es imprescidible reiniciar<br />&nbsp;para el correcto funcionamiento del terminal.</span></div></div>';
	}else{
		$msg='<div class="avisowarn"><div><span>&nbsp;Nombre de host no v&aacute;lido.</span></div></div>';
	}

}

?>
<h1>Red</h1>
<div class="divcnt5"><span class="contbout">Configuracion de red</span></div>
<br />
<?php echo $msg; ?>
<?php echo $msg_hostchange; ?>
<form id="form_network" name="form_network" method="post" action="" onSubmit="return parseForm(this, 'pane_network');">
<label>Nombre de la maquina:</label>
<input type="text" id="hostname" name="hostname" value="<?php echo $sysinfo['hostname']; ?>" onBlur="validateHost('hostname', 'msghostname')" />
<div class="divmsg" id="msghostname"></div>
<br /><br />
<label>Usar DHCP:</label>
<select id="dhcp" name="dhcp" onChange="change_dhcp()">
<option value="0"<?php if($sysinfo['dhcp']==0) echo ' selected="true"'; ?>>No</option>
<option value="1"<?php if($sysinfo['dhcp']==1) echo ' selected="true"'; ?>>Si</option>
</select>
<br /><br />
<label>Direccion IP:</label>
<input<?php if($sysinfo['dhcp']==1) echo ' disabled="disabled"'; ?> type="text" id="ifip" name="ifip" value="<?php echo ($sysinfo['dhcp']==1) ? $sysinfo['ip'] : $sysinfo['ifip']; ?>" onBlur="validateIP('ifip', 'msgifip')" />
<div class="divmsg" id="msgifip"></div>
<br /><br />
<label>Mascara de red:</label>
<input<?php if($sysinfo['dhcp']==1) echo ' disabled="disabled"'; ?> type="text" id="ifmask" name="ifmask" value="<?php echo ($sysinfo['dhcp']==1) ? $sysinfo['mask'] : $sysinfo['ifmask']; ?>" onBlur="validateMask('ifmask', 'msgifmask')" />
<div class="divmsg" id="msgifmask"></div>
<br /><br />
<label>Puerta de enlace:</label>
<input<?php if($sysinfo['dhcp']==1) echo ' disabled="disabled"'; ?> type="text" id="ifgw" name="ifgw" value="<?php echo ($sysinfo['dhcp']==1) ? $sysinfo['gw'] : $sysinfo['ifgw']; ?>" onBlur="validateIP('ifgw', 'msgifgw')" />
<div class="divmsg" id="msgifgw"></div>
<br /><br />
<label>Servidor DNS primario:</label>
<input type="text" id="dns1" name="dns1" value="<?php echo $sysinfo['dns1']; ?>" onBlur="validateIP('dns1', 'msgdns1')" />
<div class="divmsg" id="msgdns1"></div>
<br /><br />
<label>Servidor DNS secundario:</label>
<input type="text" id="dns2" name="dns2" value="<?php echo $sysinfo['dns2']; ?>" onBlur="validateIP('dns2', 'msgdns2')" />
<div class="divmsg" id="msgdns2"></div>
<br /><br />
<button type="submit" id="chnet" name="chnet">Aplicar</button>
<br />
</form>
<br />
<?php } else {
require('unauthorized.inc.php');
} ?>
