<?php
// Copyright (C) 2007                                                                                                                                                               
//                                                                                                                                                                                  
// This program is free software; you can redistribute it and/or modify                                                                                                             
// it under the terms of the GNU General Public License as published                                                                                                                
// by the Free Software Foundation; version 2 only.                                                                                                                                 
//                                                                                                                                                                                  
// This program is distributed in the hope that it will be useful,                                                                                                                  
// but WITHOUT ANY WARRANTY; without even the implied warranty of                                                                                                                   
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                                                                                                                    
// GNU General Public License for more details.                                                                                                                                     
// 
require('init.inc.php');
if($_SESSION['admin']==1) {

	if(!empty($_POST)) {
		if ((!empty($_POST['pw_admin']) && $_POST['pw_admin']!=$_POST['pw_admin2']) || (!empty($_POST['pw_user']) && $_POST['pw_user']!=$_POST['pw_user2']) || (!empty($_POST['pw_vnc']) && $_POST['pw_vnc']!=$_POST['pw_vnc2'])) {
			$msg='<div class="avisowarn"><div><span>Se han introducido dos contrase&ntilde;as que no coinciden.<br />No se ha realizado ning&uacute;n cambio.</span></div></div>';
		}else{
			$usrinfo=posix_getpwnam('root');
			$passwd_root=$usrinfo['passwd'];
			$new_passwd_root=(!empty($_POST['pw_admin'])) ? crypt_md5($_POST['pw_admin']) : $passwd_root;
			$usrinfo=posix_getpwnam('usuario');
			$passwd_user=$usrinfo['passwd'];
			$new_passwd_user=(!empty($_POST['pw_user'])) ? crypt_md5($_POST['pw_user']) : $passwd_user;

			$etcpass='root:' . $new_passwd_root . ':0:0:root:/root:/bin/ash' . "\n";
			$etcpass.='usuario:' . $new_passwd_user . ':500:10:Linux User,,,:/home/usuario:/bin/sh' . "\n";
			file_put_contents(PASSWD, $etcpass);
			if(!empty($_POST['pw_vnc'])) {
				$cmd=LETVNC_SH . ' passwd ' . $_POST['pw_vnc'];
				$ok=lanzaLetScript($cmd);
				if($ok==0) {
					$msg='<div class="avisook"><div><span>&nbsp;Se ha cambiado la contrase&ntilde;a de VNC correctamente.<br />&nbsp;La nueva configuraci&oacute;n se aplicar&aacute; la siguiente vez<br />&nbsp;que se arranque el servicio.</span></div></div>';
				}else{
					$msg='<div class="aviso"><div><span>Ha ocurrido un problema al ejecutar el proceso '.$cmd.'</span></div></div>';
				}
			}
			if ($_POST['autologin']=='true') {
				$cmd=LETAUTOLOGIN_SH . ' auto';
			}else{
				$cmd=LETAUTOLOGIN_SH . ' noauto';
			}
			$ok=lanzaLetScript($cmd);
			if($ok==0) {
				$msg.='<div class="avisook"><div><span>&nbsp;El autologin ha quedado ';
 				if ($_POST['autologin']!='true') {
					$msg.='IN';
					$msg.='HABILITADO.<br />&nbsp;La nueva configuraci&oacute;n se aplicar&aacute; una vez<br />&nbsp;reiniciada la sesi&oacute;n.</span></div></div>';
				}else{
					$msg.='<div class="aviso"><div><span>Ha ocurrido un problema al ejecutar el proceso '.$cmd.'</span></div></div>';
				}
			}
			$sysinfo['autologin']=($_POST['autologin']=='true') ? 1 : 0;
		}
	}

?>
<h1>Contrase&ntilde;as</h1>
<div class="divcnt5"><span class="contbout">Cambiar contrase&ntilde;as</span></div> 
<br />
<form id="form_passwords" name="form_passwords" method="post" action="" onSubmit="return parseForm(this, 'pane_passwords');">
<label>Administrador (root):</label>
<input class="inputlogistic" type="password" id="pw_admin" name="pw_admin" value="" />
Confirmar:
<input class="inputlogistic" type="password" id="pw_admin2" name="pw_admin2" value="" />
<br /><br />
<label>Usuario (usuario):</label>
<input class="inputlogistic" type="password" id="pw_user" name="pw_user" value="" />
Confirmar:
<input class="inputlogistic" type="password" id="pw_user2" name="pw_user2" value="" />
<br /><br />
<label>Servicio VNC:</label>
<input class="inputlogistic" type="password" id="pw_vnc" name="pw_vnc" value="" />
Confirmar:
<input class="inputlogistic" type="password" id="pw_vnc2" name="pw_vnc2" value="" />
<br /><br />
<label>Autologin de usuario:</label>
<input type="checkbox" class="check" id="autologin" name="autologin" value="1" <?php if($sysinfo['autologin']==1) echo 'checked="true"'; ?> />
<br /><br />
<button type="submit" id="chpasswords" name="chpasswords">Aplicar</button>
<br /><br />
</form>
<?php echo $msg; ?>
<br /><br />
<?php } else {
require('unauthorized.inc.php');
} ?>
