<?php
// Copyright (C) 2007                                                                                                                                                               
//                                                                                                                                                                                  
// This program is free software; you can redistribute it and/or modify                                                                                                             
// it under the terms of the GNU General Public License as published                                                                                                                
// by the Free Software Foundation; version 2 only.                                                                                                                                 
//                                                                                                                                                                                  
// This program is distributed in the hope that it will be useful,                                                                                                                  
// but WITHOUT ANY WARRANTY; without even the implied warranty of                                                                                                                   
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                                                                                                                    
// GNU General Public License for more details.                                                                                                                                     
// 
require('init.inc.php');
if($_SESSION['admin']==1) {

	if (isset($_POST['resolution']) && !empty($_POST)) {
		$arr_screen=array("800","1024","1280");
		$arr_timeout=array("5","10","15");
		$resolution=$_POST['resolution'];
		$timeout=$_POST['timeout'];
		$lock=$_POST['scrlock'];
		if ($lock=='true') {
			$cmdlock='ON';
			$lock=1;
		}else{
			$cmdlock='OFF';
			$lock=0;
		}
		$cmd=LETRES_SH."  $arr_screen[$resolution] $arr_timeout[$timeout] $cmdlock";
		$ok=lanzaLetScript($cmd);
		if ($ok==0){
			$sysinfo['resolution']=$arr_screen[$resolution];
			$sysinfo['timeout']=$arr_timeout[$timeout];
			$sysinfo['scrlock']=$lock;
			$msg='<div class="avisook"><div><span>&nbsp;Cambios realizados correctamente.<br />&nbsp;La nueva configuraci&oacute;n se aplicar&aacute; una vez<br />&nbsp;reiniciado el terminal o el servicio WDM.</span></div></div>';
		}else{
			$msg='<div class="aviso"><div><span>Ha ocurrido un problema al ejecutar el proceso '.$cmd.'</span></div></div>';
		}
	}
?>
<h1>Pantalla</h1>
<div class="divcnt5"><span class="contbout">Configuracion de pantalla</span></div>
<?php echo $msg; ?>
<br />
<form id="form_screen" name="form_screen" method="post" action="" onSubmit="return parseForm(this, 'pane_screen');">
<label>Monitor y resolucion:</label>
<select id="resolution" name="resolution">
<option value="0"<?php if($sysinfo['resolution']==800) echo ' selected="true"'; ?>>Monitor de 15", 17" o 19" a 800x600</option>
<option value="1"<?php if($sysinfo['resolution']==1024) echo ' selected="true"'; ?>>Monitor de 15", 17" o 19" a 1024x768</option>
<option value="2"<?php if($sysinfo['resolution']==1280) echo ' selected="true"'; ?>>Monitor de 17" o 19" a 1280x1024</option>
</select>
<br /><br />
<label>Activar salvapantallas despues de</label>
<select id="timeout" name="timeout">
<option value="0"<?php if($sysinfo['timeout']==5) echo ' selected="true"'; ?>>5 minutos</option>
<option value="1"<?php if($sysinfo['timeout']==10) echo ' selected="true"'; ?>>10 minutos</option>
<option value="2"<?php if($sysinfo['timeout']==15) echo ' selected="true"'; ?>>15 minutos</option>
</select>
<br /><br />
<label>Requerir contraseña para desbloquear</label>
<input type="checkbox" class="check" id="scrlock" name="scrlock" value="1" <?php if($sysinfo['scrlock']==1) echo 'checked="true"'; ?> />
<br /><br />
<button type="submit" id="chscreen" name="chscreen">Aplicar</button>
<br /><br />
</form>
<br />
<?php 
}else{
	require('unauthorized.inc.php');
}
?>
