<?php
// Copyright (C) 2007 Programado por Antonio J. Aguilar Bravo
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published
// by the Free Software Foundation; version 2 only.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//

/****************
*
* Clase para la gestion de impresoras
* leerImpresoras
* escribirImpresoras
* getInfo
* addImpresora
* deleteImpresora
* TODO: anadir documentacion de la clase
*
****************/

class Impresoras {

	private $_impresoras = array();
	private $_names = array();
	private $_ports = array();
	private $_spooldirs = array();
	private $_has_filter = array();         /* Tipo disponibles lp,usb,net,lpr*/
	private $_nimpresoras = 0;
	private $_filepath=PRINTCAP;
	private $_custom_filter ='if=/usr/libexec/lprng/filters/ifhp';
	private $_custom_parameters = 'sh:ff_separator';
	private $_fheader = "#\n# /etc/printcap generado por Letconf\n# Por favor utilice esta utilidad para modificar este fichero\n# http://localhost/letconf\n";
	private $_newline_win = "\r\n";
	private $_newline = "\n";
	private $_lp0=FALSE;
	private $_devlp='/dev/lp0';
	private $_devusb='/dev/usblp0';
	private $_spooldir='/var/spool/lpd/';
	private $_usbports=array('lp=/dev/usblp0','lp=/dev/usblp1','lp=/dev/usblp2','lp=/dev/usblp3','lp=/dev/usblp4','lp=/dev/usblp5');
	private $_devusblp=array('/dev/usblp0','/dev/usblp1','/dev/usblp2','/dev/usblp3','/dev/usblp4','/dev/usblp5');

	
	/*private function leerImpresoras($path){*/
	public function __construct($path){
	        $lineas_validas=array();
	        if (file_exists($path)===TRUE){
	        	$lineas = file($path);
		        $this->_filepath=$path;
		        foreach ($lineas as $klinea => $linea) {
	       		 	if (!preg_match('/^#|^\s/',$linea)) $this->_impresoras[] = rtrim($linea);
	        	}
		                foreach($this->_impresoras as $printer){
                			list($nombre,$port,$spool,$filter)=  explode(':',$printer);
			                $this->_names[]=$nombre;
					$this->_ports[]=$port;
		                	$this->_spooldirs[]=$spool;
		                	$this->_has_filter[]=$this->hasFilter($filter);
                		}
	        }else{
	        	$this->_filepath=$path;
	        	/* Al introducir nuevas impresoras crear� el fichero */
	        	//echo '<div class="error">Error no existe el fichero: '.$path.'</div>';
	        	//die;
	        }
	        
	}
	
	private function hasFilter($filter){
		$hasfilter=FALSE;
		if(strstr($filter,'sh')==FALSE) $hasfilter=TRUE;
		return $hasfilter;
		
	}
	
	public function getNetxUsbPort($busyports){
		$availableports=array_diff($this->_usbports,$busyports);
		if (count($availableports)==6) {
			$port=$this->_devusblp[0];
		}else{
			$port=substr(current($availableports),3);
		}
		return $port;
	}


	public function getUsbPorts(){
		return $this->_usbports;
	}
	public function buildSpoolDir($spool){
		if (!empty($spool)) return strtolower($this->_spooldir.$spool);
		else return FALSE;
	}
	
	public function getDevLp0(){
		return $this->_devlp;
	}
	
	/* 31/05/07 Incluido para la restricci�n de una sola impresora por usb */ 
	public function getDevUsb0(){ 
		return $this->_devusb; 
	}

	public function isDefinedLp0(){
		$this->_lp0=FALSE;
		foreach ($this->_ports as $port){
			if (strstr($port,$this->_devlp)==TRUE) $this->_lp0=TRUE;
		}
		return $this->_lp0;
	}
	
	public function setPrnType($type){
		$this->_prntype=$type;
	}
	
	public function getPrnType(){
		return $this->_prntype;
	}
	
	private function getNAD($nad,$type){
		$oc=substr_count($nad, '|');
		switch($oc){
			case 0:
				$name=$nad;
				break;
			case 1:
				list($name,$alias) = explode('|',$nad);
				break;
			case 2:
				list($name,$alias,$description)= explode('|',$nad);
				break;
		}
		switch($type){
			case 1: /*Name*/
				return $name;		
				break;
			case 2: /*Alias*/
				return $alias;
				break;
			case 3: /*Descripcion*/
				return $description;
				break;
		}
			
	}
	
	public function getPorts(){
		return $this->_ports;
	}
	
	public function getSpoolDirs(){
		return $this->_spooldirs;
	}
	
	public function getPrinterNames(){
		return $this->_names;
	}
	
	public function getNprinters(){
		$this->_nimpresoras=count($this->_names);
		return $this->_nimpresoras;
	}
	
	public function getPrinterName($elemNames){
        	return $this->getNAD($elemNames,$type=1);
	}
	
	public function getPrinterAlias($elemNames){
		return $this->getNAD($elemNames,$type=2);
		
	}

	public function getPrinterDescription($elemNames){
		return $this->getNAD($elemNames,$type=3);
	}
	
	public function addSpoolDir($spool){
		$this->_spooldirs[]="sd=$spool";
	}
	
	public function addPort($port){
		$this->_ports[]="lp=$port";
	
	}
	
	public function addHasFilter($type){
		switch ($type){
			case 'lp':
			case 'usb':
			case 'net':
				$this->_has_filter[]=TRUE;
				break;
			case 'lpr':
				$this->_has_filter[]=FALSE;
				break;
		}
	}
	
	public function addNameAliasDesc($name,$alias,$desc){
		$namealiasdesc=$name;
		if (empty($alias)) {
			$namealiasdesc.="|$name";
		}else{
			$namealiasdesc.="|$alias";
		}
		if (!empty($desc)) $namealiasdesc.="|$desc";
		
		$this->_names[]=$namealiasdesc;
	}
	
	public function getSpoolDir($elemSpoolDirs){
		return substr($elemSpoolDirs,3);
	}
	
	public function getPort($elemPorts){
		return substr($elemPorts,3);
	}
	
	public function getImpresoras(){
		return $this->impresoras;
	}
	
	public function getNumImpresoras(){
		return $this->nimpresoras;
	}
	
	public function writePrinters(){
	
		$handle = fopen($this->_filepath, 'w');
	        fwrite($handle, $this->_fheader);
	        fwrite($handle, "# ".$this->getTimestamp());
	        fwrite($handle, $this->_newline);
		for ($i=0; $i < $this->getNprinters(); $i++){
			$printer=$this->buildPrinter($i);
			fwrite($handle, $printer);
			fwrite($handle, $this->_newline);
	        }                   
	       	fclose($handle);
	       	
	       	return TRUE;
	
	}
	
	private function buildPrinter($index){
		$name=$this->_names[$index];
		$port=$this->_ports[$index];
		$spool=$this->_spooldirs[$index];
		
		$parametros=$this->getCustomParameters();
		
		/* seleccion de filtro, solo se aplica a impresoras LPR ('lpr') */
		$has_filter=$this->_has_filter[$index];
		
		if ($has_filter){
			$filtro=$this->getFilter();
			$strprinter="$name:$port:$spool:$filtro:$parametros";
		}else{
			$strprinter="$name:$port:$spool:$parametros";
		}
		
		return $strprinter;
	}
	
        private function getTipoImpresora($strPort){
          	$this->local=FALSE;
          	$this->red=FALSE;
                list($puerto,$ip)=explode('@',$strPort);
                if (empty($ip)){
               		$this->local=TRUE;
                }else{
                        $this->red=TRUE;
                        $this->prnip=$ip;
                }
        }
        
	public function deletePrinter($index){
		$namesTmp=array();
		$portsTmp=array();
		$spoolTmp=array();
		for ($i=0; $i<$this->getNPrinters(); $i++) {
			if ($i!=$index){
				$namesTmp[]=$this->_names[$i];
				$portsTmp[]=$this->_ports[$i];
				$spoolTmp[]=$this->_spooldirs[$i];
				$hasFilterTmp[]=$this->_has_filter[$i];
			}
		}
		$this->_names=$namesTmp;
		$this->_ports=$portsTmp;
		$this->_spooldirs=$spoolTmp;
		$this->_has_filter=$hasFilterTmp;
	}
	
	private function getTimestamp(){
		return date(DATE_RFC822);
	}
	
	private function getFilter(){
		return $this->_custom_filter;
	}
	
	private function getCustomParameters(){
		return $this->_custom_parameters;
	}
		
}

?>
