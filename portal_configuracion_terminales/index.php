<?php require('init.inc.php'); ?>
<?php $xhtml=1;
if($xhtml==1) { ?>
<?php echo '<?xml version="1.0" encoding="UTF-8" ?>' . "\n"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
<?php } else { ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html40/loose.dtd">
<html>
<?php } ?>
<head>
<title>Configuracion de terminales HP - <?php echo $sysinfo['hostname']; ?></title>
<meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
<link rel="shortcut icon" href="/favicon.ico" />
<style type="text/css" media="all">
@import "css/tabs.css";
@import "css/styles.css";
@import "css/layout.css";
</style>
<script type="text/javascript" src="tabs.js"></script>
<script type="text/javascript" src="ajax.js"></script>
<script type="text/javascript" src="md5.js"></script>
<script type="text/javascript" src="crypt_md5.js"></script>
</head>
<body onload='setupPanes("content", "tab_info");'>
<div id="head"></div>
<div id="content">
<?php require('tabs.inc.php'); ?>
<div class="tab-panes">
<div class="divtabs" id="pane_info"></div>
<div class="divtabs" id="pane_login"></div>    
<div class="divtabs" id="pane_network"></div>
<div class="divtabs" id="pane_printers"></div> 
<div class="divtabs" id="pane_date"></div> 
<div class="divtabs" id="pane_services"></div>
<div class="divtabs" id="pane_screen"></div>
<div class="divtabs" id="pane_misc"></div> 
<div class="divtabs" id="pane_storage"></div>
<div class="divtabs" id="pane_logistic"></div>
<div class="divtabs" id="pane_passwords"></div>
</div>
</div>
<div id="foot">
<br />
Version:&nbsp;<?php echo $sysinfo['os']; ?>&nbsp;
<a href="http://www.cherokee-project.com"><img border="0" src="images/cherokee.png" /></a>
<a href="http://www.php.net"><img border="0" src="images/php.png" /></a>
&nbsp;&nbsp;&nbsp;&nbsp;
</div>
</body>
</html>
