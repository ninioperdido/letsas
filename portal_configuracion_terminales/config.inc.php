<?php
// Copyright (C) 2007                                                                                                                                                               
//                                                                                                                                                                                  
// This program is free software; you can redistribute it and/or modify                                                                                                             
// it under the terms of the GNU General Public License as published                                                                                                                
// by the Free Software Foundation; version 2 only.                                                                                                                                 
//                                                                                                                                                                                  
// This program is distributed in the hope that it will be useful,                                                                                                                  
// but WITHOUT ANY WARRANTY; without even the implied warranty of                                                                                                                   
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                                                                                                                    
// GNU General Public License for more details.                                                                                                                                     
// 

define('DELAY', 0);

define('PASSWD', '/etc/passwd');
define('XORG_CONF', '/etc/X11/xorg.conf');
define('WDM_CONFIG', '/etc/X11/wdm/wdm-config');
define('HOSTNAME', '/proc/sys/kernel/hostname');
define('IFCONFIG', '/sbin/ifconfig eth0');
define('GATEWAY', '/sbin/route -n | grep UG');
define('RESOLV', '/etc/resolv.conf');
define('INTERFACES', '/etc/network/interfaces');
define('PRINTCAP','/etc/printcap');
define('XAUTOLOCK','/etc/xautolock');
define('ADLAGENTCONF','/opt/altiris/deployment/adlagent/conf/adlagent.conf');
define('NTPDATECONF','/etc/ntpdate.conf');
define('FSTAB','/etc/fstab');

define('HARDWAREINFO', '/tmp/hardware.info');
define('LOGISTICINFO', '/etc/sysconfig/logistica.info');
define('SMBINFO','/etc/sysconfig/smb.info');
define('IMAGEID', '/etc/sysconfig/image.id');

// Status services
define('STATUS_NETWORK', '/etc/init.d/networking status');
define('STATUS_PRINTER', '/etc/init.d/lpd status');
define('STATUS_VNC', '/etc/init.d/vnc status');
define('STATUS_ALTIRIS', '/etc/init.d/adlagent status');
define('STATUS_XINETD', '/etc/init.d/xinetd status');

// scripts Letconfig
define('LETSCRIPTPATH', '/mnt/letconfig/');
define('LETRES_SH', LETSCRIPTPATH . 'LetRes.sh');
define('LETNETWORK_SH', LETSCRIPTPATH . 'LetNetwork.sh');
define('LETDATETIME_SH', LETSCRIPTPATH . 'LetDateTime.sh');
define('LETALTIRIS_SH', LETSCRIPTPATH . 'LetAltiris.sh');
define('LETNTP_SH', LETSCRIPTPATH . 'LetNtp.sh');
define('LETHWINFO_SH', LETSCRIPTPATH . 'LetHWInfo.sh');
define('LETUSB_SH', LETSCRIPTPATH . 'LetUsb.sh');
define('LETVNC_SH', LETSCRIPTPATH . 'LetVNC.sh');
define('LETAUTOLOGIN_SH', LETSCRIPTPATH . 'LetAutoLogin.sh');
define('ROXSOAP_SH', LETSCRIPTPATH . 'roxSOAP.sh');
define('LETADDICON_SH', LETSCRIPTPATH . 'LetAddIcon.sh');
define('LETREMOVEICON_SH', LETSCRIPTPATH . 'LetRemoveIcon.sh');
define('LETNFS_SH', LETSCRIPTPATH . 'LetNfs.sh');
define('SYSINFO_SH', LETSCRIPTPATH . 'sysinfo.sh');
define('REMOTESTORE', LETSCRIPTPATH . 'XSmbMount.sh');

// Gestion Iconos Desktop
define('ICONPATH','/usr/share/icons/');
define('DESKTOPFILES','/mnt/desktop/');

?>
