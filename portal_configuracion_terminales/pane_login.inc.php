<?php
// Copyright (C) 2007                                                                                                                                                               
//                                                                                                                                                                                  
// This program is free software; you can redistribute it and/or modify                                                                                                             
// it under the terms of the GNU General Public License as published                                                                                                                
// by the Free Software Foundation; version 2 only.                                                                                                                                 
//                                                                                                                                                                                  
// This program is distributed in the hope that it will be useful,                                                                                                                  
// but WITHOUT ANY WARRANTY; without even the implied warranty of                                                                                                                   
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                                                                                                                    
// GNU General Public License for more details.                                                                                                                                     
// 
require('init.inc.php');
?>
<h1>Acceso administrador</h1>
<div class="divcnt5"><span class="contbout">Acceso administrador</span></div>
<br />
<?php
if($_SESSION['admin']==1) {

?>
<form id="form_login" name="form_login" method="post" action="" onSubmit="return logout('pane_login');">
<span style="display: block; width: 100%; text-align: center;">Conectado como administrador. Haga click en el boton para salir.</span>
<br />
<button type="submit">Salir</button>
<br /><br />
<?php } else { ?>
<form id="form_login" name="form_login" method="post" action="" onSubmit="return login('pane_login');">
<label>Contraseña administrador:</label>
<input type="hidden" id="salt" name="salt" value="<?php echo $salt; ?>" />
<input type="hidden" id="challenge" name="challenge" value="<?php echo $challenge; ?>" />
<input type="password" id="passwd" name="passwd" value="" />
<br /><br />
<button type="submit">Acceder</button>
<br /><br />
<?php } ?>
</form>
