<?php
// Copyright (C) 2007                                                                                                                                                               
//                                                                                                                                                                                  
// This program is free software; you can redistribute it and/or modify                                                                                                             
// it under the terms of the GNU General Public License as published                                                                                                                
// by the Free Software Foundation; version 2 only.                                                                                                                                 
//                                                                                                                                                                                  
// This program is distributed in the hope that it will be useful,                                                                                                                  
// but WITHOUT ANY WARRANTY; without even the implied warranty of                                                                                                                   
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                                                                                                                    
// GNU General Public License for more details.                                                                                                                                     
// 
require('init.inc.php');
if($_SESSION['admin']==1) {

	if(!empty($_POST)) {
		$allowvnc=$_POST['allowvnc']; /* == false lanzar servicio vnc lopd */ 
		$service=$_POST['service'];
		$action=$_POST['service_action'];
		$actions = array('start','stop','restart');
		$acciones = array('iniciado', 'parado', 'reiniciado');
		$services = array ('red','impresion','vnc','altiris','xinetd','terminal','wdm');
		$output="";
		if ($services[$service]=='vnc' && $allowvnc=='false' && ($_POST['service_action']==0 || $_POST['service_action']==2)){
			/* inicia el servicio sin cumplir la LOPD */
			if ($_POST['service_action']==0) {
				$cmd=LETVNC_SH." startlopd";
				$retval=lanzaLetScript($cmd,$output);
			}else{
				$cmd=LETVNC_SH." restartlopd";                                                                                                                      
                                $retval=lanzaLetScript($cmd,$output);                                                                                                               
			}
		}else{
			/* todos los demas casos */
			$retval=lanzaInitScript($services[$service], $actions[$action],$output);
		}
		if (strstr($output,'OK')){
			$msg='<div class="avisook"><div><span>&nbsp;Servicio '.strtoupper($services[$service]).' '.$acciones[$action].' correctamente.</span></div></div>';
		}else{
			$msg='<div class="aviso"><div><span>&nbsp;Ha ocurrido un error al realizar '.$actions[$action].' del servicio '.$services[$service].'.</span></div></div>';
		}
	}
servicesinfo();
?>
<h1>Servicios</h1>
<div class="divcnt5"><span class="contbout">Reiniciar servicios</span></div>
<br />
<form id="form_services" name="form_services" method="post" action="">
<label>Servicio:</label>
<select id="service" name="service" onChange="return change_service();">
<option value="0">Red</option>
<option value="1">Impresion</option>
<option value="2">VNC</option>
<option value="3">Altiris</option>
<option value="4">Xinetd</option>
<option value="5">Terminal</option>
<option value="6">WDM</option>
</select>
<input type="hidden" id="service_network" name="service_network" value="<?php echo $servicesinfo['network']; ?>" />
<input type="hidden" id="service_printer" name="service_printer" value="<?php echo $servicesinfo['printer']; ?>" />
<input type="hidden" id="service_vnc" name="service_vnc" value="<?php echo $servicesinfo['vnc']; ?>" />
<input type="hidden" id="service_altiris" name="service_altiris" value="<?php echo $servicesinfo['altiris']; ?>" />
<input type="hidden" id="service_xinetd" name="service_xinetd" value="<?php echo $servicesinfo['xinetd']; ?>" />
<input type="hidden" id="service_terminal" name="service_terminal" value="1" />
<input type="hidden" id="service_wdm" name="service_wdm" value="<?php echo $servicesinfo['wdm']; ?>" />
<input type="hidden" id="service_action" name="service_action" value="" />
<div class="buttons">
<button disabled="disabled" id="start" name="start" onClick="return init_service('pane_services', 0)">Arrancar</button>
<button disabled="disabled" id="stop" name="stop" onClick="return init_service('pane_services', 1)">Parar</button>
<button id="restart" name="restart" onClick="return init_service('pane_services', 2)">Reiniciar</button>
</div>
<br /><br />
<label>Ignorar autorizaci&oacute;n local (VNC):</label>
<input disabled="disabled" type="checkbox" class="check" id="allowvnc" name="allowvnc" value="1" />
<br /><br />
</form>
<?php echo $msg; ?>
<br /><br />
<?php } else {
require('unauthorized.inc.php');
} ?>
