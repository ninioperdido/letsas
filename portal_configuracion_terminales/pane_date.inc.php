<?php
// Copyright (C) 2007                                                                                                                                                               
//                                                                                                                                                                                  
// This program is free software; you can redistribute it and/or modify                                                                                                             
// it under the terms of the GNU General Public License as published                                                                                                                
// by the Free Software Foundation; version 2 only.                                                                                                                                 
//                                                                                                                                                                                  
// This program is distributed in the hope that it will be useful,                                                                                                                  
// but WITHOUT ANY WARRANTY; without even the implied warranty of                                                                                                                   
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                                                                                                                    
// GNU General Public License for more details.                                                                                                                                     
// 
require('init.inc.php');
if($_SESSION['admin']==1) {

	if(!empty($_POST)) {
		$hour=$_POST['hour'];
		$min=$_POST['min'];
		$zone=$_POST['zone'];
		$day=$_POST['day'];
		$month=$_POST['month'];
		$year=$_POST['year'];

		if($zone==1) $zoneinfo='Canarias'; else $zoneinfo='Peninsula';
		$cmdline=LETDATETIME_SH . ' ' . $hour . ' ' . $min . ' ' . $day . ' ' . $month . ' ' . $year . ' ' . $zoneinfo;
		$retval=lanzaLetScript($cmdline,$output);
		if ($retval==0) {
			$msg='<div class="avisook"><div><span>&nbsp;Fecha y hora ajustadas correctamente.</span></div></div>';
		}else{
			$msg='<div class="aviso"><div><span>&nbsp;Se ha producido el siguiente error: $output.</span></div></div>';
		}
	}
	else {
		if (readlink("/etc/localtime")=="/usr/share/zoneinfo/Europe/Peninsula") date_default_timezone_set("CET");
		else date_default_timezone_set("WET");
		list($day, $month, $year, $hour, $min)=explode(' ', date('d m Y H i'));
		}
?>
<h1>Fecha y hora</h1>
<div class="divcnt5"><span class="contbout">Establecer fecha y hora</span></div> 
<br />
<?php echo $msg; ?>
<form id="form_date" name="form_date" method="post" action="" onSubmit="return parseForm(this, 'pane_date');">
<label>Hora:</label>
<?php generate_combobox('id="hour" name="hour"', 0, 23, $hour) ?>
:
<?php generate_combobox('id="min" name="min"', 0, 59, $min) ?>
<br /><br />
<label>Zona horaria:</label>
<select id="zone" name="zone">
<option value="0"<?php if (readlink("/etc/localtime")=="/usr/share/zoneinfo/Europe/Peninsula") echo " selected";?>>Peninsular</option>
<option value="1"<?php if (readlink("/etc/localtime")=="/usr/share/zoneinfo/Europe/Canarias") echo " selected";?>>Canarias</option>
</select>
<br /><br />
<label>Fecha:</label>
<?php generate_combobox('id="day" name="day"', 1, 31, $day) ?>
/
<?php generate_combobox('id="month" name="month"', 1, 12, $month) ?>
/
<?php generate_combobox('id="year" name="year"', 2000, 2020, $year) ?>
<br /><br />
<button type="submit" id="chdate" name="chdate">Aplicar</button>
<br /><br />
</form>
<br />
<?php } else {
require('unauthorized.inc.php');
} ?>
