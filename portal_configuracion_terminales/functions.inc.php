<?php
// Copyright (C) 2007                                                                                                                                                               
//                                                                                                                                                                                  
// This program is free software; you can redistribute it and/or modify                                                                                                             
// it under the terms of the GNU General Public License as published                                                                                                                
// by the Free Software Foundation; version 2 only.                                                                                                                                 
//                                                                                                                                                                                  
// This program is distributed in the hope that it will be useful,                                                                                                                  
// but WITHOUT ANY WARRANTY; without even the implied warranty of                                                                                                                   
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                                                                                                                    
// GNU General Public License for more details.                                                                                                                                     
// 

/*! @function validate_IP
    @abstract Verifica que una direccion IP es valida.
    @param ip IP que queremos verificar.
    @result Devuelve true si la direccion IP es valida y false si no lo es.
*/
function validate_IP($ip) {
if (($lip=ip2long($ip))!==false) if ($ip==long2ip($lip)) return true;
return false;
}

/*! @function validate_host
    @abstract Verifica que un nombre de host es valido.
    @param host Nombre que queremos verificar.
    @result Devuelve true si el nombre es valido y false si no lo es.
*/
function validate_host($host) {
if(validate_IP($host)) return true;
$no_valid='������������';
if(preg_match('/^([A-Za-z0-9-_]+\.?)*$/', $host) && strstr($host,$no_valid)==FALSE ) return true;
return false;
}

/*! @function validate_mask
    @abstract Verifica que una mascara de red es valida.
    @param mask Mascara que queremos verificar.
    @result Devuelve true si la mascara es valida y false si no lo es.
*/
function validate_mask($mask) {
if(!validate_IP($mask)) return false;
$lmask=sprintf("%u", ip2long($mask));
$gmp=gmp_init($lmask);
$bin=gmp_strval($gmp, 2);
$cidr=strlen(str_replace('0', '', $bin));
if(strlen(str_replace('1', '', substr($bin, 0, $cidr-1)))==0) return true;
return false;
}

function generate_tab($tooltip, $accesskey, $name, $label) {
echo '<li>';
echo '<a title="' . $tooltip . '" ';
echo 'accesskey="' . $accesskey . '" ';
echo 'class="hbout" href="#" ';
echo 'onClick="return activatePane(\'pane_' . $name . '\', this)" id="tab_' . $name . '">';
echo $label;
echo '</a></li>';
echo "\n";
}

/*! @function generate_combobox
    @abstract Genera una caja desplegable de seleccion.
    @param attr Atributos que se le pasan a la etiqueta "select".
    @param begin Primer valor seleccionable.
    @param end Ultimo valor seleccionable.
    @param def Valor seleccionado por defecto.
    @result No devuelve ningun valor
*/
function generate_combobox($attr, $begin, $end, $def) {
echo '<select ' . $attr . '>' . "\n";
for($i=$begin; $i<=$end; $i++) {
	echo '<option value="';
	printf("%02d", $i);
	echo '"';
	if($i==$def) echo ' selected="true"';
	echo '>';
	printf("%02d", $i);
	echo '</option>' . "\n";
}
echo '</select>' . "\n";
}

/*! @function crypt_md5
    @abstract Encripta una clave con un salt aleatorio.
    @result Devuelve la clave encriptada en crypt-md5
*/
function crypt_md5($passwd) {
$salt=substr(str_shuffle("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"), 0, 8);
return crypt($passwd, '$1$' . $salt . '$');
}

/*! @function sysinfo
    @abstract Genera un array global con informacion sobre el sistema.
    @result No devuelve ningun valor
*/
function sysinfo() {
global $sysinfo;
$sysinfo=array();

if(file_exists(HARDWAREINFO)===FALSE) lanzaLetScript(LETHWINFO_SH);

// Hardware
$info=@join('', file(HARDWAREINFO));
$sysinfo['vendor']=(preg_match('/Vendor: "(.*)"/', $info, $match)) ? $match[1] : 'Desconocido';
$sysinfo['version']=(preg_match('/Version: "(.*)"/', $info, $match)) ? $match[1] : 'Desconocido';
$sysinfo['date']=(preg_match('/Date: "(\d{2})\/(\d{2})\/(\d{4})"/', $info, $match)) ? $match[2] . '/' . $match[1] . '/' . $match[3] : 'Desconocido';
$sysinfo['manufacturer']=(preg_match('/Manufacturer: "(.*)"/', $info, $match)) ? $match[1] : 'Desconocido';
$sysinfo['product']=(preg_match('/Product: "(.*)"/', $info, $match)) ? $match[1] : 'Desconocido';
$sysinfo['serial']=(preg_match('/Serial: "(.*)"/', $info, $match)) ? $match[1] : 'Desconocido';
$sysinfo['cpu']=(preg_match('/Processor Version: "(.*)"/', $info, $match)) ? $match[1] : 'Desconocido';
$sysinfo['speed']=(preg_match('/Current Speed: (.*)/', $info, $match)) ? $match[1] : 'Desconocido';
$sysinfo['ram']=(preg_match_all('/Size: (.*)/', $info, $match)) ? $match[1][1] : 'Desconocido';
$sysinfo['flash']=(preg_match('/Device: "(.*MB ATA Flash Disk)"/', $info, $match)) ? $match[1] : 'Desconocido';
$sysinfo['os']=@join('', file(IMAGEID));

// Resolution
$res=readlink(XORG_CONF);
$sysinfo['resolution']=(preg_match('/^.*\.(\d+)$/', $res, $match)) ? $match[1] : 0;

$xautolock=@join('', file(XAUTOLOCK));
$sysinfo['timeout']=(preg_match('/TIME=(.*)/', $xautolock, $match)) ? $match[1] : '10';
$sysinfo['scrlock']=(preg_match('/BLOCK=no/', $xautolock, $match)) ? 0 : 1;

// Networking
$sysinfo['hostname']=@trim(join('', file(HOSTNAME)));
exec(IFCONFIG, $output); 
$ifconfig=join('', $output);
$sysinfo['ip']=(preg_match('/inet addr:(\d+\.\d+\.\d+\.\d+)/', $ifconfig, $match)) ? $match[1] : '';
$sysinfo['mask']=(preg_match('/Mask:(\d+\.\d+\.\d+\.\d+)/', $ifconfig, $match)) ? $match[1] : '';
exec(GATEWAY, $output); 
$route=join('', $output);
$sysinfo['gw']=(preg_match('/0\.0\.0\.0\s+(\d+\.\d+\.\d+\.\d+)\s+0\.0\.0\.0/', $route, $match)) ? $match[1] : '';
$sysinfo['mac']=(preg_match('/HWaddr\s(\w+\:\w+\:\w+\:\w+\:\w+\:\w+)\s/', $ifconfig, $match)) ? $match[1] : '';
$resolv=@join('', file(RESOLV));

$sysinfo['dns1']=(preg_match_all('/nameserver (\d+\.\d+\.\d+\.\d+)/', $resolv, $match)) ? $match[1][0] : '';
$sysinfo['dns2']=(array_key_exists(1, $match[1])) ? $match[1][1] : '';
$interfaces=@join('', file(INTERFACES));
if(preg_match('/iface eth0 inet static/', $interfaces, $match)) {
$sysinfo['dhcp']=0;
$sysinfo['ifip']=(preg_match('/address (\d+\.\d+\.\d+\.\d+)/', $interfaces, $match)) ? $match[1] : '';
$sysinfo['ifmask']=(preg_match('/netmask (\d+\.\d+\.\d+\.\d+)/', $interfaces, $match)) ? $match[1] : '';
$sysinfo['ifgw']=(preg_match('/gateway (\d+\.\d+\.\d+\.\d+)/', $interfaces, $match)) ? $match[1] : '';
} else {
$sysinfo['dhcp']=1;
$sysinfo['ifip']='';
$sysinfo['ifmask']='';
$sysinfo['ifgw']='';
}

// Altiris
$altiris=@join('', file(ADLAGENTCONF));
$sysinfo['altiris']=(preg_match('/TcpAddr=(.*)/', $altiris, $match)) ? $match[1] : '';
$sysinfo['altirisp']=(preg_match('/TcpPort=(\d+)/', $altiris, $match)) ? $match[1] : '';
// NTP
$ntpdate=@join('', file(NTPDATECONF));
$sysinfo['ntp']=(preg_match('/NTPHOST=(.*)/', $ntpdate, $match)) ? $match[1] : '';

// USB
$fstab=@join('', file(FSTAB));
$sysinfo['usb']=(preg_match('/\/home\/usuario\/USB/', $fstab, $match)) ? 1 : 0;

// Autologin
$autologin=readlink(WDM_CONFIG);
$sysinfo['autologin']=($autologin=='/etc/X11/wdm/wdm-config.auto') ? 1 : 0;

}

function writeLogisticaTemplate() {
        $status="";
        $output="";
        $status=lanzaLetScript(SYSINFO_SH,$output);
}

function writeLogistica($vctLogistica) {
        $newline_win = "\r\n";
        $newline = "\n";
        global $logisticinfo;
        $logistica_items=array('Numero de serie del terminal:log_term_serial:'.$vctLogistica['log_term_serial'].$newline,
                                'Marca y modelo del terminal:log_term_model:'.$vctLogistica['log_term_model'].$newline,
                                'Numero de inventario del terminal:log_term_inven:'.$vctLogistica['log_term_inven'].$newline,
                                'Fabricante de BIOS:log_bios_man:'.$logisticinfo['log_bios_man'].$newline,
                                'Version de BIOS:log_bios_ver:'.$logisticinfo['log_bios_ver'].$newline,
                                'Fecha de BIOS:log_bios_date:'.$logisticinfo['log_bios_date'].$newline,
                                'Modelo de Procesador:log_sys_proc:'.$logisticinfo['log_sys_proc'].$newline,
                                'Memoria RAM:log_system_mem:'.$logisticinfo['log_system_mem'].$newline,
                                'Almacenamiento:log_system_stor:'.$logisticinfo['log_system_stor'].$newline,
                                'Version de sistema:log_system_ver:'.$logisticinfo['log_system_ver'].$newline,
                                'Version de kernel:log_kernel_ver:'.$logisticinfo['log_kernel_ver'].$newline,
                                'Nombre del terminal:log_net_hostname:'.$logisticinfo['log_net_hostname'].$newline,
                                'Direccion IP:log_net_ip:'.$logisticinfo['log_net_ip'].$newline,
                                'Direccion MAC:log_net_mac:'.$logisticinfo['log_net_mac'].$newline,
                                'Mascara de red:log_net_mask:'.$logisticinfo['log_net_mask'].$newline,
                                'Puerta de enlace:log_net_gateway:'.$logisticinfo['log_net_gateway'].$newline,
                                'Servidores DNS:log_net_dns:'.$logisticinfo['log_net_dns'].$newline,
                                'Numero de serie del monitor:log_monitor_serial:'.$vctLogistica['log_monitor_serial'].$newline,
                                'Marca y modelo del monitor:log_monitor_model:'.$vctLogistica['log_monitor_model'].$newline,
                                'Numero de inventario del monitor:log_monitor_inven:'.$vctLogistica['log_monitor_inven'].$newline,
                                'Numero de serie del lector sc:log_sc_serial:'.$vctLogistica['log_sc_serial'].$newline,
                                'Marca y modelo del lector sc:log_sc_model:'.$vctLogistica['log_sc_model'].$newline,
                                'Numero de inventario del lector sc:log_sc_inven:'.$vctLogistica['log_sc_inven'].$newline,
                                'Numero de serie del teclado:log_keyboard_serial:'.$vctLogistica['log_keyboard_serial'].$newline,
                                'Marca y modelo del teclado:log_keyboard_model:'.$vctLogistica['log_keyboard_model'].$newline,
                                'Numero de inventario del teclado:log_keyboard_inven:'.$vctLogistica['log_keyboard_inven'].$newline,
                                'Numero de serie de la impresora:log_printer_serial:'.$vctLogistica['log_printer_serial'].$newline,
                                'Marca y modelo de la impresora:log_printer_model:'.$vctLogistica['log_printer_model'].$newline,
                                'Numero de inventario de la impresora:log_printer_inven:'.$vctLogistica['log_printer_inven'].$newline,
                                'Puerto de impresora:log_printer_port:'.$logisticinfo['log_printer_port'].$newline,
                                'Tecnico intalador:log_installer:'.$logisticinfo['log_installer'].$newline,
                                'Fecha instalacion:log_inst_date:'.$logisticinfo['log_inst_date'].$newline,
                                'Observaciones:log_comments:'.$vctLogistica['log_comments'].$newline,
                                'Ubicacion_fisica:log_location:'.$vctLogistica['log_location'].$newline,
                                'Organizacion:log_organization:'.$vctLogistica['log_organization'].$newline,
                                'Departamento:log_department:'.$vctLogistica['log_department'].$newline,
                                'Edificio:log_building:'.$vctLogistica['log_building'].$newline,
                                'Centro:log_center:'.$vctLogistica['log_center'].$newline,
                                'Provincia:log_province:'.$vctLogistica['log_province'].$newline,
                                'Persona de contacto:log_contact_name:'.$vctLogistica['log_contact_name'].$newline,
                                'Telefono de contacto:log_contact_phone:'.$vctLogistica['log_contact_phone'].$newline,
                                'Asignado a:log_assigned:'.$vctLogistica['log_assigned'].$newline,
                                'ID de usuario:log_userid:'.$vctLogistica['log_userid'].$newline,
                                'Telefono:log_phone:'.$vctLogistica['log_phone'].$newline,
                                'Soportado por:log_supportedby:'.$vctLogistica['log_supportedby'].$newline,
                                'Telefono soporte:log_support_phone:'.$vctLogistica['log_support_phone'].$newline,
                                'Fecha ultima modificacion:log_mod_date:'.$vctLogistica['log_mod_date'].$newline);
        file_put_contents(LOGISTICINFO,$logistica_items);
}

/*! @function logisticinfo
    @abstract Genera un array global con informacion logistica.
    @result No devuelve ningun valor
*/
function logisticinfo() {
	global $logisticinfo;
	$logisticinfo=array();
	if(file_exists(LOGISTICINFO)===FALSE) {
		writeLogisticaTemplate();
	}

	if(($fp=@fopen(LOGISTICINFO, 'r'))!==FALSE) {
		while(!feof($fp)) {
			$buff=fgets($fp, 1024);
			list($desc, $index, $value)=explode(':', $buff);
			$logisticinfo[$index]=rtrim($value);
		}
		fclose($fp);
	}
}

/*! @function servicesinfo
    @abstract Genera un array global con informacion sobre los servicios.
    @result No devuelve ningun valor
*/
function servicesinfo() {
global $servicesinfo;
$servicesinfo=array();
exec(STATUS_NETWORK, $output_network); 
$status_network=join('', $output_network);

//solo reiniciar
$servicesinfo['network']=1;
exec(STATUS_PRINTER, $output_printer); 
$status_printer=join('', $output_printer);
$servicesinfo['printer']=(preg_match('/\d+/', $status_printer, $match)) ? 1 : 0;
exec(STATUS_VNC, $output_vnc); 
$status_vnc=join('', $output_vnc);
$servicesinfo['vnc']=(preg_match('/\d+/', $status_vnc, $match)) ? 1 : 0;
exec(STATUS_ALTIRIS, $output_altiris); 
$status_altiris=join('', $output_altiris);
$servicesinfo['altiris']=(preg_match('/\d+/', $status_altiris, $match)) ? 1 : 0;
exec(STATUS_XINETD, $output_xinetd); 
$status_xinetd=join('', $output_xinetd);
$servicesinfo['xinetd']=(preg_match('/\d+/', $status_xinetd, $match)) ? 1 : 0;

// wdm solo reiniciar
$servicesinfo['wdm']=1;
}

?>
