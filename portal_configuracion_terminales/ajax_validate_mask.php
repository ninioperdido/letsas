<?php
// Copyright (C) 2007                                                                                                                                                               
//                                                                                                                                                                                  
// This program is free software; you can redistribute it and/or modify                                                                                                             
// it under the terms of the GNU General Public License as published                                                                                                                
// by the Free Software Foundation; version 2 only.                                                                                                                                 
//                                                                                                                                                                                  
// This program is distributed in the hope that it will be useful,                                                                                                                  
// but WITHOUT ANY WARRANTY; without even the implied warranty of                                                                                                                   
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                                                                                                                    
// GNU General Public License for more details.                                                                                                                                     
// 
require('init.inc.php');
$mask=$_POST['mask'];
if(validate_mask($mask)){
	 echo '<span style="color: #360;">M&aacute;scara de red v&aacute;lida.</span>'; 
}else{ 
	echo '<span style="color: #f00;">La m&aacute;scara de red no es v&aacute;lida</span>';
}
?>
