<?php
// Copyright (C) 2007 Programado por Antonio J. Aguilar Bravo
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published
// by the Free Software Foundation; version 2 only.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//


/*! @function lanzaLetScript                                                    
    @abstract Ejecuta el comando Verifica que una direccion IP es valida.       
    @param cmd Cadena que contiene el comando a ejecutar.                       
    @result Devuelve la salida del comando ejecutado.                           
*/                                                                              
function lanzaLetScript($cmd,&$output=""){ 
	exec($cmd,$output,$status);
	usleep(250000);
	$output=join('',$output);
	return $status; 
}                                                    


/*! @function lanzaInitScript
    @abstract Ejecuta scripts de servicios.
    @param service Servicio a iniciar / reiniciar / parar .
    @param action Accion a realizar start|restart|stop
    @result Devuelve la salida del comando ejecutado.
*/
function lanzaInitScript($service,$action,&$output=""){
	$status;
	$output;
	
	switch ($service) {
		case "red":
			$script="networking";
			break;
		case "impresion":
			$script="lpd";
			break;
		case "vnc":
			$script="vnc";
			break;
		case "altiris":
			$script="adlagent";
			break;
		case "xinetd":
			$script="xinetd";
			break;
		case "terminal":
			$script="terminal";
			break;
		case "wdm":
			$script="wdm";
			break;
	}
	
	$initPath="/etc/init.d";
	$cmd="$initPath/$script $action";
	exec($cmd,$output,$status);
	usleep(250000);
	$output=join('',$output);
	return $status;
}


/*! @function validate_spoolname
    @abstract Verifica que un nombre de host es valido.
    @param host Nombre que queremos verificar.
    @result Devuelve true si el nombre es valido y false si no lo es.
*/

function isValidSpoolName($name) {
	if (empty($name)) return false;
        if (preg_match('/^([A-Za-z0-9-_]+\.?)*$/', $name)) return true;
        return false;
}
            
function duplicateName($name,$names){
	foreach ($names as $item){
		if (strcmp($name,getName($item,1))==0) return true;
	}
	return false;
}


function getName($nad,$type){
	$oc=substr_count($nad, '|');
		switch($oc){
			case 0:
				$name=$nad;
				break;
			case 1:
				list($name,$alias) = explode('|',$nad);
				break;
			case 2:
				list($name,$alias,$description)= explode('|',$nad);
				break;
		}
		switch($type){
			case 1: /*Name*/
				return $name;
				break;
			case 2: /*Alias*/
				return $alias;
				break;
			case 3: /*Descripcion*/
				return $description;
				break;
		}
}

function validPort($port){
	$valid=false;
	if ($port > 0 && $port <= 65536) $valid=true;
	return $valid;
}

function writeSMBInfo($smb_type,$smb_user,$smb_pass,$smb_dom,$smb_ip,$smb_path) {
        $newline_win = "\r\n";
        $newline = "\n";
        if (strstr($smb_path,'${SMB_USER}')==FALSE){ /* si $smb_path usa %user% debe pedir la credenciales al hacer la conexi�n */
        	$smbinfo=array( 'SMB_TYPE='.$smb_type.$newline,
	        		'SMB_USER='.$smb_user.$newline,
		        	'SMB_PASS='.$smb_pass.$newline,
			        'SMB_DOM='.$smb_dom.$newline,
			        'SMB_IP='.$smb_ip.$newline,
		        	'SMB_PATH='.$smb_path.$newline );	
        }else{
        	$smbinfo=array( 'SMB_TYPE='.$smb_type.$newline,
			        'SMB_DOM='.$smb_dom.$newline,
			        'SMB_IP='.$smb_ip.$newline,
			        'SMB_PATH='.$smb_path.$newline );	
        }
        
	return file_put_contents(SMBINFO,$smbinfo); 
}

function getIconDesktop($app_name,$app_path,$app_icon,$app_comment=""){
        $newline_win = "\r\n";
        $newline = "\n";
	$icon_desktop=array("[Desktop Entry]".$newline,
			    "Name=$app_name".$newline,
			    "Exec=$app_path".$newline,
			    "Icon=$app_icon".$newline,
			    "Comment=$app_comment".$newline);
	return $icon_desktop;
}

function removeNL($string) {
	/* Processes \r\n's first so they aren't converted twice. */
	$order   = array("\r\n", "\n", "\r");
	$replace = ' ';
	return str_replace($order, $replace, $string);
}
?>
