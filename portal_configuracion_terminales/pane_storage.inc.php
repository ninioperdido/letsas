<?php
// Copyright (C) 2007                                                                                                                                                               
//                                                                                                                                                                                  
// This program is free software; you can redistribute it and/or modify                                                                                                             
// it under the terms of the GNU General Public License as published                                                                                                                
// by the Free Software Foundation; version 2 only.                                                                                                                                 
//                                                                                                                                                                                  
// This program is distributed in the hope that it will be useful,                                                                                                                  
// but WITHOUT ANY WARRANTY; without even the implied warranty of                                                                                                                   
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                                                                                                                    
// GNU General Public License for more details.                                                                                                                                     
// 
require('init.inc.php');
if($_SESSION['admin']==1) {

	if(!empty($_POST)){
		$error_empty_fields=FALSE;
		$disable_remote_conf=FALSE;
		
		$store=$_POST['store'];
		switch($store){
			case '0': /*local*/
				$msg_off="El almacenamiento para dispositivos USB ha quedado INHABILITADO.";
				$msg_on="El almacenamiento para dispositivos USB ha quedado HABILITADO.";
				$store=$_POST['store'];
				$usb=($_POST['usb']=='true')? 'ON' : 'OFF';
				$msg=($usb=='ON')? $msg_on : $msg_off;
				$cmd=LETUSB_SH." $usb";
				$ok=lanzaLetScript($cmd,$output);
				
				if ($ok==0) {
					$sysinfo['usb']=($_POST['usb']=='true') ? 1 : 0;
					$msg="<div class=\"avisook\"><div><span>$msg</span></div></div>";
				}else{
					$msg='<div class="aviso"><div><span>Ha ocurrido un problema al ejecutar el proceso: '.$cmd.' - '.$output.'<span></div></div>';
				}
				break;
			case '1': /*remoto*/
				$smb_type = ($_POST['smb_type']==0)? 'SMB': 'NFS';
				$smb_user = rtrim($_POST['smb_user']);
				if (strcmp($smb_user,'%terminal%')==0) $smb_user='${SMB_HOST}';
				$smb_pass = $_POST['smb_pass'];
				$smb_dom  = rtrim($_POST['smb_dom']);
				$smb_ip   = rtrim($_POST['smb_ip']);
				$patterns=array('%user%','%terminal%');
				$replace=array('${SMB_USER}','${SMB_HOST}');
				$smb_path=str_replace($patterns, $replace, rtrim($_POST['smb_path']));
				
				if (empty($smb_ip) || empty($smb_dom) || empty($smb_path)) $error_empty_fileds=TRUE;
				if (empty($smb_ip) && empty($smb_dom) && empty($smb_path)) $disable_remote_conf=TRUE;
				
				if ($error_empty_fileds===TRUE && $disable_remote_icon===FALSE) {
					$msg='<div class="avisowarn"><div><span>Alguno de los campos obligatorios est&aacute;n vac&iacute;os.</span></div></div>';
					break;
				}
				
				if ($disable_remote_conf===TRUE){
				// eliminar fichero de configuracion
					
					$smb_user="";
					$smb_path="";
					
				      	if (file_exists(SMBINFO)){
				      		unlink(SMBINFO);
				      	}
				      	
				      	// eliminar entrada en fstab para nfs
				      	$cmd_nfs=LETNFS_SH." OFF";
				      	$ok_nfs=lanzaLetScript($cmd_nfs,$output);
				      	
					// Elminar icono si existe
					$remoto_desktop_file=DESKTOPFILES.'remoto.desktop';
                                        $cmd_remove=ROXSOAP_SH." remove $remoto_desktop_file 2>&1";
                                        $ok_remove=lanzaLetScript($cmd_remove,$output_remove);
                                        $msg_ok=FALSE;
                                        switch($ok_remove) {
                                        	case 0:
							if (strstr($output_remove,'CRITICAL')!=FALSE){
								/* logado el usuario */
								/* eliminar del fichero pb_Desktop */
								$cmd_pb=LETREMOVEICON_SH." remoto.desktop";
								$ok_pb=lanzaLetScript($cmd_pb,$output_pb);
								if ($ok_pb==0) $msg_ok=TRUE;
							}else{
								//root logado, icono eliminado en cmd_remove
								$msg_ok=TRUE;
							}
							
                                        		break;
                                        	case 100:
                                        		/* no hay usuario logado eliminar del fichero pb_Desktop */
                                        		$cmd_pb=LETREMOVEICON_SH." remoto.desktop";
                                        		$ok_pb=lanzaLetScript($cmd_pb,$output_pb);
                                        		if ($ok_pb==0) $msg_ok=TRUE;
                                        		break;
                                        	default:
                                        		break;
                                        }
					
					if($msg_ok==TRUE){
						$msg='<div class="avisook"><div><span>Configuraci&oacute;n remota eliminada.</span></div></div>';
					}else{
						$msg='<div class="avisowarn"><div><span>No se ha podido eliminar el icono.</span></div></div>';	
					}
					break;
				}
				
				if (writeSMBInfo($smb_type,$smb_user,$smb_pass,$smb_dom,$smb_ip,$smb_path)==FALSE){
					$msg='<div class="aviso"><div><span>Error al generar el fichero '.SMBINFO.'</span></div></div>';
				}else{
					/*generamos el icono*/
					$app_name="Unidad Remota";
					$app_path=REMOTESTORE;
					$app_icon=ICONPATH."Tango/48x48/places/folder-remote.png";
					$application_desktop = getIconDesktop($app_name,$app_path,$app_icon);
					$remoto_desktop_file=DESKTOPFILES.'remoto.desktop';
					file_put_contents($remoto_desktop_file,$application_desktop);
					
					/*lanzamos el icono*/
					if (file_exists($remoto_desktop_file)==TRUE){
					
						// si el recurso es nfs a�adimos el soporte en el fstab
						if ($smb_type=='NFS'){
							//eliminamos por si existe modificacion en los datos
							$cmd_nfs_off=LETNFS_SH." OFF";
							$ok_nfs_off=lanzaLetScript($cmd_nfs_off,$output_nfs_off);
							//a�adimos la entrada
							$cmd_nfs_on=LETNFS_SH." ON";
							$ok_nfs_on=lanzaLetScript($cmd_nfs_on,$output_nfs_on);
						}
					
						$y=0;
						$x=0;
						
						/* eliminamos y a�adimos */
						$cmd=ROXSOAP_SH." remove $remoto_desktop_file 2>&1";
						$ok=lanzaLetScript($cmd,$output);
						
						switch ($ok) {
							case 0:
								/**/
								if (strstr($output,'CRITICAL')==FALSE){
									/* logado root, a�adimos el icono */
									$cmd=ROXSOAP_SH." add $remoto_desktop_file $x $y \"$app_name\" 2>&1";
									$ok=lanzaLetScript($cmd,$output);
									
									$msg='<div class="avisook"><div><span>La configuraci&oacute;n para el acceso a unidades remotas ya est&aacute; disponible en el escritorio.</span></div></div>';
								}else{
									/* logado el usuario */	
									/* a�adir linea pb_desktop */
									$icon="<icon x=\\\"$x\\\" y=\\\"$y\\\" label=\\\"$app_name\\\">$remoto_desktop_file</icon>";
									//$icon='<icon x="'.$x.'" y="'.$y.'" label="'.$app_name.'">'.$remoto_desktop_file.'</icon>';
									$cmd_icon=LETADDICON_SH." \"  $icon\"";
									$ok_icon=lanzaLetScript($cmd_icon,$output);
									if ($ok_icon==0){
										$msg='<div class="avisook"><div><span>La configuraci&oacute;n para el acceso a unidades remotas estar&aacute; disponible una vez reiniciado el servicio WDM.</span></div></div>';
									}else{
										$msg='<div class="avisowarn"><div><span>El icono de acceso remoto ya existe.</span></div></div>';
									}
								}
								break;
							case 100:
								/* no hay usuario logado */
								/* a�adir linea pb_desktop */
								$icon="<icon x=\\\"$x\\\" y=\\\"$y\\\" label=\\\"$app_name\\\">$remoto_desktop_file</icon>";
								$cmd_icon=LETADDICON_SH." \"  $icon\"";
								//print_r($cmd);
								$ok_icon=lanzaLetScript($cmd_icon,$output);
								if ($ok_icon==0) {
									$msg='<div class="avisook"><div><span>La configuraci&oacute;n para el acceso a unidades remotas estar&aacute; disponible una vez iniciada la sesi&oacute;n.</span></div></div>';
								}else{
									$msg='<div class="avisowarn"><div><span>El icono de acceso remoto ya existe.</span></div></div>';
								}
								break;
							default:
								/*error no controlado*/
								break;
						}
					}else{
						$msg='<div class="aviso"><div><span>Error al generar el fichero $remoto_desktop_file</span></div></div>';
					}
				}
				break;
		}
			
	}

	if (file_exists(SMBINFO)){
		$remote_info=join('', file(SMBINFO));
		$smb_type=(preg_match('/SMB_TYPE=(.*)\s/', $remote_info, $matches)) ? rtrim($matches[1]) : '';
		$smb_user=(preg_match('/SMB_USER=(.*)\s/', $remote_info, $matches)) ? rtrim($matches[1]) : '';
		$smb_pass=(preg_match('/SMB_PASS=(.*)\s/', $remote_info, $matches)) ? rtrim($matches[1]) : '';
		$smb_dom=(preg_match('/SMB_DOM=(.*)\s/', $remote_info, $matches)) ? rtrim($matches[1]) : '';
		$smb_ip=(preg_match('/SMB_IP=(.*)\s/', $remote_info, $matches)) ? rtrim($matches[1]) : '';
		$smb_path=(preg_match('/SMB_PATH=(.*)\s/', $remote_info, $matches)) ? rtrim($matches[1]) : '';
		
		$smb_user=rtrim(str_replace('${SMB_HOST}','%terminal%',$smb_user));
		$patterns=array('${SMB_USER}','${SMB_HOST}');
		$replace=array('%user%','%terminal%');
		$smb_path=str_replace($patterns,$replace,rtrim($smb_path));
	}else{
		$smb_user="";
		$smb_pass="";
	}
?>
<h1>Almacenamiento</h1>
<div class="divcnt5"><span class="contbout">Almacenamiento</span></div>
<?php echo $msg; ?>
<br />
<form id="form_storage" name="form_storage" method="post" action="" onSubmit="return parseForm(this, 'pane_storage');">
<label>Almacenamiento en:</label>
<select id="store" name="store" onChange="return change_storage();">
<option value="0">Local</option>
<option value="1">Remoto</option>
</select>
<br /><br />
<label>Permitir USB:</label>
<input type="checkbox" class="check" id="usb" name="usb" value="1" <?php if($sysinfo['usb']==1) echo 'checked="true" ';?>/>
<br /><br />
<label>Tipo de recurso remoto:</label>
<select disabled="disabled" id="smb_type" name="smb_type">
<option value="0" <?php if ($smb_type=='SMB') echo 'selected="true"';?>>SMB</option>
<option value="1" <?php if ($smb_type=='NFS') echo 'selected="true"';?>>NFS</option>
</select>
<br /><br />
<label>Usuario:</label>
<input class="inputancho" disabled="disabled" type="text" id="smb_user" name="smb_user" value="<?php echo $smb_user; ?>" />
<br />
<label>&nbsp;</label><span class="comentario">(Vac&iacute;o para que se solicite en el acceso)</span>
<br /><br />
<label>Clave:</label>
<input class="inputancho" disabled="disabled" type="password" id="smb_pass" name="smb_pass" value="<?php echo $smb_pass; ?>" />
<br />
<label>&nbsp;</label><span class="comentario">(Vac&iacute;o para que se solicite en el acceso)</span>
<br /><br />
<label>Dominio:</label>
<input class="inputancho" disabled="disabled" type="text" id="smb_dom" name="smb_dom" value="<?php echo $smb_dom; ?>" />
<br /><br />
<label>Direccion IP:</label>
<input class="inputancho" disabled="disabled" type="text" id="smb_ip" name="smb_ip" value="<?php echo $smb_ip; ?>" onblur="validateIPHost('smb_ip', 'msgsmbhost')"/>
<div class="divmsg" id="msgsmbhost"></div>
<br /><br />
<label>Ruta:</label>
<input class="inputancho2" disabled="disabled" type="text" id="smb_path" name="smb_path" value="<?php echo $smb_path; ?>" />
<br /><br />
<button type="submit" id="chstorage" name="chstorage">Aplicar</button>
<br /><br />
</form>
<br />
<?php } else {
require('unauthorized.inc.php');
} ?>
