<?php
// Copyright (C) 2007                                                                                                                                                               
//                                                                                                                                                                                  
// This program is free software; you can redistribute it and/or modify                                                                                                             
// it under the terms of the GNU General Public License as published                                                                                                                
// by the Free Software Foundation; version 2 only.                                                                                                                                 
//                                                                                                                                                                                  
// This program is distributed in the hope that it will be useful,                                                                                                                  
// but WITHOUT ANY WARRANTY; without even the implied warranty of                                                                                                                   
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                                                                                                                    
// GNU General Public License for more details.                                                                                                                                     
// 
require('init.inc.php');
$host=$_POST['host'];
if (validate_host($host)) { /* hostname valido */
	$iptmp=gethostbynamel($host);
	if ($iptmp==FALSE){
		echo '<span style="color: #360;">Nombre de host v&aacute;lido.</span>';
	}else{
		echo '<span style="color: #360;">Nombre de host v&aacute;lido. IP: '.$iptmp[0].'</span>';
	}
}else{
	echo '<span style="color: #f00;">Nombre de host no v&aacute;lido.</span>';
}

?>
