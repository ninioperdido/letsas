<?php require_once('init.inc.php'); ?>
<ul class="tabs">
<?php
if($_SESSION['admin']==1 || $_SERVER['SERVER_ADDR']==$_SERVER['REMOTE_ADDR'] || $_SERVER['SERVER_NAME']=='localhost') {
generate_tab('Ver informacion del sistema', 'T', 'info', 'Sis<span>t</span>ema');
}
generate_tab('Acceso administrador', 'A', 'login', '<span>A</span>cceso');
if($_SESSION['admin']==1) {
generate_tab('Cambiar la configuracion de red', 'R', 'network', '<span>R</span>ed');
generate_tab('Cambiar la configuracion de las impresoras', 'I', 'printers', '<span>I</span>mpresoras');
generate_tab('Cambiar la fecha y la hora', 'F', 'date', '<span>F</span>echa y hora');
generate_tab('Arrancar, parar y reiniciar servicios', 'S', 'services', '<span>S</span>ervicios');
generate_tab('Cambiar la configuracion de pantalla', 'P', 'screen', '<span>P</span>antalla');
generate_tab('Cambiar la configuracion de varios servicios', 'V', 'misc', '<span>V</span>arios');
generate_tab('Cambiar tipo de almacenamiento', 'A', 'storage', '<span>A</span>lmacenamiento');
generate_tab('Datos logisticos', 'L', 'logistic', '<span>L</span>ogistica');
generate_tab('Gestionar contrase&ntilde;as', 'C', 'passwords', '<span>C</span>ontrase&ntilde;as');
}
?>
</ul>
