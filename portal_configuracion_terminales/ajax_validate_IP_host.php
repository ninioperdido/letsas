<?php
// Copyright (C) 2007                                                                                                                                                               
//                                                                                                                                                                                  
// This program is free software; you can redistribute it and/or modify                                                                                                             
// it under the terms of the GNU General Public License as published                                                                                                                
// by the Free Software Foundation; version 2 only.                                                                                                                                 
//                                                                                                                                                                                  
// This program is distributed in the hope that it will be useful,                                                                                                                  
// but WITHOUT ANY WARRANTY; without even the implied warranty of                                                                                                                   
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                                                                                                                    
// GNU General Public License for more details.                                                                                                                                     
// 
require('init.inc.php');
$iphost=$_POST['iphost'];
if(validate_IP($iphost)){ /* ip valida */
	$hostname=gethostbyaddr($iphost);
	if ($hostname==$iphost){
		echo '<span style="color: #360;">La direccion IP es correcta.</span>'; 
	}else{
		echo '<span style="color: #360;">La direccion IP es correcta. Host: '.$hostname.'</span>';
	}
}elseif (validate_host($iphost)) { /* hostname valido */
	$iptmp=gethostbynamel($iphost);
	if ($iptmp==FALSE){
		echo '<span style="color: #360;">Nombre de host v&aacute;lido.</span>';
	}else{
		echo '<span style="color: #360;">Nombre de host v&aacute;lido. IP: '.$iptmp[0].'</span>';
	}
}else{
	echo '<span style="color: #f00;">Direcci&oacute;n IP o nombre de host no validos.</span>';
}
?>
