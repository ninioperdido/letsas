#!/usr/bin/python
# Distributed under the terms of the GNU General Public License v2.
# @author: Gerardo Puerta <gerardo.puerta@juntadeandalucia.es>
# v0.1 10/2013 First version

import sys, getopt, os, subprocess, parted

def main(argv):
    mount_dir = '/mnt/letsas'
    root_part = 2
    boot_part = 1
    boot_mount_point = '/boot'
    debug = False

    try:
        opts, args = getopt.getopt(argv,"hd:")
    except getopt.GetoptError:
        help() 
        sys.exit(2)

    for opt, arg in opts:
        if opt == '-h':
	    help()
	    sys.exit()
        elif opt in ('-d'):
            mount_dir = arg
        elif opt in ('-d'):
            debug = True
    
    # creating root mount point
    try:
        os.makedirs(mount_dir)
    except OSError as e:
        if e.errno = errno.EEXIST and os.path.isdir(mount_dir):
	    pass
	else:
            print "No device found, ", e
            sys.exit(1)
    
    # libparted stuff
    try:
        dev_path = parted.getAllDevices()[0].path
    except Exception as e:
        print "No device found, ", e
        sys.exit(1)
    root_dev_path = dev_path+str(root_part)
    boot_dev_path = dev_path+str(boot_part)

    # Root partition
    try:
        root_path = mountdir
        root_command = ['mount', root_dev_path, root_path]
        root_output = subprocess.check_output(root_command)
    except subprocess.CalledProcessError as e:
        print "Problem found mounting root partition", e
	sys.exit(1)

    # Boot partition
    # creating root mount point
    boot_path = mount_dir + boot_mount_point
    try:
        os.makedirs(boot_path)
    except OSError as e:
        if e.errno = errno.EEXIST and os.path.isdir(boot_path):
	    pass
        else:
	    print "No device found, ", e
	    sys.exit(1)

    try:
        boot_command = ['mount', boot_dev_path, boot_path]
        root_output = subprocess.check_output(boot_command)
    except subprocess.CalledProcessError as e:
        print "Problem found mounting boot partition", e
	sys.exit(1)


    # TODO: seriously you need to investigate about
    # YAML output in python
    print '---'
    print 'device_path:', dev_path
    print 'mounted filesystems:'
    print '  - filesystem:', root_dev_path
    print '    mountpoint:', root_path
    print '  - filesystem:', boot_dev_path
    print '    mountpoint:', boot_path
   
    if debug:
        print 'Boot command:', boot_command
        print boot_output
        print 'Root command:', root_command 
	print root_output
    
    sys.exit()


def help():
    print 'Description:'
    print 'format.py will make two filesystems on the first disk'
    print 'found by parted.'
    print 'First filesystem will have the format of the parameter -bf'
    print '(ext2) if empty, with the options declared on -bo'
    print '(-L boot) if empty.'
    print 'Second filesystem will have the format of the parameter -rf'
    print '(f2fs) if empty, with the options declared on -ro'
    print '(-l root) if empty.'
    print 'Usage:'
    print 'format.py -bf <format> -bo <fs options> -rf <format> -ro <fs options>'


if __name__ == "__main__":
    main(sys.argv[1:])

