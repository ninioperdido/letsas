#!/usr/bin/python
# Distributed under the terms of the GNU General Public License v2.
# @author: Gerardo Puerta <gerardo.puerta@juntadeandalucia.es>
# v0.1 10/2013 First version

import sys, getopt, parted, subprocess

def main(argv):
    boot_format = 'ext2'
    root_format = 'f2fs'
    boot_partition = 1
    root_partition = 2
    boot_options = '-L boot' 
    root_options = '-l root'
    debug = False

    try:
        opts, args = getopt.getopt(argv,"hbf:rf:bo:ro:")
    except getopt.GetoptError:
        help() 
	sys.exit(2)

    for opt, arg in opts:
        if opt == '-h':
	    help()
	    sys.exit()
        elif opt in ('-bf'):
            boot_format = arg
        elif opt in ('-rf'):
            root_format = arg
        elif opt in ('-bo'):
            boot_options = arg
        elif opt in ('-ro'):
            root_options = arg

    # libparted stuff
    try:
        dev_path = parted.getAllDevices()[0].path
    except Exception as e:
        print "No device found, ", e
        sys.exit(1)
    # Boot partition
    try:
        boot_path = dev_path+str(boot_partition)
        boot_command = ['mkfs.'+boot_format, boot_path]
	if boot_options:
	    boot_command.append(boot_options)
        boot_output = subprocess.check_output(boot_command)
    except subprocess.CalledProcessError as e:
        print "Problem found formatting boot partition", e
	sys.exit(1)
    # Root partition
    try:
        root_path = dev_path+str(root_partition)
        root_command = ['mkfs.'+root_format, root_path]
	if root_options:
	    root_command.append(root_options)
        root_output = subprocess.check_output(root_command)
    except subprocess.CalledProcessError as e:
        print "Problem found formatting root partition", e
	sys.exit(1)

    # TODO: seriously you need to investigate about
    # YAML output in python
    print '---'
    print 'device_path:', dev_path
    print 'filesystems:'
    print '  - filesystem:', boot_path
    print '    type:      ', boot_format
    print '    options:   ', boot_options
    print '  - filesystem:', root_path
    print '    type:      ', root_format
    print '    options:   ', root_options
   
    if debug:
        print 'Boot command:', boot_command
        print boot_output
        print 'Root command:', root_command 
	print root_output
    
    sys.exit()


def help():
    print 'Description:'
    print 'format.py will make two filesystems on the first disk'
    print 'found by parted.'
    print 'First filesystem will have the format of the parameter -bf'
    print '(ext2) if empty, with the options declared on -bo'
    print '(-L boot) if empty.'
    print 'Second filesystem will have the format of the parameter -rf'
    print '(f2fs) if empty, with the options declared on -ro'
    print '(-l root) if empty.'
    print 'Usage:'
    print 'format.py -bf <format> -bo <fs options> -rf <format> -ro <fs options>'


if __name__ == "__main__":
    main(sys.argv[1:])

