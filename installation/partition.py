#!/usr/bin/python
# Distributed under the terms of the GNU General Public License v2.
# @author: Gerardo Puerta <gerardo.puerta@juntadeandalucia.es>
# v0.1 10/2013 First version

import sys, getopt, parted

def main(argv):
    boot_size = ''

    try:
        opts, args = getopt.getopt(argv,"hb:r:")
    except getopt.GetoptError:
        help() 
	sys.exit(2)

    for opt, arg in opts:
        if opt == '-h':
	    help()
	    sys.exit()
        elif opt in ('-b'):
	    try:
                boot_size = int(arg)*1024*1024/512
            except Exception as e:
	        print 'Boot size must be a number', e
		help()
		sys.exit(2)

    if not boot_size:
        help()
        sys.exit(2)
    # libparted stuff
    try:
        dev = parted.getAllDevices()[0]
    except Exception as e:
        print "No device found, ", e
        sys.exit(1)
 
    # disk init
    disk = parted.freshDisk(dev, 'gpt')
    constraint = parted.Constraint(device=dev)
    # boot partiition	
    boot_geom = parted.Geometry(device=dev, start=1, end=boot_size)
    boot_part = parted.Partition(disk=disk, type=parted.PARTITION_NORMAL, geometry=boot_geom)
    rv = disk.addPartition(partition=boot_part, constraint=constraint)
    if not rv:
        print "Problem adding boot partition to disk, ", disk
	sys.exit(1)

    # root partition
    root_geom = parted.Geometry(device=dev, start=boot_geom.end+1, end=(constraint.maxSize - 1))
    root_part = parted.Partition(disk=disk, type=parted.PARTITION_NORMAL, geometry=root_geom)
    rv = disk.addPartition(partition=root_part, constraint=constraint)
    if not rv:
        print "Problem adding root partition to disk, ", disk
	sys.exit(1)

    # commit changes
    disk.commit()

    # TODO: seriously you need to investigate about
    # YAML output in python
    print '---'
    print 'device:         ', dev.model
    print 'path:           ', dev.path
    print 'type:           ', disk.type
    print '# of partitions:', disk.primaryPartitionCount
    print ''
    print 'partitions:'
    print '  - part_no: 1'
    print '    start:  ', disk.partitions[0].geometry.start 
    print '    end:    ', disk.partitions[0].geometry.end
    print ''
    print '  - part_no: 2' 
    print '    start:  ', disk.partitions[1].geometry.start
    print '    end:    ', disk.partitions[1].geometry.end

    sys.exit()


def help():
    print 'Description:'
    print 'partition.py will make a new gpt label on the first disk'
    print 'found by parted and will create 2 partitions on it'
    print 'first partition will have the size of the parameter and'
    print 'second one will expand to the rest of the drive'
    print 'Usage:'
    print 'partition.py -b <size>'
    print 'where -b is the size of the boot (#1) partition in MB'


if __name__ == "__main__":
    main(sys.argv[1:])

