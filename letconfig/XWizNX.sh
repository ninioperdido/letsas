#!/bin/sh
DIALOG=Xdialog

aplicacion="Escritorio" #Variable que almacena lo que queremos publicar
esc="on"
app="off"
resolucion="800x600" 	#Variable que almacena la resolución elegida 800x600 por defecto
R1024="off" 		#Variable que almacena la opción última seleccionada
R800="on"		#Variable que almacena la opción última seleccionada
Ravailable="off"	#Variable que almacena la opción última seleccionada
Rfullscreen="off"	#Variable que almacena la opción última seleccionada
teclado="es"		#Variable que almacena la configuración de nuestro teclado
es="on"			#Variable que almacena la opción última seleccionada
en="off"		#Variable que almacena la opción última seleccionada
usuario="Usuario"	#Variable que almacena el dato de usuario
contrasena=""		#Variable que almacena el dato de contraseña
dirip="0.0.0.0"		#Variable que almacena la dirección ip o el nombre de host
impresora="Ninguna"	#Variable que almacena el tipo de impresión
cola="usblp"		#Variable que almacena el nombre de la cola a direccionar
par="off"		#Variable que almacena la opción última seleccionada
col="off"		#Variable que almacena la opción última seleccionada
no="on"			#Variable que almacena la opción última seleccionada
sonido="Off"		#Variable que almacena si queremos sonido o no
on="off"		#Variable que almacena la opción última seleccionada
off="on"		#Variable que almacena la opción última seleccionada
ruta="/mnt/desktop/" 	#Variable que almacena la ruta de los archivos .desktop
rutautil="/mnt/letconfig/"	#Variable que almacena la ruta de las utilidades de letconfig
icon="/usr/share/icons/"  	#Variable que almacena la ruta de los archivos de iconos
sesion="NX1"			#Variable que almacena la sesión
archivo="NX1.desktop"		#Variable que almacena el archivo donde se guarda la sesión
salir="falso"		#Variable que indica cuando salir del configurador
guardado="verdadero"	#Variable que indica cuando hemos hecho un cambio para salvarlo antes de salir
flag="true"		#Variable que indica cuando podemos dejar la ventana de guardar


while [ $salir = "falso" ]; do #Mientras no seleccionemos salir
 flag="true" #Nos va a permitir controlar si la sesión existe para evitar sobreescribirla por error
 $DIALOG --title "Configuración sesiones NX" --icon "$icon"nx-desktop.png --menu "Configuración de la conexión" 25 60 10 "Aplicación" "$aplicacion" "Resolución"  "$resolucion" "Teclado" "$teclado"  "Identificación" "$usuario $dirip" "Impresora" "$impresora" "Sonido" "$sonido" "Guardar" "Guarda la sesión configurada" "Eliminar" "Elimina una sesión existente" "Conectar" "Lanza la conexión configurada" "Salir" "Abandona el cliente" 2> /tmp/menu.tmp.$$	#Lanzamos el menú de configuración

 retval=$?

 choice=`cat /tmp/menu.tmp.$$` #Comprobamos qué variable queremos configurar
 rm -f /tmp/menu.tmp.$$

 case $retval in
   0)                          #La opción elegida lanza la pantalla de configuración del parámetro
     case $choice in

      Aplicación)	#Si elegimos configurar la aplicación a publicar
	aux=$aplicacion
	$DIALOG --title "Aplicación a publicar" --icon "$icon"Tango/32x32/apps/preferences-system-sessions.png --radiolist "Elija si desea publicar una aplicación en concreto o el escritorio al completo" 12 45 2 "Escritorio" "Publica el entorno" $esc "Aplicación" "Publica aplicación" $app 2>/tmp/aplicacion.tmp.$$ #Un Radiolist para elegir la aplicación

	retval=$?

	case $retval in
	 0)
	  aplicacion=`cat /tmp/aplicacion.tmp.$$`	#Guardamos la configuración de la aplicación a publicar
	  rm -f /tmp/aplication.tmp.$$
	  if [ "$aplicacion" != "$aux" ]; then 	#Comprobamos si ha habido alguna midoficación para avisar antes de salir
	   guardado="falso"
	  fi
	  if [ "$aplicacion" = "Escritorio" ]; then
	   esc="on"
	   app="off"
	  else
	   esc="off"
           app="on"

	   $DIALOG --title "Aplicación a publicar" --icon "$icon"Tango/32x32/apps/preferences-system-session.png --left --ok-label "Ok" --cancel-label "Cancelar" --inputbox "Introduzca la ruta completa de la aplicación a publicar" 0 0 "$aplicacion" 2> /tmp/aplicacion.tmp.$$
	
           retval=$?
	   aplicacion=`cat /tmp/aplicacion.tmp.$$`
	   rm -f /tmp/aplicacion.tmp.$$
	  fi
	  ;;
	 1)
          rm -f /tmp/aplicacion.tmp.$$ #No guardamos nada, pero borramos el archivo temporal de almacenamiento
	  echo "Cancelado configuración de aplicación publicada"
	  ;;

         255)
          rm -f /tmp/aplicacion.tmp.$$ #No guardamos nada, pero borramos el archivo temporal de almacenamiento
	  echo "Cerrada ventana de configuración de aplicacion publicada"
	  ;;
        esac;;



      Resolución)	#Si elegimos configurar la resolución	
        aux=$resolucion
	$DIALOG --title "Resolución de pantalla" --icon "$icon"Tango/32x32/apps/config-xfree.png --radiolist "Elija la resolución" 25 45 4 "1024x768" "Alta resolución" $R1024 "800x600" "Resolución media" $R800 "available" "Resolución disponible" $Ravailable "fullscreen" "Pantalla completa" $Rfullscreen 2>/tmp/resolucion.tmp.$$ #Un Radiolist para elegir la resolución

        retval=$?
 
        case $retval in
         0)
 	  resolucion=`cat /tmp/resolucion.tmp.$$` #Guardamos la configuración de resolución
	  if [ "$resolucion" != "$aux" ]; then #Comprobamos si ha habido alguna modificación para avisar antes de salir
	   guardado="falso"
	  fi
	  if [ "$resolucion" = "1024x768" ]; then #Guardamos el estado configurado para que aparezca el radio-button adecuado
	   R1024="on"
	   R800="off"
	   Ravailable="off"
	   Rfullscreen="off"
	  else
	   if [ "$resolucion" = "available" ]; then
		R1024="off"
		R800="off"
		Ravailable="on"
		Rfullscreen="off"
	   else
		if [ "$resolucion" = "fullscreen" ]; then
			R1024="off"
			R800="off"
			Ravailable="off"
			Rfullscreen="on"
		else
			R1024="off"
			R800="on"
			Ravailable="off"
			Rfullscreen="off"
		fi
	   fi

	  fi
          rm -f /tmp/resolucion.tmp.$$
          ;;
         
         1)
          rm -f /tmp/resolucion.tmp.$$ #No guardamos nada, pero borramos el archivo temporal de almacenamiento
	  echo "Cancelado configuración de resolución"
	  ;;

         255)
          rm -f /tmp/resolucion.tmp.$$ #No guardamos nada, pero borramos el archivo temporal de almacenamiento
	  echo "Cerrada ventana de configuración de resolución"
	  ;;
        esac;;

      Teclado) 	 #Si elegimos configurar el teclado
	aux=$teclado
        $DIALOG --title "Teclado" --icon "$icon"Tango/32x32/devices/keyboard.png --radiolist "Elija la configuración del teclado" 12 45 2 "es" "Español" $es "us" "Inglés americano" $en 2>/tmp/teclado.tmp.$$ #Un Radiolist para elegir el teclado

        retval=$?
 
        case $retval in
         0)
 	  teclado=`cat /tmp/teclado.tmp.$$` #Guardamos la configuración de teclado
	  if [ "$teclado" != "$aux" ]; then
	   guardado="falso"
	  fi
	  if [ "teclado" = "es" ]; then #Guardamos el estado configurado para que aparezca el radio-button adecuado
	   es="on"
	   en="off"
	  else
	   es="off"
	   en="on"
	  fi
          rm -f /tmp/teclado.tmp.$$
          ;;
         
         1)
          rm -f /tmp/teclado.tmp.$$ #No guardamos nada, pero borramos el archivo temporal de almacenamiento
	  echo "Candelado configuración de teclado"
	  ;;

         255)
          rm -f /tmp/teclado.tmp.$$ #No guardamos nada, pero borramos el archivo temporal de almacenamiento
          echo "Cerrada ventana de configuración de teclado"
	  ;;
        esac;;

      Identificación) #Pedimos los datos de host remoto, usuario y contraseña
	aux="$dirip|$usuario"
       $DIALOG	--separator "|" --title "Conexión" --icon "$icon"Tango/32x32/apps/config-users.png --backtitle "Datos del equipo remoto y de usuario" --left --2inputsbox "Introduzca sus datos" 25 50 "Dirección IP o nombre del host:" $dirip "Usuario:" "$usuario" 2>/tmp/login.tmp.$$ #Metemos los datos separados por el carácter |

        ret=$?

        case $ret in
	 0)
		usuario=`awk -F "|" '{ print $2 }' /tmp/login.tmp.$$`     #Extraemos el usuario
#		contrasena=`awk -F "|" '{ print $3 }' /tmp/login.tmp.$$` #Extraemos la contraseña
		dirip=`awk -F "|" '{ print $1 }' /tmp/login.tmp.$$`    #Extraemos la dirección o nombre del host
		aux2=`echo "$dirip|$usuario"`

		if [ "$aux" != "$aux2" ]; then
		 guardado="falso"
		fi
		rm -f /tmp/login.tmp.$$ 
		;;
	 1)
		rm -f /tmp/login.tmp.$$ #Borramos el archivo temporal
		echo "Cancelado configuración de datos de conexión"
		;;
	 255)
		rm -f /tmp/login.tmp.$$ #Borramos el archivo temporal
		echo "Cerrada la ventana de configuración de datos de conexión"
		;;
        esac;;

      Impresora)
	$DIALOG --title "Configuración de Impresoras" --icon "$icon"Tango/32x32/devices/printer.png --ok-label "Aceptar" --cancel-label "Cancelar" --yesno "Para configurar la impresora local debe configurar la cola de impresión\nde la misma en la sesión del usuario correspondiente\nen el equipo servidor." 0 0
	;;

      Sonido) 	 #Si elegimos configurar el sonido
	aux=$sonido
        $DIALOG --title "Sonido" --icon "$icon"Tango/32x32/devices/yast_soundcard.png --radiolist "Elija la si desea activar el sonido" 12 45 2 "On" "Sonido activado" $on "Off" "Sonido desactivado" $off 2>/tmp/sonido.tmp.$$ #Un Radiolist para elegir si queremos sonido

        retval=$?
 
        case $retval in
         0)
 	  sonido=`cat /tmp/sonido.tmp.$$` #Guardamos la configuración de sonido
	  if [ "$sonido" != "$aux" ]; then
	   guardado="falso"
	  fi
	  if [ "$sonido" = "On" ]; then #Guardamos el estado configurado para que aparezca el radio-button adecuado
	   on="on"
	   off="off"
	  else
	   on="off"
	   off="on"
	  fi
          rm -f /tmp/sonido.tmp.$$
          ;;
         
         1)
          rm -f /tmp/sonido.tmp.$$ #No guardamos nada, pero borramos el archivo temporal de almacenamiento
	  echo "Cancelado configuración de sonido"
	  ;;

         255)
          rm -f /tmp/sonido.tmp.$$ #No guardamos nada, pero borramos el archivo temporal de almacenamiento
	  echo "Cerrada ventana de configuración de sonido"
	  ;;
        esac;;

      Guardar)

       while [ "$flag" = "true" ]; do #Mientras no guardemos o cancelemos, seguimos en el bucle
	$DIALOG --title "Guardar Sesión" --icon "$icon"Tango/32x32/actions/document-save.png --left --ok-label "Guardar" --cancel-label "Cancelar" --inputbox "Introduzca el nombre de la sesión a guardar" 0 0 "$sesion" 2> /tmp/save.tmp.$$
	
	retval=$?
	sesion=`cat /tmp/save.tmp.$$`
	rm -f /tmp/save.tmp.$$
	archivo="$sesion.desktop"
	aux=`ls "$ruta$archivo"`
	aux2=`cat /root/.config/rox.sourceforge.net/ROX-Filer/pb_Desktop | grep "$sesion"`
	
	if [ "$ruta$archivo" != "$aux" -o "$aux2" = "" ]; then #comprobamos si el archivo ya existe

         case $retval in #Si no existe, lo creamos
 	  0)
			#A partir de las variables configuradas, montamos la plantilla
		TEMPLATE=/mnt/sessions/template.nxs
		cat $TEMPLATE | sed "s/__RESOLUTION__/$resolucion/g" > "/mnt/sessions/$sesion.nxs"
		echo "1"
		cp "/mnt/sessions/$sesion.nxs" /tmp/template.tmp.nxs

		TEMPLATE=/tmp/template.tmp.nxs
		cat $TEMPLATE | sed "s/__KEYBOARD__/$teclado/g" > "/mnt/sessions/$sesion.nxs"
		echo "2"
		cp "/mnt/sessions/$sesion.nxs" /tmp/template.tmp.nxs

		TEMPLATE=/tmp/template.tmp.nxs
		if [ "$aplicacion" = "Escritorio" ]; then
			cat $TEMPLATE | sed -e "s/__DESKTOP__/console/g" -e "s/__DESKTOP2__/gnome/g" -e "s/__COMMAND__//g" > "/mnt/sessions/$sesion.nxs"
			cp "/mnt/sessions/$sesion.nxs" /tmp/template.tmp.nxs
		else
			cat $TEMPLATE | sed -e "s#__DESKTOP__#application#g" -e "s#__DESKTOP2__#console#g" -e "s#__COMMAND__#$aplicacion#g" > "/mnt/sessions/$sesion.nxs"
			cp "/mnt/sessions/$sesion.nxs" /tmp/template.tmp.nxs
		fi
		echo "3"

		TEMPLATE=/tmp/template.tmp.nxs
		cat $TEMPLATE | sed  "s/__HOST__/$dirip/g" > "/mnt/sessions/$sesion.nxs"
		echo "4"
		cp "/mnt/sessions/$sesion.nxs" /tmp/template.tmp.nxs

		if [ "$usuario" = "" ]; then
			usuario=`whoami`
		fi
		TEMPLATE=/tmp/template.tmp.nxs
		cat $TEMPLATE | sed  "s/__USER__/$usuario/g" > "/mnt/sessions/$sesion.nxs"
		echo "5"
		cp "/mnt/sessions/$sesion.nxs" /tmp/template.tmp.nxs

		TEMPLATE=/tmp/template.tmp.nxs
       		if [ "$sonido" = "On" ]; then
			cat $TEMPLATE | sed "s/__AUDIO__/true/g" > "/mnt/sessions/$sesion.nxs"
		else
			cat $TEMPLATE | sed "s/__AUDIO__/false/g" > "/mnt/sessions/$sesion.nxs"
		fi
		echo "6"
		rm -f /tmp/template.tmp.nxs



		"$rutautil"roxSOAP.sh remove "$ruta$archivo" #Eliminamos el icono para no duplicarlo
		echo "[Desktop Entry]" > "$ruta$archivo"
		echo "Exec=/usr/bin/nxclient --session \"/mnt/sessions/"$sesion".nxs\"" >> "$ruta$archivo"	
		echo "Icon="$icon"nx-desktop.png" >> "$ruta$archivo"	#creamos el archivo .desktop
		echo "Name=$sesion" >> "$ruta$archivo"
		x=`/mnt/letconfig/LetPosIcon.sh | awk '{print $1}'`
		y=`/mnt/letconfig/LetPosIcon.sh | awk '{print $2}'`
		"$rutautil"roxSOAP.sh add "$ruta$archivo" $x $y "$sesion"	#Añadimos el icono en el escritorio

		$DIALOG --title "Información" --icon "$icon"Tango/32x32/status/dialog-information.png --no-cancel --msgbox "La sesión ha sido creada con éxito" 0 0

		guardado="verdadero" 	#Indicamos que se han guardado los cambios
		flag="false";;	 	#Salimos del bucle de guardado
	  1)
		echo "Cancelado guardar sesión"	#Si le damos a cancelar en la ventana de guarda, salimos sin guardar
		flag="false";;
	  255)
		echo "Cerrada ventana de guardar sesión" 
		flag="false";;
         esac

	else	#Si el archivo existe, pedimos confirmación para guardarlo
	 $DIALOG --title "Aviso" --icon "$icon"Tango/32x32/status/dialog-warning.png --ok-label "Sí" --cancel-label "No" --yesno "El archivo ya existe ¿desea sobreescribirlo?" 0 0
	 
	 retval=$?

	 case $retval in 	#Si aceptamos sobreescribir, guardamos
	  0)

			#A partir de las variables configuradas, montamos la plantilla
		TEMPLATE=/mnt/sessions/template.nxs
		cat $TEMPLATE | sed "s/__RESOLUTION__/$resolucion/g" > "/mnt/sessions/$sesion.nxs"
		cp "/mnt/sessions/$sesion.nxs" /tmp/template.tmp.nxs

		TEMPLATE=/tmp/template.tmp.nxs
		cat $TEMPLATE | sed "s/__KEYBOARD__/$teclado/g" > "/mnt/sessions/$sesion.nxs"
		cp "/mnt/sessions/$sesion.nxs" /tmp/template.tmp.nxs

		TEMPLATE=/tmp/template.tmp.nxs
		if [ "$aplicacion" = "Escritorio" ]; then
			cat $TEMPLATE | sed -e "s/__DESKTOP__/console/g" -e "s/__DESKTOP2__/gnome/g" -e "s/__COMMAND__//g" > "/mnt/sessions/$sesion.nxs"
		else
			cat $TEMPLATE | sed -e "s#__DESKTOP__#application#g" -e "s#__DESKTOP2__#console#g" -e "s#__COMMAND__#$aplicacion#g" > "/mnt/sessions/$sesion.nxs"
			cp "/mnt/sessions/$sesion.nxs" /tmp/template.tmp.nxs
		fi

		TEMPLATE=/tmp/template.tmp.nxs
		cat $TEMPLATE | sed  "s/__HOST__/$dirip/g" > "/mnt/sessions/$sesion.nxs"
		cp "/mnt/sessions/$sesion.nxs" /tmp/template.tmp.nxs

		if [ "$usuario" = "" ]; then
			usuario=`whoami`
		fi
		TEMPLATE=/tmp/template.tmp.nxs
		cat $TEMPLATE | sed  "s/__USER__/$usuario/g" > "/mnt/sessions/$sesion.nxs"
		cp "/mnt/sessions/$sesion.nxs" /tmp/template.tmp.nxs

		TEMPLATE=/tmp/template.tmp.nxs
       		if [ "$sonido" = "On" ]; then
			cat $TEMPLATE | sed "s/__AUDIO__/true/g" > "/mnt/sessions/$sesion.nxs"
		else
			cat $TEMPLATE | sed "s/__AUDIO__/false/g" > "/mnt/sessions/$sesion.nxs"
		fi
		rm -f /tmp/template.tmp.nxs



		"$rutautil"roxSOAP.sh remove "$ruta$archivo" #Eliminamos el icono para no duplicarlo
		echo "[Desktop Entry]" > "$ruta$archivo"
		echo "Exec=/usr/bin/nxclient --session \"/mnt/sessions/"$sesion".nxs\"" >> "$ruta$archivo"	
		echo "Icon="$icon"nx-desktop.png" >> "$ruta$archivo"	#creamos el archivo .desktop
		echo "Name=$sesion" >> "$ruta$archivo"
		x=`/mnt/letconfig/LetPosIcon.sh | awk '{print $1}'`
		y=`/mnt/letconfig/LetPosIcon.sh | awk '{print $2}'`
	
		"$rutautil"roxSOAP.sh add "$ruta$archivo" $x $y "$sesion"	#Añadimos el icono en el escritorio

		$DIALOG --title "Información" --icon "$icon"Tango/32x32/status/dialog-information.png --no-cancel --msgbox "La sesión ha sido creada con éxito" 0 0

		guardado="verdadero" 	#Indicamos que se han guardado los cambios
		flag="false";;	 	#Salimos del bucle de guardado
	  1)
		echo "volvemos a la ventana de guardar";;	#No queremos sobreescribir, volvemos a la ventana de guardar
	  255)
		echo "Cerrada ventana de confirmación, volvemos a la ventana de guardar";;
	 esac		#Si cerramos la confirmación, volvemos a la ventana de guardar
	fi
       done 
      ;;

      Eliminar)
	while [ "$flag" = "true" ]; do #Mientras no borremos o cancelemos, seguimos en el bucle

		$DIALOG --title "Configuración sesiones NX" --icon "$icon"Tango/32x32/actions/eraser.png --backtitle "¡¡¡Atención!!!, va a proceder a la eliminación de una sesión NX" --left  --ok-label "Eliminar" --cancel-label "Cancelar" --inputbox "Introduzca el nombre de la sesión" 0 0 "$sesion" 2> /tmp/login.tmp.$$

		retval=$?

	
		case $retval in
	  	0)
			sesion=`cat /tmp/login.tmp.$$` #Extraemos el nombre de la sesión 
			archivo="$sesion.desktop"
	
			rm -f /tmp/login.tmp.$$

			$DIALOG --title "Aviso" --icon "$icon"Tango/32x32/status/dialog-warning.png --ok-label "Sí" --cancel-label "No" --yesno "Ha seleccionado eliminar la sesión '$sesion', ¿desea continuar?" 0 0

			retval=$?

	 		case $retval in 	#Si aceptamos, eliminamor
	  		0)
				aux2=`cat "$ruta$archivo" | grep "nx-desktop"`
				aux3=`cat .config/rox.sourceforge.net/ROX-Filer/pb_Desktop | grep "$sesion"`
				if [ -e "$ruta$archivo" ]; then #Si existe el archivo
					if [ "$aux2" != "" ]; then #Si es del tipo adecuado			 
						"$rutautil"roxSOAP.sh remove "$ruta$archivo" #Eliminamos el icono 
						rm -f "$ruta$archivo" 2> /dev/null
						rm -f "/mnt/sessions/$sesion.nxs" 2> /dev/null
						$DIALOG --title "Información" --icon "$icon"Tango/32x32/status/dialog-information.png --no-cancel --msgbox "La sesión ha sido eliminada con éxito" 0 0
					else	#Si no es del tipo adecuado damos aviso
						$DIALOG --title "Aviso" --icon "$icon"Tango/32x32/status/dialog-warning.png --no-cancel --msgbox "Está intentando eliminar un acceso que no es una sesión de NX, permiso denegado" 0 0
					fi
				else
					if [ "$aux3" != ""]; then

						"$rutautil"roxSOAP.sh remove "$ruta$archivo" #Eliminamos el icono 
						rm -f "$ruta$archivo" 2> /dev/null
						rm -f "/mnt/sessions/$sesion.nxs" 2> /dev/null
						$DIALOG --title "Información" --icon "$icon"Tango/32x32/status/dialog-information.png --no-cancel --msgbox "La sesión ha sido eliminada con éxito" 0 0
							
					else	#Si no existe tampoco el icono, avisamos
						$DIALOG --title "Aviso" --icon "$icon"Tango/32x32/status/dialog-warning.png --no-cancel --msgbox "Está intentando eliminar un acceso que no existe." 0 0
					fi
				fi

				flag="false";; #Salimos del bucle
						
		  	1)
				echo "Volvemos a la ventana de eliminar";;#No queremos eliminar, volvemos atrás						
			255)
				echo "Cerrada ventana de confirmación, volvemos a la ventana de guardar";;
	 		esac	#Si cerramos la confirmación, volvemos a la ventana de guardar
			;;
	 
		1)
	    		rm -f /tmp/login.tmp.$$
	    		echo "Cancelado borrado de sesión NX"
			flag=false;;

				
		255)
			rm -f /tmp/login.tmp.$$
			echo "Se ha cerrado la ventana de configuración"
			flag="false";;
	
		esac
	done;;


      Conectar)		
			#A partir de las variables configuradas, montamos la plantilla
		TEMPLATE=/mnt/sessions/template.nxs
		cat $TEMPLATE | sed "s/__RESOLUTION__/$resolucion/g" > "/tmp/template.tmp1.nxs"
		cp "/tmp/template.tmp1.nxs" /tmp/template.tmp1.nxs

		TEMPLATE=/tmp/template.tmp.nxs
		cat $TEMPLATE | sed "s/__KEYBOARD__/$teclado/g" > "/tmp/template.tmp1.nxs"
		cp "/tmp/template.tmp1.nxs" /tmp/template.tmp.nxs

		TEMPLATE=/tmp/template.tmp.nxs
		if [ "$aplicacion" = "Escritorio" ]; then
			cat $TEMPLATE | sed -e "s/__DESKTOP__/console/g" -e "s/__DESKTOP2__/gnome/g" -e "s/__COMMAND__//g" > "/tmp/template.tmp1.nxs"
		else
			cat $TEMPLATE | sed -e "s#__DESKTOP__#application#g" -e "s#__DESKTOP2__#console#g" -e "s#__COMMAND__#$aplicacion#g" > "/tmp/template.tmp1.nxs"
			cp "/tmp/template.tmp1.nxs" /tmp/template.tmp.nxs
		fi

		TEMPLATE=/tmp/template.tmp.nxs
		cat $TEMPLATE | sed  "s/__HOST__/$dirip/g" > "/tmp/template.tmp1.nxs"
		cp "/tmp/template.tmp1.nxs" /tmp/template.tmp.nxs

		if [ "$usuario" = "" ]; then
			usuario=`whoami`
		fi
		TEMPLATE=/tmp/template.tmp.nxs
		cat $TEMPLATE | sed  "s/__USER__/$usuario/g" > "/tmp/template.tmp1.nxs"
		cp "/tmp/template.tmp1.nxs" /tmp/template.tmp.nxs

		TEMPLATE=/tmp/template.tmp.nxs
       		if [ "$sonido" = "On" ]; then
			cat $TEMPLATE | sed "s/__AUDIO__/true/g" > "/tmp/template.tmp1.nxs"
		else
			cat $TEMPLATE | sed "s/__AUDIO__/false/g" > "/tmp/template.tmp1.nxs"
		fi
		rm -f /tmp/template.tmp.nxs

			#llamamos al comando para ver si la conexión se puede realizar
		/usr/bin/nxclient --session /tmp/template.tmp1.nxs 2>/tmp/status_nx.tmp.$$
		 rm -f /tmp/template.tmp1.nxs
		 status=`cat /tmp/status_nx.tmp.$$` 
		 rm -f /tmp/status_nx.tmp.$$
		 if [ "$status" != ""  ]; then 	#Si se ha producido algún error
			$DIALOG --title "Aviso" --left --icon "$icon"Tango/32x32/status/dialog-warning.png --msgbox "$status" 0 0 0		#Vemos los mensajes de error por pantalla
		 fi
	         ;;

      Salir)	#Si seleccionamos salir, comprobamos si se ha realizado algún cambio
	if [ "$guardado" = "verdadero" ]; then
	 salir="verdadero"
	else	#Si ha habido cambios, se da el aviso de que se perderán 
	 $DIALOG --title "Aviso" --center --icon "$icon"Tango/32x32/status/dialog-warning.png --ok-label "Sí" --cancel-label "No" --yesno "Ha realizado modificaciones y no ha salvado la sesión.\n¿Desea guardar los cambios?" 0 0 0
	 case $? in
	 0)

          while [ "$flag" = "true" ]; do #Mientras no guardemos o cancelemos, seguimos en el bucle
		$DIALOG --title "Guardar Sesión" --icon "$icon"Tango/32x32/actions/document-save.png --left --ok-label "Guardar" --cancel-label "Cancelar" --inputbox "Introduzca el nombre de la sesión a guardar" 0 0 "$sesion" 2> /tmp/save.tmp.$$
	
		retval=$?
		sesion=`cat /tmp/save.tmp.$$`
		rm -f /tmp/save.tmp.$$
		archivo="$sesion.desktop"
		aux=`ls "$ruta$archivo"`
		aux2=`cat /root/.config/rox.sourceforge.net/ROX-Filer/pb_Desktop | grep "$sesion"`
	
		if [ "$ruta$archivo" != "$aux" -o "$aux2" = "" ]; then #comprobamos si el archivo ya existe

	         case $retval in #Si no existe, lo creamos
	 	  0)
			#A partir de las variables configuradas, montamos la plantilla
			TEMPLATE=/mnt/sessions/template.nxs
			cat $TEMPLATE | sed "s/__RESOLUTION__/$resolucion/g" > "/mnt/sessions/$sesion.nxs"
			cp "/mnt/sessions/$sesion.nxs" /tmp/template.tmp.nxs
	
			TEMPLATE=/tmp/template.tmp.nxs
			cat $TEMPLATE | sed "s/__KEYBOARD__/$teclado/g" > "/mnt/sessions/$sesion.nxs"
			cp "/mnt/sessions/$sesion.nxs" /tmp/template.tmp.nxs
	
			TEMPLATE=/tmp/template.tmp.nxs
			if [ "$aplicacion" = "Escritorio" ]; then
				cat $TEMPLATE | sed -e "s/__DESKTOP__/console/g" -e "s/__DESKTOP2__/gnome/g" -e "s/__COMMAND__//g" > "/mnt/sessions/$sesion.nxs"
			else
				cat $TEMPLATE | sed -e "s#__DESKTOP__#application#g" -e "s#__DESKTOP2__#console#g" -e "s#__COMMAND__#$aplicacion#g" > "/mnt/sessions/$sesion.nxs"
				cp "/mnt/sessions/$sesion.nxs" /tmp/template.tmp.nxs
			fi
	
			TEMPLATE=/tmp/template.tmp.nxs
			cat $TEMPLATE | sed  "s/__HOST__/$dirip/g" > "/mnt/sessions/$sesion.nxs"
			cp "/mnt/sessions/$sesion.nxs" /tmp/template.tmp.nxs
	
			if [ "$usuario" = "" ]; then
				usuario=`whoami`
			fi
			TEMPLATE=/tmp/template.tmp.nxs
			cat $TEMPLATE | sed  "s/__USER__/$usuario/g" > "/mnt/sessions/$sesion.nxs"
			cp "/mnt/sessions/$sesion.nxs" /tmp/template.tmp.nxs
	
			TEMPLATE=/tmp/template.tmp.nxs
	       		if [ "$sonido" = "On" ]; then
				cat $TEMPLATE | sed "s/__AUDIO__/true/g" > "/mnt/sessions/$sesion.nxs"
			else
				cat $TEMPLATE | sed "s/__AUDIO__/false/g" > "/mnt/sessions/$sesion.nxs"
			fi
			rm -f /tmp/template.tmp.nxs
	
	
	
			"$rutautil"roxSOAP.sh remove "$ruta$archivo" #Eliminamos el icono para no duplicarlo
			echo "[Desktop Entry]" > "$ruta$archivo"
			echo "Exec=/usr/bin/nxclient --session \"/mnt/sessions/"$sesion".nxs\"" >> "$ruta$archivo"	
			echo "Icon="$icon"nx-desktop.png" >> "$ruta$archivo"	#creamos el archivo .desktop
			echo "Name=$sesion" >> "$ruta$archivo"
			x=`/mnt/letconfig/LetPosIcon.sh | awk '{print $1}'`
			y=`/mnt/letconfig/LetPosIcon.sh | awk '{print $2}'`
	
			"$rutautil"roxSOAP.sh add "$ruta$archivo" $x $y "$sesion"	#Añadimos el icono en el escritorio
	
			$DIALOG --title "Información" --icon "$icon"Tango/32x32/status/dialog-information.png --no-cancel --msgbox "La sesión ha sido creada con éxito" 0 0
	
			guardado="verdadero" 	#Indicamos que se han guardado los cambios
			flag="false"	 	#Salimos del bucle de guardado
			salir="verdadero";;
		  1)
			echo "Cancelado guardar sesión"	#Si le damos a cancelar en la ventana de guarda, salimos sin guardar
			flag="false";;
		  255)
			echo "Cerrada ventana de guardar sesión" 
			flag="false";;
	         esac
	
		else	#Si el archivo existe, pedimos confirmación para guardarlo
		 $DIALOG --title "Aviso" --icon "$icon"Tango/32x32/status/dialog-warning.png --ok-label "Sí" --cancel-label "No" --yesno "El archivo ya existe ¿desea sobreescribirlo?" 0 0
	 
		 retval=$?
	
		 case $retval in 	#Si aceptamos sobreescribir, guardamos
		  0)
	
				#A partir de las variables configuradas, montamos la plantilla
			TEMPLATE=/mnt/sessions/template.nxs
			cat $TEMPLATE | sed "s/__RESOLUTION__/$resolucion/g" > "/mnt/sessions/$sesion.nxs"
			cp "/mnt/sessions/$sesion.nxs" /tmp/template.tmp.nxs
	
			TEMPLATE=/tmp/template.tmp.nxs
			cat $TEMPLATE | sed "s/__KEYBOARD__/$teclado/g" > "/mnt/sessions/$sesion.nxs"
			cp "/mnt/sessions/$sesion.nxs" /tmp/template.tmp.nxs
	
			TEMPLATE=/tmp/template.tmp.nxs
			if [ "$aplicacion" = "Escritorio" ]; then
				cat $TEMPLATE | sed -e "s/__DESKTOP__/console/g" -e "s/__DESKTOP2__/gnome/g" -e "s/__COMMAND__//g" > "/mnt/sessions/$sesion.nxs"
			else
				cat $TEMPLATE | sed -e "s#__DESKTOP__#application#g" -e "s#__DESKTOP2__#console#g" -e "s#__COMMAND__#$aplicacion#g" > "/mnt/sessions/$sesion.nxs"
				cp "/mnt/sessions/$sesion.nxs" /tmp/template.tmp.nxs
			fi
	
			TEMPLATE=/tmp/template.tmp.nxs
			cat $TEMPLATE | sed  "s/__HOST__/$dirip/g" > "/mnt/sessions/$sesion.nxs"
			cp "/mnt/sessions/$sesion.nxs" /tmp/template.tmp.nxs
	
			if [ "$usuario" = "" ]; then
				usuario=`whoami`
			fi
			TEMPLATE=/tmp/template.tmp.nxs
			cat $TEMPLATE | sed  "s/__USER__/$usuario/g" > "/mnt/sessions/$sesion.nxs"
			cp "/mnt/sessions/$sesion.nxs" /tmp/template.tmp.nxs
	
			TEMPLATE=/tmp/template.tmp.nxs
	       		if [ "$sonido" = "On" ]; then
				cat $TEMPLATE | sed "s/__AUDIO__/true/g" > "/mnt/sessions/$sesion.nxs"
			else
				cat $TEMPLATE | sed "s/__AUDIO__/false/g" > "/mnt/sessions/$sesion.nxs"
			fi
			rm -f /tmp/template.tmp.nxs
	
	
	
			"$rutautil"roxSOAP.sh remove "$ruta$archivo" #Eliminamos el icono para no duplicarlo
			echo "[Desktop Entry]" > "$ruta$archivo"
			echo "Exec=/usr/bin/nxclient --session \"/mnt/sessions/"$sesion".nxs\"" >> "$ruta$archivo"	
			echo "Icon="$icon"nx-desktop.png" >> "$ruta$archivo"	#creamos el archivo .desktop
			echo "Name=$sesion" >> "$ruta$archivo"
			x=`/mnt/letconfig/LetPosIcon.sh | awk '{print $1}'`
			y=`/mnt/letconfig/LetPosIcon.sh | awk '{print $2}'`
	
			"$rutautil"roxSOAP.sh add "$ruta$archivo" $x $y "$sesion"	#Añadimos el icono en el escritorio
	
			$DIALOG --title "Información" --icon "$icon"Tango/32x32/status/dialog-information.png --no-cancel --msgbox "La sesión ha sido creada con éxito" 0 0
	
			guardado="verdadero" 	#Indicamos que se han guardado los cambios
			flag="false"	 	#Salimos del bucle de guardado
			salir="verdadero";;
		  1)
			echo "volvemos a la ventana de guardar";;	#No queremos sobreescribir, volvemos a la ventana de guardar
		  255)
			echo "Cerrada ventana de confirmación, volvemos a la ventana de guardar";;
		  esac		#Si cerramos la confirmación, volvemos a la ventana de guardar
		fi
           done;; 

	 1)
	  echo "Ha seleccionado no guardar los cambios"
	  salir="verdadero";;
	 255)
	  echo "Ha cerrado la ventana de confirmación";;
	 esac
	fi
         ;;
     esac
     ;;     
    1)
     echo "Cancelado configuración de sesión NX"
     salir="verdadero"
     ;;
   255)
     echo "Cerrada ventana de configuración de sesión NX"
     salir="verdadero"
     ;;
 esac
done
