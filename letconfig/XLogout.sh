#!/bin/sh

##########################################
# Muestra el diálogo de cierre de sesión #
##########################################

Xdialog --title "Cerrar sesion" --icon /usr/share/icons/Tango/32x32/actions/system-log-out.png --default-no --ok-label "Sí" --cancel-label "No" --yesno "¿Desea cerrar la sesion?" 6 50 3000
case $? in
0)
	aux=`mount | grep USB`
	if [ "$aux" != "" ]; then
		umount /mnt/home/usuario/USB
	fi
	aux=`mount | grep REMOTE`
	if [ "$aux" != "" ]; then
		umount /mnt/home/usuario/REMOTE
	fi
	killall icewm;;
1)
	echo "Cierre de sesión cancelado";;
255)
	echo "Ventana de cierre de sesión cerrada";;
esac
