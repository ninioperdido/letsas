#!/bin/sh
# Copyright (C) 2007 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published
# by the Free Software Foundation; version 2 only.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#

# GPL - Antonio Jose Aguilar Bravo
# Let v3.0
# Script LetNetwork.sh
# Script para cambiar la configuracion de red
#

Salir() {
	CODE=$1
        echo "Uso: $0 dhcp"
        echo "Uso: $0 static IP MASK GATEWAY"
        echo "Uso: $0 hostname HOSTNAME"
        echo "Uso: $0 dns DNS1 DNS2 ..."
        exit $CODE
	                                        
}
PARAMS=$#
if [ ${PARAMS} -eq 0 ] ; then
	 Salir 1
fi

TIPO=$1

NETDIRCONF=/etc/network
NETFILECONF=interfaces
DNSFILE=/etc/resolv.conf
HOSTNAMEFILE=/etc/hostname
HOSTSFILE=/etc/hosts

DATE=$(date)
HOSTNAME=$(hostname)

case $TIPO in 
	"dhcp")
cat <<EOF > ${NETDIRCONF}/${NETFILECONF}
# Fecha creacion: ${DATE}
# Fichero configuracion LetNetwork.sh

auto lo
iface lo inet loopback

auto eth0
iface eth0 inet dhcp
hostname ${HOSTNAME}
EOF
		;;
	"static")
		if [ ${PARAMS} -eq 4 ]; then
			IP=$2
			MASK=$3
			GATEWAY=$4
cat <<EOF > ${NETDIRCONF}/${NETFILECONF}
# Fecha creacion: $DATE
# Fichero configuracion LetNetwork.sh

auto lo 
iface lo inet loopback

auto eth0
iface eth0 inet static
address ${IP}
netmask ${MASK}
gateway ${GATEWAY}
EOF
		else
			Salir 1 
		fi
		;;

	"hostname")
	        if [ ${PARAMS} -eq 2 ]; then
	        	LETNFS=/mnt/letconfig/LetNfs.sh
	        	NEWHOSTNAME=$2
	        	OLDHOSTNAME=${HOSTNAME}
	        	if [ "${OLDHOSTNAME}" == "${NEWHOSTNAME}" ] ; then
	        		echo "Nombre no valido"
	        		exit 100
	        	fi 
	        	
	        	echo ${NEWHOSTNAME} > ${HOSTNAMEFILE}
	        	
	        	/bin/hostname -F ${HOSTNAMEFILE}
	        	
	        	sed -i 1s/${OLDHOSTNAME}/${NEWHOSTNAME}/ ${HOSTSFILE}

			# cambiar hostname en $NETFILECONF en caso de tener IP dinamica
			if [ $(grep -c dhcp ${NETDIRCONF}/${NETFILECONF}) -ne 0 ]; then
				sed -i -r "s/${OLDHOSTNAME}/${NEWHOSTNAME}/g" ${NETDIRCONF}/${NETFILECONF}
			fi
	        	
	        	# comprobar fstab para los casos de despliegue NFS 
	        	. ${LETNFS} HOSTNAMECHANGE
	        	
	        else
	        	Salir 1
	        fi
		;;
	"dns")
		if [ ${PARAMS} -ge 2 ]; then
			rm -rf ${DNSFILE}

cat <<EOF >> ${DNSFILE}
# Fecha creacion: ${DATE}
# Fichero configuracion LetNetwork.sh
EOF
			shift
			for DNS in $* 
			do
				echo "nameserver ${DNS}" >> $DNSFILE
			done
		else
			Salir 1
		fi
		;;			
	*)
		Salir 1
		;;

esac

exit 0
