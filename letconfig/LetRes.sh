#!/bin/sh
# Copyright (C) 2007 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published
# by the Free Software Foundation; version 2 only.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#

# GPL - Antonio Jose Aguilar Bravo
# Let v3.0
# Script LetRes.sh
# Script para cambiar la resolucion de pantalla 
# Ficheros necesarios /etc/X11/xorg.conf.800 /etc/X11/xorg.conf.1024 /etc/X11/xorg.conf.1280
# Cambio de Fondo escritorio /opt/hpdata/letbg-800|1024|1280.jpg
# /etc/xautolock
# Screensaver timeout 5
# Lock Screen yes|no

if [ -z $1 ] ; then
	echo "Uso: $0 (800|1024|1280) [SCRSVR timeout] [LOCK ON|OFF]"
	exit 1
fi
                
if [ $1 != 800 -a $1 != 1024 -a  $1 != 1280 ] ; then
	echo "Uso: $0 (800|1024|1280)"
	exit 1
fi

PARAMS=$#
RES=$1
DATE=`date`
DIRCONF=/etc/X11
DEFAULTBG=/mnt/hpdata/icebg.jpg
LETBG=/mnt/hpdata/letbg-$RES.jpg
AUTOLOCKFILE=/etc/xautolock

if [ -f "$DIRCONF/xorg.conf.$RES" -a -f "$LETBG" ] ; then

	rm -f $DIRCONF/xorg.conf
	ln -sf $DIRCONF/xorg.conf.$RES $DIRCONF/xorg.conf
	rm -f $DEFAULTBG
	ln -sf $LETBG $DEFAULTBG 
	
else
	echo "Uso: $0 (800|1024|1280)"
        echo "ERROR, no existen los ficheros de configuraci�n:"
        echo "$DIRCONF/xorg.conf.$RES � $LETBG"
        exit 100
fi

# Configuracion Salvapantallas y Xlock
. $AUTOLOCKFILE         # defaults
case $PARAMS in
	1)	;;
	2)
		TIME=$2
		BLOCK=no
		;;
	3)
		case $3 in
			"ON")
				BLOCK=yes
				;;
			"OFF")
				BLOCK=no
				;;
			*)
				echo "Uso: $0 (800|1024|1280) [SCRSVR timeout] [LOCK ON|OFF]"
				exit 1
				;;
		esac
		TIME=$2
		;;
	*)
		echo "Uso: $0 (800|1024|1280) [SCRSVR timeout] [LOCK ON|OFF]"
		exit 1
		;;
esac
cat <<EOF > /etc/xautolock
# Fecha creacion: $DATE
# Fichero configuracion Autolock
# BLOCK yes|no Bloquea o desbloquea el salvapantallas 
# TIME tiempo de inactividad para ejecutar el salvapantalla

BLOCK=$BLOCK
TIME=$TIME
EOF

exit 0
