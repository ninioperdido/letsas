#!/bin/sh
DIALOG=Xdialog

icon="/usr/share/icons/"
archivo="Web1"
sesion="Web1"
ruta="/mnt/desktop/"
rutautil="/mnt/letconfig/"
dir="www.hp.es"
seleccion=""
flag="true"
flag1="true"



while [ "$flag1" = "true" ]; do

	flag="true"
 
	$DIALOG --title "Asistente Lanzador WEB" --icon "$icon"Tango/48x48/mimetypes/html.png --center --backtitle "Elija la opción que desee y pulse siguiente para  continuar con el\nasistente que le ayudará a gestionar sus lanzadores WEB"  --ok-label "Siguiente" --cancel-label "Cancelar" --radiolist "Seleccione qué desea hacer" 17 55 8 "Crear" "Crear un lanzador WEB" on "Eliminar" "Elimina un lanzador WEB existente" off 2> /tmp/choice.tmp.$$

	retval=$?

	seleccion=`cat /tmp/choice.tmp.$$`
	rm -f /tmp/choice.tmp.$$
	
	case $retval in
	0)
		if [ "$seleccion" = "Crear" ]; then
			



			while [ "$flag" = "true" ]; do #Mientras no guardemos o cancelemos, seguimos en el bucle

				$DIALOG --title "Asistente Lanzador WEB" --separator "|" --icon "$icon"Tango/32x32/actions/document-save.png  --backtitle "Creación de lanzador WEB" --left  --ok-label "Guardar" --cancel-label "Cancelar" --2inputsbox "Introduzca los datos de la sesión" 17 52 "Dirección web" "$dir" "Nombre del lanzador" "$sesion" 2> /tmp/login.tmp.$$

				retval=$?


				case $retval in
			  	0)
					sesion=`awk -F "|" '{ print $2 }' /tmp/login.tmp.$$` #Extraemos el archivo
					dir=`awk -F "|" '{ print $1 }' /tmp/login.tmp.$$`    #Extraemos la dirección o nombre del host
					archivo="$sesion.desktop"

					rm -f /tmp/login.tmp.$$
		
		
					aux=`ls "$ruta$archivo"`
					aux2=`cat /root/.config/rox.sourceforge.net/ROX-Filer/pb_Desktop | grep "$sesion"`

	
					if [ "$ruta$archivo" != "$aux" -o "$aux2" = "" ]; then #comprobamos si el archivo ya existe

						"$rutautil"roxSOAP.sh remove "$ruta$archivo" #Eliminamos el icono para no duplicarlo
						echo "[Desktop Entry]" >"$ruta$archivo"
						echo "Exec= /opt/firefox/firefox $dir" >>"$ruta$archivo"	#creamos el archivo .desktop
						echo "Icon="$icon"Tango/48x48/mimetypes/html.png" >>"$ruta$archivo"
						echo "Name=$sesion" >>"$ruta$archivo"
						x=`/mnt/letconfig/LetPosIcon.sh | awk '{print $1}'`
						y=`/mnt/letconfig/LetPosIcon.sh | awk '{print $2}'`
						"$rutautil"roxSOAP.sh add "$ruta$archivo" $x $y "$sesion"	#Añadimos el icono en el escritorio
						$DIALOG --title "Información" --icon "$icon"Tango/32x32/status/dialog-information.png --no-cancel --msgbox "La sesión ha sido creada con éxito" 0 0

						flag="false"	 	#Salimos del bucle de guardado
						flag1="false"

					else	#Si el archivo existe, pedimos confirmación para guardarlo

						$DIALOG --title "Aviso" --icon "$icon"Tango/32x32/status/dialog-warning.png --ok-label "Sí" --cancel-label "No" --yesno "El archivo ya existe ¿desea sobreescribirlo?" 0 0

						retval=$?

				 		case $retval in 	#Si aceptamos sobreescribir, guardamos
				  		0)
	 
							"$rutautil"roxSOAP.sh remove "$ruta$archivo" #Eliminamos el icono para no duplicarlo
							echo "[Desktop Entry]" >"$ruta$archivo"
							echo "Exec= /opt/firefox/firefox $dir" >>"$ruta$archivo"	#creamos el archivo .desktop
							echo "Icon="$icon"Tango/48x48/mimetypes/html.png" >>"$ruta$archivo"
							echo "Name=$sesion" >>"$ruta$archivo"
							x=`/mnt/letconfig/LetPosIcon.sh | awk '{print $1}'`
							y=`/mnt/letconfig/LetPosIcon.sh | awk '{print $2}'`		
							"$rutautil"roxSOAP.sh add "$ruta$archivo" $x $y "$sesion"	#Añadimos el icono en el 
escritorio
							$DIALOG --title "Información" --icon "$icon"Tango/32x32/status/dialog-information.png --no-cancel --msgbox "La sesión ha sido creada con éxito" 0 0

							flag="false"
							flag1="false";;

				  		1)
							echo "Volvemos a la ventana de guardar";;		#No queremos sobreescribir, volvemos a la ventana de guardar
						255)
							echo "Cerrada ventana de confirmación, volvemos a la ventana de guardar";;
				 		esac		#Si cerramos la confirmación, volvemos a la ventana de guardar
					fi
				;;
	 
			  	1)
			    		rm -f /tmp/login.tmp.$$
			    		echo "Cancelado creación de lanzador WEB"
					flag="false";;
	
				255)
					rm -f /tmp/login.tmp.$$
					echo "Cerrada ventana de configuración lanzador WEB"
					flag="false";;

				esac
			done 
		else
			while [ "$flag" = "true" ]; do #Mientras no borremos o cancelemos, seguimos en el bucle

				$DIALOG --title "Asistente Lanzador WEB" --icon "$icon"Tango/32x32/actions/eraser.png --backtitle "¡¡¡Atención!!!, va a proceder a la eliminación de un lanzador WEB" --left  --ok-label "Eliminar" --cancel-label "Cancelar" --inputbox "Introduzca el nombre del lanzador" 0 0 "$sesion" 2> /tmp/login.tmp.$$

				retval=$?

	
				case $retval in
			  	0)
					sesion=`cat /tmp/login.tmp.$$` #Extraemos el nombre de la sesión 
					archivo="$sesion.desktop"
			
					rm -f /tmp/login.tmp.$$


					$DIALOG --title "Aviso" --icon "$icon"Tango/32x32/status/dialog-warning.png --ok-label "Sí" --cancel-label "No" --yesno "Ha seleccionado eliminar el lanzador '$sesion', ¿desea continuar?" 0 0

					retval=$?

			
			 		case $retval in 	#Si aceptamos, eliminamor
			  		0)
						
						aux2=`cat "$ruta$archivo" | grep "html.png"`
						aux3=`cat .config/rox.sourceforge.net/ROX-Filer/pb_Desktop | grep "$sesion"`
						if [ -e "$ruta$archivo" ]; then #Si existe el archivo
							if [ "$aux2" != "" ]; then #Si es del tipo adecuado
			 
								"$rutautil"roxSOAP.sh remove "$ruta$archivo" #Eliminamos el icono 	
								rm -f "$ruta$archivo" 2> /dev/null #Eliminamos el archivo

								$DIALOG --title "Información" --icon "$icon"Tango/32x32/status/dialog-information.png --no-cancel --msgbox "El lanzador ha sido eliminado con éxito" 0 0
							else	#Si no es del tipo adecuado damos aviso
								$DIALOG --title "Aviso" --icon "$icon"Tango/32x32/status/dialog-warning.png --no-cancel --msgbox "Está intentando eliminar un acceso que no es un lanzador WEB, permiso denegado" 0 0
							fi
						else 	#Si el archivo no existe comprobamos que exista el icono
							if [ "$aux3" != ""]; then

								"$rutautil"roxSOAP.sh remove "$ruta$archivo" #Eliminamos el icono 
								rm -f "$ruta$archivo" 2> /dev/null
								$DIALOG --title "Información" --icon "$icon"Tango/32x32/status/dialog-information.png --no-cancel --msgbox "El lanzador ha sido eliminado con éxito" 0 0
							else #si no existe, damos el aviso
								$DIALOG --title "Aviso" --icon "$icon"Tango/32x32/status/dialog-warning.png --no-cancel --msgbox "Está intentando eliminar un acceso que no existe." 0 0
							fi
						fi

						flag="false" #Salimos del bucle
						flag1="false";;

				  	1)
						echo "Volvemos a la ventana de eliminar";;#No queremos eliminar, volvemos atrás						
					255)
						echo "Cerrada ventana de confirmación, volvemos a la ventana de guardar";;
			 		esac	#Si cerramos la confirmación, volvemos a la ventana de guardar
					;;
	 
				1)
			    		rm -f /tmp/login.tmp.$$
			    		echo "Cancelado borrado de lanzador WEB"
					flag=false;;

				
				255)
					rm -f /tmp/login.tmp.$$
					echo "Se ha cerrado el asistente"
					flag="false"
					flag1="false";;
	
				esac
			done
       
		fi;;
	1)
		echo "Cancelado asistente"
		flag1="false";;
	255)
		echo "Ventana de asistente cerrada"
		flag1="false";;
	esac
done

       
