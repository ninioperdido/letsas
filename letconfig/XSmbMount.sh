#!/bin/sh

comando="mount.cifs"
ruta_montaje="/home/usuario/REMOTO"
ruta_icono="/usr/share/icons/Tango/32x32"
archivo_datos="/etc/sysconfig/smb.info"
cod_error="0"
SMB_USER=""
SMB_PASS=""

aux=`mount | grep "$ruta_montaje"`
#comprobamos que la unidad ya esté montada
if [ "$aux" = "" ]; then

	if [ ! -e "$ruta_montaje" ]; then #Si no existe la ruta de montaje, la creamos
		mkdir "$ruta_montaje"
	fi

	#verificar si existe el archivo /etc/smb.info
	if [ ! -e "$archivo_datos" ]; then
		cod_error="1" #No existe el archivo de configuración
	else

		SMB_HOST=`hostname` #Pedimos el nombre de host
		. $archivo_datos #Incorporamos las variables desde el archivo de configuración

		#Comprobamos que tipo de montaje tenemos que hacer
		if [ "$SMB_TYPE" != "NFS" ]; then

			#En caso de SMB comprobamos que existan los datos usuario y contraseña y los pedimos si faltan
			if [ "$SMB_USER" = "" -o "$SMB_PASS" = "" ]; then
				aux=`Xdialog --stdout --separator "|" --icon "$ruta_icono/apps/config-users.png" --title "Unidad Remota" --backtitle "Introduzca identificación de usuario remoto" --left --password --2inputsbox "Identificación remota" 0 0 "Usuario" "Usuario" "Contraseña" ""`

				ret=$?

				case $ret in
				0)
	
					SMB_USER=`echo "$aux" | awk -F "|" '{ print $1}'`
					SMB_PASS=`echo "$aux" | awk -F "|" '{ print $2}'`

					. $archivo_datos #Incorporamos las variables desde el archivo de configuración

					if [ "$SMB_USER" = "" ]; then
						cod_error="2" #Error de autenticación
					fi 
					;;
				1)
					cod_error="2"
					;;
				255)
					cod_error="2"
					;;
				esac
			fi
	
		fi			
		#Si está todo completo, se llama al comando.
		if [ "$cod_error" = "0" -o "$cod_error" = "2" ]; then

			#Comprobamos que tipo de montaje tenemos que hacer
			if [ "$SMB_TYPE" = "NFS" ]; then
				comando="mount"
				$comando $ruta_montaje 1> /tmp/error.tmp.$$
			else			
				if [ "$SMB_DOM" = "" ]; then
					$comando //$SMB_IP$SMB_PATH $ruta_montaje -o user=$SMB_USER,pass=$SMB_PASS -n 1> /tmp/error.tmp.$$
				else
					$comando //$SMB_IP$SMB_PATH $ruta_montaje -o user=$SMB_USER,pass=$SMB_PASS,dom=$SMB_DOM -n 1> /tmp/error.tmp.$$
				fi
			fi
			aux=`cat /tmp/error.tmp.$$`
			if [ "$aux" != "" ]; then #Se sacan los mensajes de error por pantalla
				Xdialog --title "Aviso" --icon "$ruta_icono/status/error.png" --no-cancel --msgbox "$aux" 0 0 0
			fi
			
			rm -f /tmp/error.tmp.$$
			aux=`mount | grep "$ruta_montaje"`
			if [ "$aux" != "" ]; then #Si la unidad ya está montada llamamos al pcmanfm
				pcmanfm $ruta_montaje &
				cod_error="0"
			else			#Si no damos mensaje de error
				Xdialog --title "Aviso" --icon "$ruta_icono/status/error.png" --no-cancel --msgbox "No ha sido posible montar la unidad, \nel servidor rechazó la petición o el recurso no está disponible" 0 0 0
			fi
		fi
	fi
	if [ "$cod_error" = "1" ]; then
		Xdialog --title "Aviso" --icon "$ruta_icono/status/error.png" --no-cancel --msgbox "No está autorizado a abrir este recurso compartido.\nContacte con su administrador para obtener los permisos necesarios." 0 0 0
	else
		if [ "$cod_error" = "2" ]; then
			Xdialog --title "Aviso" --icon "$ruta_icono/status/error.png" --no-cancel --msgbox "No ha introducido datos correctos de usuario" 0 0 0
		fi
	fi
else
	pcmanfm $ruta_montaje &
fi			
					

		



