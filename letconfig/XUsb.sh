#!/bin/sh

####################################
# Desmontar / Montar la unidad USB #
####################################


Xdialog --title "Montar/Desmontar USB" --icon /usr/share/icons/Tango/32x32/devices/usbpendrive_mount.png --radiolist "Elija qué desea hacer" 12 45 0 "Montar" "USB" on "Desmontar" "USB" off 2>/tmp/usb.tmp.$$
retval=$?
choice=`cat /tmp/usb.tmp.$$`
rm -f /tmp/usb.tmp.$$

case $retval in
 0)
	case $choice in
    	Montar)
		aux=`cat /etc/fstab | grep USB` #Si no está permitido montar el usb
		if [ "$aux" = "" ]; then
			Xdialog --title "aviso" --icon /usr/share/icons/Tango/32x32/status/error.png --no-cancel --msgbox "No está autorizado a utilizar dispositivos de almacenamiento USB.\nContacte con su administrador para habilitarle esta opción" 0 0 0	
       		else
		     	aux=`mount | grep USB`
     			if [ "$aux" = "" ]; then #Si la unidad no está ya montada
				if [ ! -e /dev/sda1 -a ! -e /dev/sda ]; then #Si el dispositivo no está conectado pedimos que lo conecte
					Xdialog --title "Aviso" --icon /usr/share/icons/Tango/32x32/status/info.png --no-cancel --msgbox "Conecte el dispositivo" 0 0 0
					if [ ! -e /dev/sda1 -a ! -e /dev/sda ]; then #Si aún no está conectado esperamos 3 segundos
						sleep 3
					fi
				fi

				if [ -e /dev/sda1 -o -e /dev/sda ]; then #Si ya existe el dispositivo, lo montamos
					aux="/mnt/home/usuario/USB" #Si no existe el directorio lo crea
					if [ ! -e $aux ]; then
						mkdir $aux
       					fi	
       					mount /dev/sda1 2>/tmp/usb_status.tmp.$$
       					estado=`cat /tmp/usb_status.tmp.$$`
       					rm /tmp/usb_status.tmp.$$
					if [ "$estado" = "" ]; then
		 				Xdialog --title "aviso" --icon /usr/share/icons/Tango/32x32/status/info.png --no-cancel --msgbox "Dispositivo montado correctamente" 0 0 0
					else
		 				mount /dev/sda 2>/tmp/usb_status.tmp.$$
		 				estado=`cat /tmp/usb_status.tmp.$$`
						rm /tmp/usb_status.tmp.$$
						if [ "$estado" = "" ]; then
		  					Xdialog --title "aviso" --icon /usr/share/icons/Tango/32x32/status/info.png --no-cancel --msgbox "Dispositivo montado correctamente" 0 0 0
		 				else	
		  					Xdialog --title "Aviso" --icon /usr/share/icons/Tango/32x32/status/error.png --no-cancel --msgbox "$estado" 0 0 0
		 				fi
        				fi
       				
       					aux=`mount | grep USB`
       					if [ "$aux" != "" ]; then

						aux=`whoami`
						if [ "$aux" != "root" ]; then				
							aux=".config/rox.sourceforge.net/ROX-Filer/Unidad USB.desktop"
							/mnt/letconfig/roxSOAP.sh remove "$aux" #Eliminamos el icono para no duplicarlo
							echo "[Desktop Entry]" >"$aux"
							echo "Exec=pcmanfm /mnt/home/usuario/USB" >> "$aux"
							echo "Icon=/usr/share/icons/Tango/48x48/devices/usbpendrive.png" >>"$aux"
							echo "Name=Unidad USB" >>"$aux"
							x=`/mnt/letconfig/LetPosIcon.sh | awk '{print $1}'`
							y=`/mnt/letconfig/LetPosIcon.sh | awk '{print $2}'`
							/mnt/letconfig/roxSOAP.sh add "$aux" $x $y "Unidad USB"	#Añadimos el icono en el 
escritorio
						fi
						
						pcmanfm /home/usuario/USB & #Llamamos al administrador de archivos
       					fi
				else
					Xdialog --title "aviso" --icon /usr/share/icons/Tango/32x32/status/dialog-warning.png --no-cancel --msgbox "Dispositivo USB no detectado.\nVerifique el dispositivo USB o repita el proceso de montaje." 0 0 0
				fi
			else
				pcmanfm /mnt/home/usuario/USB &
			fi
		fi;;
	Desmontar)
		aux=`mount | grep USB`
     		if [ "$aux" != "" ]; then
     			sync
			umount /dev/sda1 2>/tmp/usb_status.tmp.$$
			estado=`cat /tmp/usb_status.tmp.$$`
			rm /tmp/usb_status.tmp.$$
     			if [ "$estado" = "" ]; then
				aux=`whoami`
				if [ "$aux" != "root" ]; then
					aux=".config/rox.sourceforge.net/ROX-Filer/Unidad USB.desktop"
					/mnt/letconfig/roxSOAP.sh remove "$aux" #Eliminamos el icono
					rm "$aux" #eliminamos el archivo
				fi
      				Xdialog --title "Aviso" --icon /usr/share/icons/Tango/32x32/status/info.png --no-cancel --msgbox "Ya puede retirar el hardware con seguridad" 0 0 3000
     			else
      				umount /dev/sda 2>/tmp/usb_status.tmp.$$
      				estado=`cat /tmp/usb_status.tmp.$$`
      				rm /tmp/usb_status.tmp.$$
      				if [ "$estado" = "" ]; then
					aux=`whoami`
					if [ "$aux" != "root" ]; then
						aux=".config/rox.sourceforge.net/ROX-Filer/Unidad USB.desktop"
						/mnt/letconfig/roxSOAP.sh remove "$aux" #Eliminamos el icono
						rm "$aux" #Eliminamos el archivo
					fi
      					Xdialog --title "Aviso" --icon /usr/share/icons/Tango/32x32/status/info.png --no-cancel --msgbox "Ya puede retirar el hardware con seguridad" 0 0 3000
      				else
       					Xdialog --title "Aviso" --icon /usr/share/icons/Tango/32x32/status/error.png --no-cancel --msgbox "$estado" 0 0 0
      				fi
     			fi
		else
			Xdialog --title "Aviso" --icon /usr/share/icons/Tango/32x32/status/info.png --no-cancel --msgbox "No hay ninguna unidad USB montada" 0 0 3000
		fi;;
	esac;;
1)
	echo "Montaje de USB cancelado";;
255)
	echo "Ventana de confirmación cerrada";;
esac
   
