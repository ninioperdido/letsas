#!/bin/sh
# Copyright (C) 2007 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published
# by the Free Software Foundation; version 2 only.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#

# GPL - Antonio Jose Aguilar Bravo
# Let v1.0
# Script LetRemoveIcon.sh
# Script para eliminar iconos del escritorio del administrador
# Parametro: Path de la aplicacion del icono .desktop


Salir(){

echo "Uso: $0 file.desktop"
exit 1

}
[ $# -ne 1 ] && Salir 
ICONO=$1
PBDESKTOP="/root/.config/rox.sourceforge.net/ROX-Filer/pb_Desktop"
LINEA=$(grep -c $ICONO $PBDESKTOP |cut -d: -f1)
[ "$LINEA" -eq 0 ] && exit 0
sed -i -e "/$ICONO/d" $PBDESKTOP
