#!/bin/sh
# Copyright (C) 2007 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published
# by the Free Software Foundation; version 2 only.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#

# GPL - Antonio Jose Aguilar Bravo
# Let v3.0
# Script LetNtp.sh
# Script para configurar la sincronizacion de fecha y hora mediante netdate  
# [REMOVE] elimina la configuracion de NTP

if [ $# -ne 1 ]; then
	echo "Uso: $0 NTPServer"
	exit 1
fi


if [ "$1" == "REMOVE" ]; then
	rm -rf /etc/ntpdate.conf
	exit 0
fi

NTPSERVER=$1
DATE=`date`
SERVICE=/etc/init.d/ntpdate

cat <<EOF > /etc/ntpdate.conf
# Fecha creacion: $DATE 
# Fichero configuracion Netdate
# NTPHOST = IP o nombre del servidor de hora 
# NTPLIMIT = timeout en segundos

NTPHOST=$NTPSERVER
NTPLIMIT=5
EOF

$SERVICE start
if [ $? -nq 0 ]; then
	echo "No puede conectar con el servidor NTP: $NTPSERVER"
 	exit 100
fi

exit 0
