#!/bin/sh
# Copyright (C) 2007 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published
# by the Free Software Foundation; version 2 only.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#

# GPL - Antonio Jose Aguilar Bravo
# Let v3.0
# Script LetAltiris.sh
# Script para configurar el agente Altiris
# Parametros : IP [PUERTO TCP]

PARAMS=$#

ALTIRIS_DIRCONF=/opt/altiris/deployment/adlagent/conf
ALTIRIS_FILENAMECONF=adlagent.conf
TEMPLATE=$ALTIRIS_DIRCONF/adlagent.conf.template

if [ $PARAMS -eq 0 ];  then
	echo "Uso: $0 IPServidorAltiris [Puerto TCP]"
	exit 1
fi

IPADDR=$1
case $PARAMS in
	1)
		cat $TEMPLATE | sed -e "s/<IPADDR>/$IPADDR/" -e "s/<TCPPORT>/402/" > $ALTIRIS_DIRCONF/$ALTIRIS_FILENAMECONF
		;;
	2)
		TCPPORT=$2
		cat $TEMPLATE | sed -e "s/<IPADDR>/$IPADDR/" -e "s/<TCPPORT>/$TCPPORT/" > $ALTIRIS_DIRCONF/$ALTIRIS_FILENAMECONF
		;;
	*)
		echo "Uso: $0 IPServidorAltiris [Puerto TCP]"
		exit 1
		;;
esac

exit 0
