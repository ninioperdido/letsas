#!/bin/sh
# Copyright (C) 2007 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published
# by the Free Software Foundation; version 2 only.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#

# GPL - Antonio Jose Aguilar Bravo
# Let v1.0
# Script LetAddIcon.sh
# Script para a�adir iconos al escritorio del administrador
# Parametro: ICONSTRING


Salir(){

echo "Uso: $0 ICONSTRING"
exit 1

}
[ $# -ne 1 ] && Salir 
ICONO=$1
PBDESKTOP="/root/.config/rox.sourceforge.net/ROX-Filer/pb_Desktop"
EXISTE=$(grep -c remoto.desktop $PBDESKTOP |cut -d: -f1)
if [ $EXISTE -eq 0 ]; then
	LINEAS=$(/bin/cat $PBDESKTOP | wc -l)
	LINEA=$(($LINEAS - 1))
	SCRIPT=${LINEA}a${ICONO}
	sed -i -e "$SCRIPT" $PBDESKTOP
else
	exit 1
fi
