#!/bin/sh
# Copyright (C) 2007 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published
# by the Free Software Foundation; version 2 only.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#

# GPL - Antonio Jose Aguilar Bravo
# Let v1.0
# Script LetUsb.sh
# Script que a�ade linea la /etc/fstab para el uso de memorias usb
# Parametro: ON | OFF

Salir(){
	CODE=$1
	echo "Uso: $0 ON|OFF "
	exit $CODE
}


EscribeFSTAB(){
cat <<EOF >> $FSTAB
/dev/sda       /home/usuario/USB       vfat    defaults,users,noauto,exec,sync 0 0
/dev/sda1       /home/usuario/USB       vfat    defaults,users,noauto,exec,sync 0 0
EOF
}


EliminarFSTAB(){
	LINEA=$(grep -c sda /etc/fstab |cut -d: -f1)
	[ "$LINEA" -eq 0 ] && exit 0
	
	# eleiminar una linea concreta con grep -n --> sed -i "$LINEA"d $1 
	
	sed -i -e '/sda/d' $1
	

}

PARAMS=$#

if [ $PARAMS -ne 1 ]; then 
	Salir 1
fi

OPC=$1

USBDIR=/home/usuario/USB
FSTAB=/etc/fstab

case $OPC in
	"ON")
		# comprobacion en el usuario [ -d $USBDIR ] ||  mkdir -p $USBDIR
		[ -f $FSTAB ] || touch $FSTAB
		VALUE=`grep sda1 $FSTAB`
		if [ -z "$VALUE" ]; then
			EscribeFSTAB
		else
			exit 0
		fi
		;;
	"OFF")
		[ -d $USBDIR ] || exit 0
		[ -f $FSTAB ] || exit 0
		EliminarFSTAB $FSTAB
		;;
	*)
		Salir 1
		;;
esac

