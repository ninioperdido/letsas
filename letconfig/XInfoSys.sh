#!/bin/sh

###########################################
# Presentaci�n de información del sistema #
###########################################

/mnt/letconfig/LetSummary.sh

Xdialog --title "Información del sistema" --icon /usr/share/icons/Tango/32x32/mimetypes/info.png --no-cancel --ok-label "Aceptar" --smooth --logbox /tmp/summary.info 39 60

exit 0

