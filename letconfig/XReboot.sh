#!/bin/sh

#############################################
# Muestra el diálogo de reinicio de sistema #
#############################################

Xdialog --title "Reiniciar equipo" --icon /usr/share/icons/Tango/32x32/actions/gtk-refresh.png --default-no --ok-label "Sí" --cancel-label "No" --yesno "¿Desea reiniciar el equipo?" 6 50 3000
case $? in
0)
  reboot;;
1)
  echo "Reinicio cancelado";;
255)
  echo "Ventana de reinicio cerrada";;
esac
