#!/bin/sh
# Copyright (C) 2007 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published
# by the Free Software Foundation; version 2 only.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#

# GPL - Antonio Jose Aguilar Bravo
# Let v3.0
# Script LetDateTime.sh
# Script para ajustar la fecha y hora del sistema y zona horaria
#

Salir(){
	CODE=$1
	echo "Uso: $0 HH(Hora) MM(Minutos) [ DD(Dia) MM(Mes) AAAA(A�o) ] ZonaHoraria (Peninsula|Canarias)"
	exit $CODE
}

CambiarZona(){
	ZONA=$1
	case $ZONA in
		"Peninsula")
                	rm -rf /etc/localtime
        		ln -sf /usr/share/zoneinfo/Europe/Peninsula /etc/localtime
        		if [ $? -ne 0 ]; then 
        			Salir 100
        		fi 
                	;;
        	"Canarias")
                	rm -rf /etc/localtime
                	ln -sf /usr/share/zoneinfo/Europe/Canarias /etc/localtime
                	if [ $? -ne 0 ]; then
                		Salir 100
                	fi
                	                                                        
         		;;
         	*)
         		Salir 100
         		;;
esac                                                                                                                                                                                                                                                
}

HORA=$1
MINUTO=$2

case $# in
	2)
		date -s $HORA:$MINUTO
		OUT=$?
		;;
	3)
		ZONA=$3
		CambiarZona $ZONA
		date -s $HORA:$MINUTO
		OUT=$?
		;;
	6)
		ZONA=$6
		DIA=$3
		MES=$4
		ANIO=$5
		CambiarZona $ZONA
		date -s $MES$DIA$HORA$MINUTO$ANIO
		OUT=$?
		;;
	*)
		Salir 1
		;;
esac

if [ $OUT -eq 0 ]; then 
	hwclock -w -u
else
   Salir 1
fi

exit 0

