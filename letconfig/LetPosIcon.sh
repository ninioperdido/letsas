#!/bin/sh
# Copyright (C) 2007 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published
# by the Free Software Foundation; version 2 only.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#

# GPL - Jose Garcia Robles

#Comprueba si hay argumentos
if [ $# gt 0 ]; then
	echo "Devuelve la primera posicion libre para colocar un icono"
	echo
	echo "uso: $0"
	exit 1
fi
#Archivo pb_Desktop
file="/opt/rox/rox.sourceforge.net/ROX-Filer/pb_Desktop"
#tamaño de celda
TAM=96
#Comprueba la resolución
res=`ls -l /etc/X11/xorg.conf | awk '{print $11}' | awk -F "." '{print $3}'`
case $res in
	1280)
		maxx=$((1280-$TAM))
		maxy=$((1024-$TAM))
	;;
	1024)
		maxx=$((1024-$TAM))
		maxy=$((768-$TAM))
	;;
	800)
		maxx=$((800-$TAM))
		maxy=$((600-$TAM))
	;;
esac
#Posición de la esquina superior de la primera celda
ini_x=48
ini_y=80
#Inicializamos la celda a valor ocupado
cell="busy"
#Numero de iconos
icon_n=`grep icon $file | wc -l`
#Archivo temporal para procesar los iconos
icon_tmp="/tmp/icon.tmp"
x=$ini_x
while [ $x -lt $maxx -a "$cell" = "busy" ]; do
	y=$ini_y
	while [ $y -lt $maxy -a "$cell" = "busy" ]; do
		grep icon $file > $icon_tmp
		cell="free"
		k=1
		while [ $k -le $icon_n -a "$cell" = "free" ]; do
	 		#obtener coordenadas
			icon_x=`cat $icon_tmp | awk '{ if(NR==1){print $0}}' | awk -F '"' '{print $2}'`
			icon_y=`cat $icon_tmp | awk '{ if(NR==1){print $0}}' | awk -F '"' '{print $4}'`
			#eliminar la linea leida del archivo temporal
			cat $icon_tmp | awk '{ if(NR>1){print $0}}' > /tmp/aux.tmp
			mv -f /tmp/aux.tmp $icon_tmp
			#comprobar que está dentro de la celda actual
			if [ $icon_x -gt $x -a $icon_x -lt $(($x+$TAM)) ]; then
				if [ $icon_y -gt $y -a $icon_y -lt $(($y+$TAM)) ]; then
					cell="busy"
				fi
			fi
			k=$(($k+1))
		done
		if [ "$cell" = "busy" ]; then
			y=$(($y+$TAM))
		fi
		
	done
	if [ "$cell" = "busy" ]; then
		x=$(($x+$TAM))
	fi

done
rm -f $icon_tmp
echo "$(($x+$TAM/2)) $(($y+$TAM/2))"
exit 0

