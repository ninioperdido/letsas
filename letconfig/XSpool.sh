################################
# Limpiar la cola de impresión #
################################

Xdialog --title "Limpiar cola de impresión" --icon /usr/share/icons/Tango/32x32/devices/printer.png --default-no --ok-label "Sí" --cancel-label "No" --yesno "¿Desea cancelar todos los trabajos de impresión?" 0 0 0
case $? in
 0)
  /etc/init.d/lpd clean &> /tmp/printer.tmp.$$
  cat /tmp/printer.tmp.$$
  status=`cat /tmp/printer.tmp.$$`

  rm -f /tmp/printer.tmp.$$ 
  if [ "$status" != ""]; then
   Xdialog --title "Aviso" --icon /usr/share/icons/Tango/32x32/status/dialog-warning.png --no-cancel --msgbox "$status" 0 0 0
  fi;;
 1)
  echo "Limpieza de cola cancelado";;
 255)
  echo "Ventana de limpieza de cola cerrada";;
esac
