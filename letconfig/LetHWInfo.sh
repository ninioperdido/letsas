#!/bin/sh
# Copyright (C) 2007 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published
# by the Free Software Foundation; version 2 only.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#

# GPL - Antonio Jose Aguilar Bravo
# Let v3.0
# Script LetHWInfo.sh
# Script para obtener la informacion de HW del terminal.
# Solo se ejecutará si no existe el fichero /etc/sysconfig/hardware.info
#

FILECONF=/tmp/hardware.info
HWINFO=/usr/bin/hwinfo

if [ -f $FILECONF ] ; then
	exit 0
else
	$HWINFO --bios --disk > $FILECONF
	if [ $? -eq 0 ]; then
		exit 0;
	else
		echo "Error al generar el fichero de informacion de hardware: $FILECONF"
		exit 1;
	fi		
fi
