#!/bin/sh

#############################################
# Muestra el diálogo de bloqueo de pantalla #
#############################################

Xdialog --title "Bloquear sesion" --icon /usr/share/icons/Tango/32x32/actions/system-lock-screen.png --default-no --ok-label "Sí" --cancel-label "No" --yesno "¿Desea bloquear la sesión?" 6 50 3000
case $? in
0)
  xlock -mode bat;;
1)
  echo "Bloqueo de sesión candelado";;
255)
  echo "Ventana de bloqueo de sesión cerrada";;
esac
