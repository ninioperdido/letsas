#!/bin/sh
# Copyright (C) 2007 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published
# by the Free Software Foundation; version 2 only.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#

# GPL - Antonio Jose Aguilar Bravo
# Let v3.0
# Script LetVNC.sh
# Script para configurar servidor VNC
# LOPD para pedir confirmacion al usuario para tomar el control remoto del terminal

Salir(){
	CODE=$1
	echo "Uso: $0 passwd password-string"
	echo "Uso: $0 start"
	echo "Uso: $0 startlopd"
	echo "Uso: $0 status"
	echo "Uso: $0 stop"
	exit $CODE
}

UserLogged(){
# Comprobar si el usuario esta logado
	USUARIOS=$(who | grep -v USER |awk '{print $1}');
	logged=0
	for user in $USUARIOS
	do 
		if [ "$user" != "root" ]; then
			logged=1
		fi
	done
	return $logged
}


PARAMS=$#

if [ $PARAMS -eq 0 ]; then
	Salir 1
fi

OPT=$1

case $OPT in 
	"passwd")
		if [ $PARAMS -ne 2 ]; then
			Salir 1
		else
			VNC=/usr/bin/x11vnc
			VNCPWDFILE=/etc/.x11vnc
			PASSWD=$2
			$VNC -storepasswd $PASSWD $VNCPWDFILE
		fi
		;;
	"startlopd")
		USUARIO=0
		UserLogged
		if [ "$?" -eq 1 ]; then
			# usuario logado, necesario pedir confirmacion
			/etc/init.d/vnc startlopd
			exit $?
		else
			# no hay usuario logado 
			/etc/init.d/vnc start
			exit $?
		fi
		;;
	"start")
		/etc/init.d/vnc start
		;;
	"status")
		/etc/init.d/vnc status
		;;
	"stop")
		/etc/init.d/vnc stop
		;;
	"restartlopd")
		/etc/init.d/vnc restartlopd
		;;
	*)
		Salir 1
		;;
esac
