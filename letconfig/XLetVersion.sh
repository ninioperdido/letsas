#!/bin/sh

Xdialog	--title "Acerca de..." --icon /usr/share/icons/Tango/32x32/actions/messagebox_info.png --no-cancel --center --msgbox "Distribution HP LeT 1.0v R1 dated 18/03/2007 Powered By Gentoo embedded.\nLinux Kernel based on version 2.6 of GNU/Linux.\nICEWM GPL version 1.2.26 with GTK+2.0.\nROX version 2.5.\nX.Org version 7.1.\nCopyright (C) 2007 HP-Sistemas Sevilla \n \
 \n \
This program is free software; you can redistribute it and/or \n \
modify it under the terms of the GNU General Public License \n \
as published by the Free Software Foundation; either version 2 \n \
of the License, or (at your option) any later version. \n \
 \n \
This program is distributed in the hope that it will be useful, \n \
but WITHOUT ANY WARRANTY; without even the implied warranty of \n \
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the \n \
GNU General Public License for more details. \n \
 \n \
You should have received a copy of the GNU General Public License \n \
along with this program; if not, write to the Free Software \n \
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA." 0 0 0
