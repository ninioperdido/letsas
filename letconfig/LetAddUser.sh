#!/bin/sh
# Copyright (C) 2007 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published
# by the Free Software Foundation; version 2 only.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

#
# GPL - Antonio Jose Aguilar Bravo
# Let v3.0
# Script LetAddUser.sh
# Script para a�adir usuarios  
#

PARAMS=$#

if [ $PARAMS -eq 0 ]; then
	echo "Uso: $0 USUARIO [GRUPO]"
	exit 1
fi

USUARIO=$1
HOMEDIR=/home/$USUARIO

case $PARAMS in
	1)
		adduser -h $HOMEDIR -G users -D $USUARIO
		;;
	2)
		GROUP=$2
		if `cut -d: -f1 /etc/group | grep -q $GROUP` ; then
			adduser -h $HOMEDIR -G $GROUP -D $USUARIO
		else
			echo "Grupo no valido."
			echo "Uso: $0 USUARIO [GRUPO]"
			exit 99
		fi
		;;
	*)
		echo "Uso: $0 USUARIO [GRUPO]"
		exit 1
		;;
esac

OUT=$?

if [ $? -ne 0 ]; then
        echo "Uso: $0 USUARIO [GRUPO]"
        exit 1
fi

exit 0
