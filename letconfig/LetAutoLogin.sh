#!/bin/sh 
# Copyright (C) 2007 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published
# by the Free Software Foundation; version 2 only.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#

# GPL - Antonio Jose Aguilar Bravo
# Let v3.0
# Script LetRes.sh
# Script para activar o desactivar el AutoLogin en wdm
# Ficheros necesarios /etc/X11/wdm/wdm-config.auto /etc/X11/wdm/wdm-config.noauto 

Salir(){
	echo "Uso: $0 (auto|noauto)"
	exit 1
}

if [ -z $1 ] ; then
	Salir 
fi
                
if [ $1 != "auto" ] && [ $1 != "noauto" ] ; then
Salir	echo "Uso: $0 (auto|noauto)"
fi
                    
ACCION=$1
DIRCONF="/etc/X11/wdm"
CONFILE="$DIRCONF/wdm-config"

if [ ! -f "$DIRCONF/wdm-config.$ACCION" ] ; then
	echo "ERROR, no existen los ficheros de configuración:"
	echo "$DIRCONF/wdm-config.auto, $DIRCONF/wdm-config.noauto"
	echo "en $DIRCONF"
	exit 100
fi

if `grep -q '^DisplayManager\*wdmDefaultUser' $CONFILE` ; then
	estaHabilitado=1
else 
	estaHabilitado=0
fi

if [ "$ACCION" = "auto" ]; then

	if [ $estaHabilitado -eq 1 ] ; then
		exit 0
	else
		rm -f $CONFILE
		ln -sf $DIRCONF/wdm-config.$ACCION $DIRCONF/wdm-config
    	fi
else
	if [ $estaHabilitado -eq 1 ] ; then
		rm -f $CONFILE
		ln -sf $DIRCONF/wdm-config.$ACCION $DIRCONF/wdm-config
	else
		exit 0
	fi
fi

