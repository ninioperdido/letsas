#!/bin/sh

# Copyright (C) 2007 Programado por Jorge Herrera Baeza
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published
# by the Free Software Foundation; version 2 only.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#

if [ $# -ne 3 ]; then
	echo "Uso: $0 Sesion Usuario Host"
	exit 1
fi

TEMPLATE=/mnt/sessions/template.nxs
cat $TEMPLATE | sed -e "s/__USER__/$2/g" -e "s/__HOST__/$3/g" > /mnt/sessions/${1}.nxs
exit 0
