#!/bin/sh

#########
# A�adir y quitar iconos del escritorio
# Cambio de fondo de escritorio
# Par�metros: 

# A�adir: 
# add fichero.desktop coorX coorY etiqueta 

# Eliminar
# remove fichero.desktop

# Cambiar fondo de escritorio
# fondo imagen (escalado, estirar,...) 

Salir() {
CODE=$1
	echo "$0 add fichero.desktop coorX coorY etiqueta"
	echo "$0 remove fichero.desktop"
	echo "$0 fondo imagen"
	echo "$0 desktop [nombre]"
	exit $CODE
}

PID=$(pidof ROX-Filer |awk '{print $1}')
if [ -z $PID ]; then
	echo "No se esta ejecutando ROX"
	Salir 100
fi

if [ -z $DISPLAY ]; then
	export DISPLAY=":0.0"
fi

#if [ -z $PID ]; then
#	Salir 1
#fi

ACCION=$1

case $ACCION in
	"add")
		if [ $# -ne 5 ]; then
			Salir 1
		fi
		APP=$2
		X=$3
		Y=$4
		LABEL=$5
rox --RPC <<EOF
<?xml version="1.0"?>
<env:Envelope xmlns:env="http://www.w3.org/2001/12/soap-envelope">
<env:Body xmlns="http://rox.sourceforge.net/SOAP/ROX-Filer">
<PinboardAdd>
<Path>$APP</Path>
<X>$X</X>
<Y>$Y</Y>
<Label>$LABEL</Label>
</PinboardAdd>
</env:Body>
</env:Envelope>
EOF
		/bin/sync
		;;
	"remove")
		if [ $# -ne 2 ]; then
			Salir 1
		fi
		APP=$2
rox --RPC <<EOF
<?xml version="1.0"?>
<env:Envelope xmlns:env="http://www.w3.org/2001/12/soap-envelope">
<env:Body xmlns="http://rox.sourceforge.net/SOAP/ROX-Filer">
<PinboardRemove>
<Path>$APP</Path>
</PinboardRemove>
</env:Body>
</env:Envelope>
EOF
		/bin/sync
		;;
	"fondo")
		if [ $# -ne 2 ]; then
			Salir 1
		fi
		FONDO=$2
rox --RPC <<EOF
<?xml version="1.0"?>
<env:Envelope xmlns:env="http://www.w3.org/2001/12/soap-envelope">
<env:Body xmlns="http://rox.sourceforge.net/SOAP/ROX-Filer">
<SetBackdrop>
<Filename>$FONDO</Filename>
<Style>Centered</Style>
</SetBackdrop>
</env:Body>
</env:Envelope>
EOF
		;;
		
	"desktop")
		DESKTOP=$2
rox --RPC <<EOF
<?xml version="1.0"?>
<env:Envelope xmlns:env="http://www.w3.org/2001/12/soap-envelope">
<env:Body xmlns="http://rox.sourceforge.net/SOAP/ROX-Filer">
<Pinboard>
if [ -z $DESKTOP ]; 
<Name>$DESKTOP</Name>
else
fi
</Pinboard>
</env:Body>
</env:Envelope>
EOF
		;;
	"ejecutar")
		COMMAND=$2
rox --RPC <<EOF
<?xml version="1.0"?>
<env:Envelope xmlns:env="http://www.w3.org/2001/12/soap-envelope">
<env:Body xmlns="http://rox.sourceforge.net/SOAP/ROX-Filer">
<Run>
<Filename>$COMMAND</Filename>
</Run>
</env:Body>
</env:Envelope>
EOF

		;;
		*)
		Salir 1
		;;
esac
