#!/bin/sh
DIALOG=Xdialog

resolucion="800x600" 	#Variable que almacena la resolución elegida 800x600 por defecto
R1024="off" 		#Variable que almacena la opción última seleccionada
R800="on"		#Variable que almacena la opción última seleccionada
RCUSTOM="off"		#Variable que almacena la opción última seleccionada
teclado="es"		#Variable que almacena la configuración de nuestro teclado
es="on"			#Variable que almacena la opción última seleccionada
en="off"		#Variable que almacena la opción última seleccionada
usuario="Usuario"	#Variable que almacena el dato de usuario
contrasena=""		#Variable que almacena el dato de contraseña
dirip="0.0.0.0"		#Variable que almacena la dirección ip o el nombre de host
dominio=""		#Variable que almacena el nombre de dominio
impresora="Ninguna"	#Variable que almacena el tipo de impresión
cola="usblp"		#Variable que almacena el nombre de la cola a direccionar
par="off"		#Variable que almacena la opción última seleccionada
col="off"		#Variable que almacena la opción última seleccionada
no="on"			#Variable que almacena la opción última seleccionada
sonido="Off"		#Variable que almacena si queremos sonido o no
on="off"		#Variable que almacena la opción última seleccionada
off="on"		#Variable que almacena la opción última seleccionada
ruta="/mnt/desktop/" 	#Variable que almacena la ruta de los archivos .desktop
rutautil="/mnt/letconfig/"	#Variable que almacena la ruta de las utilidades de letconfig
icon="/usr/share/icons/"  	#Variable que almacena la ruta de los archivos de iconos
sesion="RDP1"			#Variable que almacena la sesión
archivo="RDP1.desktop"		#Variable que almacena el archivo donde se guarda la sesión
salir="falso"		#Variable que indica cuando salir del configurador
guardado="verdadero"	#Variable que indica cuando hemos hecho un cambio para salvarlo antes de salir
flag="true"		#Variable que indica cuando podemos dejar la ventana de guardar

while [ $salir = "falso" ]; do #Mientras no seleccionemos salir
 flag="true" #Nos va a permitir controlar si la sesión existe para evitar sobreescribirla por error
 $DIALOG --title "Configuración sesiones RDP" --icon "$icon"gnome-remote-desktop.png --menu "Configuración de la conexión" 25 60 10 "Resolución"  "$resolucion" "Teclado" "$teclado"  "Identificación" "$usuario $dirip" "Impresora" "$impresora" "Sonido" "$sonido" "Guardar" "Guarda la sesión configurada" "Eliminar" "Elimina una sesión existente" "Conectar" "Lanza la conexión configurada" "Salir" "Abandona el cliente" 2> /tmp/menu.tmp.$$	#Lanzamos el menú de configuración

 retval=$?

 choice=`cat /tmp/menu.tmp.$$` #Comprobamos qué variable queremos configurar
 rm -f /tmp/menu.tmp.$$

 case $retval in
   0)                          #La opción elegida lanza la pantalla de configuración del parámetro
     case $choice in

      Resolución)	#Si elegimos configurar la resolución	
        aux=$resolucion
	$DIALOG --title "Resolución de pantalla" --icon "$icon"Tango/32x32/apps/config-xfree.png --radiolist "Elija la resolución" 16 45 3 "1024x768" "Alta resolución" $R1024 "800x600" "Resolución media" $R800 "Personalizada" "Resolución personalizada" $RCUSTOM 2>/tmp/resolucion.tmp.$$ #Un Radiolist para elegir la resolución

        retval=$?
 
        case $retval in
         0)
 	  resolucion=`cat /tmp/resolucion.tmp.$$` #Guardamos la configuración de resolución
	  if [ "$resolucion" = "1024x768" ]; then #Guardamos el estado configurado para que aparezca el radio-button adecuado
	   R1024="on"
	   R800="off"
	   RCUSTOM="off"
	  else
	   if [ "$resolucion" = "800x600" ]; then	
		R1024="off"
		R800="on"
		RCUSTOM="off"
	   else
		R1024="off"
		R800="off"
		RCUSTOM="off"

		resolucion=`$DIALOG --stdout --title "Resolucion personalizada" --icon "$icon"Tango/32x32/apps/config-xfree.png --inputbox "Introduzca el valor de resolución deseado (anchoxalto) " 0 0 "$resolucion"` 
	    
	    	retval=$?

	    	case $retval in
	     	 0)
			if [ "$resolucion" = "" -o "$resolucion" = "Personalizada" ]; then	#Si le metemos una resolución vacía, mantiene la que tenía
		 		resolucion=$aux
			fi
			
		 ;;
			
		 1)
			echo "Cancelada la configuración de resolución personalizada"
			resolucion=$aux;;
		 255)
			echo "Cerrada ventana de configuración de resolución personalizada"
			resolucion=$aux;;
		 esac
	   fi
	  fi
	  if [ "$resolucion" != "$aux" ]; then #Comprobamos si ha habido alguna modificación para avisar antes de salir
	   guardado="falso"
	  fi

          rm -f /tmp/resolucion.tmp.$$
          ;;
         
         1)
          rm -f /tmp/resolucion.tmp.$$ #No guardamos nada, pero borramos el archivo temporal de almacenamiento
	  echo "Cancelado configuración de resolución"
	  ;;

         255)
          rm -f /tmp/resolucion.tmp.$$ #No guardamos nada, pero borramos el archivo temporal de almacenamiento
	  echo "Cerrada ventana de configuración de resolución"
	  ;;
        esac;;

      Teclado) 	 #Si elegimos configurar el teclado
	aux=$teclado
        $DIALOG --title "Teclado" --icon "$icon"Tango/32x32/devices/keyboard.png --radiolist "Elija la configuración del teclado" 12 45 2 "es" "Español" $es "en-us" "Inglés americano" $en 2>/tmp/teclado.tmp.$$ #Un Radiolist para elegir el teclado

        retval=$?
 
        case $retval in
         0)
 	  teclado=`cat /tmp/teclado.tmp.$$` #Guardamos la configuración de teclado
	  if [ "$teclado" != "$aux" ]; then
	   guardado="falso"
	  fi
	  if [ "teclado" = "es" ]; then #Guardamos el estado configurado para que aparezca el radio-button adecuado
	   es="on"
	   en="off"
	  else
	   es="off"
	   en="on"
	  fi
          rm -f /tmp/teclado.tmp.$$
          ;;
         
         1)
          rm -f /tmp/teclado.tmp.$$ #No guardamos nada, pero borramos el archivo temporal de almacenamiento
	  echo "Candelado configuración de teclado"
	  ;;

         255)
          rm -f /tmp/teclado.tmp.$$ #No guardamos nada, pero borramos el archivo temporal de almacenamiento
          echo "Cerrada ventana de configuración de teclado"
	  ;;
        esac;;
      Identificación) #Pedimos los datos de host remoto, usuario y contraseña
	aux="$dirip|$usuario|$contrasena"
       $DIALOG	--separator "|" --title "Conexión" --icon "$icon"Tango/32x32/apps/config-users.png --backtitle "Datos del equipo remoto y de usuario" --left --password --3inputsbox "Introduzca sus datos" 25 50 "Dirección IP o nombre del host:" $dirip "Usuario:" "$usuario" "Contraseña:" "$contrasena" 2>/tmp/login.tmp.$$ #Metemos los datos separados por el carácter |

        ret=$?

        case $ret in
	 0)
		usuario=`awk -F "|" '{ print $2 }' /tmp/login.tmp.$$`     #Extraemos el usuario
		contrasena=`awk -F "|" '{ print $3 }' /tmp/login.tmp.$$` #Extraemos la contraseña
		dirip=`awk -F "|" '{ print $1 }' /tmp/login.tmp.$$`    #Extraemos la dirección o nombre del host
		aux2=`echo "$dirip|$usuario|$contrasena"`

		if [ "$aux" != "$aux2" ]; then
		 guardado="falso"
		fi
		rm -f /tmp/login.tmp.$$ 
		;;
	 1)
		rm -f /tmp/login.tmp.$$ #Borramos el archivo temporal
		echo "Cancelado configuración de datos de conexión"
		;;
	 255)
		rm -f /tmp/login.tmp.$$ #Borramos el archivo temporal
		echo "Cerrada la ventana de configuración de datos de conexión"
		;;
        esac

	#Pedimos los datos de dominio
	aux2=$dominio
	dominio=`$DIALOG --stdout --title "Dominio" --icon "$icon"Tango/32x32/apps/config-users.png --inputbox "Introduzca el nombre de dominio" 0 0 "$dominio"` 
	    
	    retval=$?

	    case $retval in
	     0)
		if [ "$dominio" != "$aux2" ]; then #Notificamos que ha habido cambios
		 guardado="falso"
		fi;;
	     1)
		echo "Cancelada la configuración de la cola de impresión"
		dominio=$aux2;;
	     255)
		echo "Cerrada ventana de configuración de cola de impresión"
		dominio=$aux2;;
	    esac
      ;;
	

      Impresora)
	aux=$impresora
	$DIALOG --title "Impresora" --icon "$icon"Tango/32x32/devices/printer.png --radiolist "Elija la configuración de la impresora" 15 65 4 "Paralelo" "Impresora local por puerto paralelo" $par "Cola" "Impresora referenciada por la cola del terminal" $col "Ninguna" "No mapear la impresora" $no 2>/tmp/impresora.tmp.$$ #Un Radiolist para elegir el mapeo de impresoras

        retval=$?
 
        case $retval in
         0)
 	  impresora=`cat /tmp/impresora.tmp.$$` #Guardamos la configuración de impresora
	  if [ "$impresora" != "$aux" ]; then #Comprobamos si ha habido cambios para avisar de ellos
	   guardado="falso"
	  fi
	  if [ "$impresora" = "Paralelo" ]; then #Guardamos el estado configurado para que aparezca el radio-button adecuado
	   par="on"
	   col="off"
	   no="off"
	  else
	   if [ "$impresora" = "Cola" ]; then #Si hemos elegido asociar la impresión a una cola en lugar de a un dispositivo
	    par="off"
	    col="on"
	    no="off"
	    aux2=$cola	#Guardamos el valor original de la cola para comprobar si hay cambios
	    cola=`$DIALOG --stdout --title "Cola de impresión" --icon "$icon"Tango/32x32/devices/printer.png --inputbox "Introduzca el nombre de la cola\nde impresión del terminal" 0 0 "$cola"` 
	    
	    retval=$?

	    case $retval in
	     0)
		if [ "$cola" != "$aux2" ]; then #Notificamos que ha habido cambios
		 guardado="falso"
		fi
		if [ "$cola" = "" ]; then	#Si le metemos una cola vacía, no pone ninguna
		 impresora="Ninguna"
		else
		 impresora="$cola"
		fi;;
	     1)
		echo "Cancelada la configuración de la cola de impresión"
		impresora=$aux;;
	     255)
		echo "Cerrada ventana de configuración de cola de impresión"
		impresora=$aux;;
	    esac
	   else
	    par="off"
	    col="off"
	    no="on"
	   fi
	  fi
          rm -f /tmp/impresora.tmp.$$
          ;;
         
         1)
          rm -f /tmp/impresora.tmp.$$ #No guardamos nada, pero borramos el archivo temporal de almacenamiento
	  echo "Cancelado configuración de impresión"
	  ;;
         255)
          rm -f /tmp/impresora.tmp.$$ #No guardamos nada, pero borramos el archivo temporal de almacenamiento
	  echo "Cerrada ventana de configuración de impresión"
	  ;;
        esac;;

      Sonido) 	 #Si elegimos configurar el sonido
	aux=$sonido
        $DIALOG --title "Sonido" --icon "$icon"Tango/32x32/devices/yast_soundcard.png --radiolist "Elija la si desea activar el sonido" 12 45 2 "On" "Sonido activado" $on "Off" "Sonido desactivado" $off 2>/tmp/sonido.tmp.$$ #Un Radiolist para elegir si queremos sonido

        retval=$?
 
        case $retval in
         0)
 	  sonido=`cat /tmp/sonido.tmp.$$` #Guardamos la configuración de sonido
	  if [ "$sonido" != "$aux" ]; then
	   guardado="falso"
	  fi
	  if [ "$sonido" = "On" ]; then #Guardamos el estado configurado para que aparezca el radio-button adecuado
	   on="on"
	   off="off"
	  else
	   on="off"
	   off="on"
	  fi
          rm -f /tmp/sonido.tmp.$$
          ;;
         
         1)
          rm -f /tmp/sonido.tmp.$$ #No guardamos nada, pero borramos el archivo temporal de almacenamiento
	  echo "Cancelado configuración de sonido"
	  ;;

         255)
          rm -f /tmp/sonido.tmp.$$ #No guardamos nada, pero borramos el archivo temporal de almacenamiento
	  echo "Cerrada ventana de configuración de sonido"
	  ;;
        esac;;

      Guardar)	#A partir de las variables configuradas, montamos la linea de comando
       resolution="-g $resolucion" 
       keyboard="-k $teclado"
       if [ "$usuario" != "" ]; then
	user="-u $usuario"
       else
	user=""
       fi
       if [ "$contrasena" != "" ]; then
	password="-p $contrasena"
       else
	password=""
       fi
       if [ "$impresora" = "Paralelo" ]; then
	printer="-r lptport:LPT1=/dev/lp0"
       else
	if [ "$impresora" = "Ninguna" ]; then
	 printer=""
	else
	 if [ "$cola" != "" ]; then
	  printer="-r printer:$cola"
	 else
	  printer=""
	  impresora="Ninguna"
	 fi
        fi
       fi
       if [ "$sonido" = "On" ]; then
	sound="-r sound:local"
       else
	sound=""
       fi
       if [ "$dominio" = "" ]; then
	  domain=""
       else
	  domain="-d $dominio"
       fi
       ipadd=$dirip

       while [ "$flag" = "true" ]; do #Mientras no guardemos o cancelemos, seguimos en el bucle
	$DIALOG --title "Guardar Sesión" --icon "$icon"Tango/32x32/actions/document-save.png --left --ok-label "Guardar" --cancel-label "Cancelar" --inputbox "Introduzca el nombre de la sesión a guardar" 0 0 "$sesion" 2> /tmp/save.tmp.$$
	
	retval=$?
	sesion=`cat /tmp/save.tmp.$$`
	rm -f /tmp/save.tmp.$$
	archivo="$sesion.desktop"
	aux=`ls "$ruta$archivo"`
	aux2=`cat /root/.config/rox.sourceforge.net/ROX-Filer/pb_Desktop | grep "$sesion"`
	
	if [ "$ruta$archivo" != "$aux" -o "$aux2" = "" ]; then #comprobamos si el archivo ya existe

         case $retval in #Si no existe, lo creamos
 	  0)


		"$rutautil"roxSOAP.sh remove "$ruta$archivo" #Eliminamos el icono para no duplicarlo
		echo "[Desktop Entry]" >"$ruta$archivo"
		echo "Exec=rdesktop $resolution -a 16 -z $keyboard $printer $sound $user $password $ipadd $domain" >>"$ruta$archivo"	
		echo "Icon="$icon"gnome-remote-desktop.png" >>"$ruta$archivo"	#creamos el archivo .desktop
		echo "Name=$sesion" >>"$ruta$archivo"
		x=`/mnt/letconfig/LetPosIcon.sh | awk '{print $1}'`	
		y=`/mnt/letconfig/LetPosIcon.sh | awk '{print $2}'`	
		"$rutautil"roxSOAP.sh add "$ruta$archivo" $x $y "$sesion"	#Añadimos el icono en el escritorio

		$DIALOG --title "Información" --icon "$icon"Tango/32x32/status/dialog-information.png --no-cancel --msgbox "La sesión ha sido creada con éxito" 0 0

		guardado="verdadero" 	#Indicamos que se han guardado los cambios
		flag="false";;	 	#Salimos del bucle de guardado
	  1)
		echo "Cancelado guardar sesión"	#Si le damos a cancelar en la ventana de guarda, salimos sin guardar
		flag="false";;
	  255)
		echo "Cerrada ventana de guardar sesión" 
		flag="false";;
         esac

	else	#Si el archivo existe, pedimos confirmación para guardarlo
	 $DIALOG --title "Aviso" --icon "$icon"Tango/32x32/status/dialog-warning.png --ok-label "Sí" --cancel-label "No" --yesno "El archivo ya existe ¿desea sobreescribirlo?" 0 0
	 
	 retval=$?

	 case $retval in 	#Si aceptamos sobreescribir, guardamos
	  0)

		"$rutautil"roxSOAP.sh remove "$ruta$archivo" #Eliminamos el icono para no duplicarlo
		echo "[Desktop Entry]" >"$ruta$archivo"
		echo "Exec=rdesktop $resolution -a 16 -z $keyboard $printer $sound $user $password $ipadd" >>"$ruta$archivo"	
		echo "Icon="$icon"gnome-remote-desktop.png" >>"$ruta$archivo"	#creamos el archivo .desktop
		echo "Name=$sesion" >>"$ruta$archivo"
		x=`/mnt/letconfig/LetPosIcon.sh | awk '{print $1}'`	
		y=`/mnt/letconfig/LetPosIcon.sh | awk '{print $2}'`				
		"$rutautil"roxSOAP.sh add "$ruta$archivo" $x $y "$sesion"	#Añadimos el icono en el escritorio

		$DIALOG --title "Información" --icon "$icon"Tango/32x32/status/dialog-information.png --no-cancel --msgbox "La sesión ha sido creada con éxito" 0 0

		guardado="verdadero"
		flag="false";;
	  1)
		echo "volvemos a la ventana de guardar";;	#No queremos sobreescribir, volvemos a la ventana de guardar
	  255)
		echo "Cerrada ventana de confirmación, volvemos a la ventana de guardar";;
	 esac		#Si cerramos la confirmación, volvemos a la ventana de guardar
	fi
       done 
      ;;

      Eliminar)
	while [ "$flag" = "true" ]; do #Mientras no borremos o cancelemos, seguimos en el bucle

		$DIALOG --title "Configuración sesiones RDP" --icon "$icon"Tango/32x32/actions/eraser.png --backtitle "¡¡¡Atención!!!, va a proceder a la eliminación de una sesión RDP" --left  --ok-label "Eliminar" --cancel-label "Cancelar" --inputbox "Introduzca el nombre de la sesión" 0 0 "$sesion" 2> /tmp/login.tmp.$$

		retval=$?

	
		case $retval in
	  	0)
			sesion=`cat /tmp/login.tmp.$$` #Extraemos el nombre de la sesión 
			archivo="$sesion.desktop"
	
			rm -f /tmp/login.tmp.$$

			$DIALOG --title "Aviso" --icon "$icon"Tango/32x32/status/dialog-warning.png --ok-label "Sí" --cancel-label "No" --yesno "Ha seleccionado eliminar la sesión '$sesion', ¿desea continuar?" 0 0

			retval=$?

	 		case $retval in 	#Si aceptamos, eliminamor
	  		0)
				aux2=`cat "$ruta$archivo" | grep "gnome-remote-desktop"`
				aux3=`cat .config/rox.sourceforge.net/ROX-Filer/pb_Desktop | grep "$sesion"`
				if [ -e "$ruta$archivo" ]; then #Si existe el archivo
					if [ "$aux2" != "" ]; then #Si es del tipo adecuado			 
						"$rutautil"roxSOAP.sh remove "$ruta$archivo" #Eliminamos el icono 
						rm -f "$ruta$archivo" 2> /dev/null
						$DIALOG --title "Información" --icon "$icon"Tango/32x32/status/dialog-information.png --no-cancel --msgbox "La sesión ha sido eliminada con éxito" 0 0
					else	#Si no es del tipo adecuado damos aviso
						$DIALOG --title "Aviso" --icon "$icon"Tango/32x32/status/dialog-warning.png --no-cancel --msgbox "Está intentando eliminar un acceso que no es una sesión de RDP, permiso denegado" 0 0
					fi
				else
					if [ "$aux3" != ""]; then

						"$rutautil"roxSOAP.sh remove "$ruta$archivo" #Eliminamos el icono 
						rm -f "$ruta$archivo" 2> /dev/null
						$DIALOG --title "Información" --icon "$icon"Tango/32x32/status/dialog-information.png --no-cancel --msgbox "La sesión ha sido eliminada con éxito" 0 0
							
					else	#Si no existe el tampoco el icono, avisamos
						$DIALOG --title "Aviso" --icon "$icon"Tango/32x32/status/dialog-warning.png --no-cancel --msgbox "Está intentando eliminar un acceso que no existe." 0 0
					fi
				fi

				flag="false";; #Salimos del bucle
						
		  	1)
				echo "Volvemos a la ventana de eliminar";;#No queremos eliminar, volvemos atrás						
			255)
				echo "Cerrada ventana de confirmación, volvemos a la ventana de guardar";;
	 		esac	#Si cerramos la confirmación, volvemos a la ventana de guardar
			;;
	 
		1)
	    		rm -f /tmp/login.tmp.$$
	    		echo "Cancelado borrado de sesión RDP"
			flag=false;;

				
		255)
			rm -f /tmp/login.tmp.$$
			echo "Se ha cerrado la ventana de configuración"
			flag="false";;
	
		esac
	done;;


      Conectar)		#A partir de las variables configuradas, montamos la linea de comando
         resolution="-g $resolucion" 
	 keyboard="-k $teclado"
	 if [ "$usuario" != "" ]; then
	  user="-u $usuario"
	 else
	  user=""
	 fi
	 if [ "$contrasena" != "" ]; then
	  password="-p $contrasena"
	 else
	  password=""
	 fi
	 if [ "$impresora" = "Paralelo" ]; then
	  printer="-r lptport:LPT1=/dev/lp0"
	 else
	  if [ "$impresora" = "Ninguna" ]; then
	   printer=""
	  else
	   if [ "$cola" != "" ]; then
	    printer="-r printer:$cola"
	   else
	    printer=""
	    impresora="Ninguna"
	   fi
          fi
         fi
	 if [ "$sonido" = "On" ]; then
	  sound="-r sound:local"
	 else
	  sound=""
	 fi
	 if [ "$dominio" = "" ]; then
	  domain=""
	 else
	  domain="-d $dominio"
	 fi
	 ipadd=$dirip
			#llamamos al comando para ver si la conexión se puede realizar
	 rdesktop $resolution -a 24 -z $keyboard $printer $sound $user $password $ipadd $domain 2>/tmp/status_rdp.tmp.$$
	 status=`cat /tmp/status_rdp.tmp.$$` 
	 rm -f /tmp/status_rdp.tmp.$$
	 if [ "$status" != ""  ]; then 	#Si se ha producido algún error
	  $DIALOG --title "Aviso" --left --icon "$icon"Tango/32x32/status/dialog-warning.png --msgbox "$status" 0 0 0		#Vemos los mensajes de error por pantalla
	 fi
         ;;

      Salir)	#Si seleccionamos salir, comprobamos si se ha realizado algún cambio
	if [ "$guardado" = "verdadero" ]; then
	 salir="verdadero"
	else	#Si ha habido cambios, se da el aviso de que se perderán 
	 $DIALOG --title "Aviso" --center --icon "$icon"Tango/32x32/status/dialog-warning.png --ok-label "Sí" --cancel-label "No" --yesno "Ha realizado modificaciones y no ha salvado la sesión.\n¿Desea guardar los cambios?" 0 0 0
	 case $? in
	 0)
	  resolution="-g $resolucion" 
          keyboard="-k $teclado"
          if [ "$usuario" != "" ]; then
	   user="-u $usuario"
          else
	   user=""
          fi
          if [ "$contrasena" != "" ]; then
	   password="-p $contrasena"
          else
	   password=""
          fi
          if [ "$impresora" = "Paralelo" ]; then
	   printer="-r lptport:LPT1=/dev/lp0"
          else
	   if [ "$impresora" = "Ninguna" ]; then
	    printer=""
	   else
	    if [ "$cola" != "" ]; then
	     printer="-r printer:$cola"
	    else
	     printer=""
	     impresora="Ninguna"
	    fi
           fi
          fi
          if [ "$sonido" = "On" ]; then
	   sound="-r sound:local"
          else
	   sound=""
          fi
           ipadd=$dirip

          while [ "$flag" = "true" ]; do #Mientras no guardemos o cancelemos, seguimos en el bucle

           $DIALOG --title "Guardar Sesión" --icon "$icon"Tango/32x32/actions/document-save.png --ok-label "Guardar" --cancel-label "Cancelar" --left --inputbox "Introduzca el nombre de la sesión a guardar" 8 50 "$sesion" 2> /tmp/save.tmp.$$
	
	   retval=$?
	   sesion=`cat /tmp/save.tmp.$$`
	   rm -f /tmp/save.tmp.$$
	   archivo="$sesion.desktop"
	   aux=`ls "$ruta$archivo"`
	   aux2=`cat /root/.config/rox.sourceforge.net/ROX-Filer/pb_Desktop | grep "$sesion"`
	
	   if [ "$ruta$archivo" != "$aux" -o "$aux2" = "" ]; then #comprobamos si el archivo ya existe

            case $retval in #Si no existe, lo creamos
 	    0)
		"$rutautil"roxSOAP.sh remove "$ruta$archivo" #Eliminamos el icono para no duplicarlo
		echo "[Desktop Entry]" >"$ruta$archivo"
		echo "Exec=rdesktop $resolution -a 16 -z $keyboard $printer $sound $user $password $ipadd" >>"$ruta$archivo"	
		echo "Icon="$icon"gnome-remote-desktop.png" >>"$ruta$archivo"	#creamos el archivo .desktop
		echo "Name=$sesion" >>"$ruta$archivo"
		x=`/mnt/letconfig/LetPosIcon.sh | awk '{print $1}'`	
		y=`/mnt/letconfig/LetPosIcon.sh | awk '{print $2}'`				
		"$rutautil"roxSOAP.sh add "$ruta$archivo" $x $y "$sesion"	#Añadimos el icono en el escritorio

		$DIALOG --title "Información" --icon "$icon"Tango/32x32/status/dialog-information.png --no-cancel --msgbox "La sesión ha sido creada con éxito" 0 0

		guardado="verdadero" 	#Indicamos que se han guardado los cambios
		flag="false"	 	#Salimos del bucle de guardado
		salir="verdadero";;	#Indicamos que ya podemos salir de la aplicación
	    1)
		echo "Cancelado guardar sesión"	#Si le damos a cancelar en la ventana de guarda, salimos sin guardar
		flag="false";;
	    255)
		echo "Cerrada ventana de guardar sesión" 
		flag="false";;
            esac

	   else	#Si el archivo existe, pedimos confirmación para guardarlo
	    $DIALOG --title "Aviso" --icon "$icon"Tango/32x32/status/dialog-warning.png --ok-label "Sí" --cancel-label "No" --yesno "El archivo ya existe ¿desea sobreescribirlo?" 0 0
	 
	    retval=$?

	    case $retval in 	#Si aceptamos sobreescribir, guardamos
	    0)
		"$rutautil"roxSOAP.sh remove "$ruta$archivo" #Eliminamos el icono para no duplicarlo
		echo "[Desktop Entry]" >"$ruta$archivo"
		echo "Exec=rdesktop $resolution -a 16 -z $keyboard $printer $sound $user $password $ipadd" >>"$ruta$archivo"	
		echo "Icon="$icon"gnome-remote-desktop.png" >>"$ruta$archivo"	#creamos el archivo .desktop
		echo "Name=$sesion" >>"$ruta$archivo"
		x=`/mnt/letconfig/LetPosIcon.sh | awk '{print $1}'`	
		y=`/mnt/letconfig/LetPosIcon.sh | awk '{print $2}'`	
		"$rutautil"roxSOAP.sh add "$ruta$archivo" $x $y "$sesion"	#Añadimos el icono en el escritorio

		$DIALOG --title "Información" --icon "$icon"Tango/32x32/status/dialog-information.png --no-cancel --msgbox "La sesión ha sido creada con éxito" 0 0

		guardado="verdadero"
		flag="false"
		salir="Verdadero";;
	    1)
		echo "volvemos a la ventana de guardar";;	#No queremos sobreescribir, volvemos a la ventana de guardar
	    255)
		echo "Cerrada ventana de confirmación, volvemos a la ventana de guardar";;
	    esac		#Si cerramos la confirmación, volvemos a la ventana de guardar
	   fi
          done;;
	 1)
	  echo "Ha seleccionado no guardar los cambios"
	  salir="verdadero";;
	 255)
	  echo "Ha cerrado la ventana de confirmación";;
	 esac
	fi
         ;;
     esac
     ;;     
    1)
     echo "Cancelado configuración de sesión RDP"
     salir="verdadero"
     ;;
   255)
     echo "Cerrada ventana de configuración de sesión RDP"
     salir="verdadero"
     ;;
 esac
done
