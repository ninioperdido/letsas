#!/bin/sh

############################################
# Muestra el diálogo de apagado de sistema #
############################################

Xdialog --title "Apagado del sistema" --icon /usr/share/icons/Tango/32x32/actions/system-shutdown.png --default-no --ok-label "Sí" --cancel-label "No" --yesno "¿Desea Apagar el equipo?" 6 50 3000
case $? in
0)
  /sbin/poweroff;;
1)
  echo "Apagado cancelado";;
255)
  echo "Ventana de apagado cerrada";;
esac
