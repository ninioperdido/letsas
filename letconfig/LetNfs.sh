#!/bin/sh
# Copyright (C) 2007 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published
# by the Free Software Foundation; version 2 only.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#

# GPL - Antonio Jose Aguilar Bravo
# Let v1.0
# Script LetNfs.sh
# Script que a�ade linea la /etc/fstab para el uso de unidades remotas mediante nfs
# Parametro: ON | OFF | HOSTNAMECHANGE

Salir(){
	CODE=$1
	echo "Uso: $0 ON|OFF|HOSTNAMECHANGE"
	exit $CODE
}

EscribeFSTAB(){
cat <<EOF >> $FSTAB
$SMB_IP:$SMB_PATH      /home/usuario/REMOTO       nfs    rw,users,noauto,hard,intr,sync,exec 0 0
EOF
}


eliminarFSTAB(){
	
	# eleminar una linea concreta con grep -n --> sed -i "$LINEA"d $FILE
	
	sed -i -e '/\/home\/usuario\/REMOTO/d' $1

}

checkFSTABdespliegue(){
	EXISTE=$(grep -c '^SMB_PATH=.*SMB_HOST.*' $SMBINFO |cut -d: -f1)
	[ "$EXISTE" -eq 0 ] && exit 0
	EliminarFSTAB $FSTAB
	EscribeFSTAB
}

PARAMS=$#

if [ $PARAMS -ne 1 ]; then 
	Salir 1
fi

# Leer configuracion remota si existe
SMBINFO=/etc/sysconfig/smb.info

SMB_HOST=$(/bin/hostname)

OPC=$1

NFSDIR=/home/usuario/REMOTO
FSTAB=/etc/fstab

case $OPC in
	"ON")
		if [ -f $SMBINFO ]; then
		         . $SMBINFO
		else
			exit 0
		fi
		
		[ "$SMB_TYPE" == "SMB" ] && exit 1
		
		# comprobacion realizada en script de inicio [ -d $NFSDIR ] ||  mkdir -p $NFSDIR
		[ -f $FSTAB ] || touch $FSTAB
		VALUE=`grep nfs $FSTAB`
		if [ -z "$VALUE" ]; then
		
			EscribeFSTAB
		
		else
			exit 0
		fi
		;;
	"OFF")
		[ -e $FSTAB ] || exit 0
		eliminarFSTAB $FSTAB
		;;

	"HOSTNAMECHANGE")
		[ -f $SMBINFO ] || exit 0
		. $SMBINFO
		if [ "$SMB_TYPE" == "NFS" ]; then
			checkFSTABdespliegue
		else
			exit 0
		fi
		;;
	
	*)
		Salir 1
		;;
esac

