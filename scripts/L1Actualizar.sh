#!/bin/bash

STATUS="0"

usage() {
	echo "Uso: $0 [-f] [<directorio imagen>]"
	echo ""
	echo "Directorio Imagen: Ruta de destino donde actualizar la imagen."
	echo ""
	echo "Ejemplo: $0 /repositorio/LeT-1.0-XX"
}

montaje() {
	echo -n "Montando unidades... "
	if [ "$(mount | grep $1/BOOT.IMG)" = "" ]; then
		mount -o loop -t ext2 "$1/BOOT.IMG" "$SYSROOT/boot" >> "$1/.l1log" 2>&1
		STATUS="$?"
		if [ $STATUS -ne 0 ]; then
			echo "X"
			echo "ERROR: Se ha intentado montar la unidad $1/BOOT.IMG en $SYSROOT/boot"
			echo "  y ha fallado, monta la unidad manualmente."
			exit 1
		fi
	fi
	if [ "$(mount | grep $1/MNT.IMG)" = "" ]; then
		mount -o loop -t ext2 "$1/MNT.IMG" "$SYSROOT/mnt" >> "$1/.l1log" 2>&1
		STATUS="$?"
		if [ $STATUS -ne 0 ]; then
			echo "X"
			echo "ERROR: Se ha intentado montar la unidad $1/MNT.IMG en $SYSROOT/mnt"
			echo "  y ha fallado, monta la unidad manualmente."
			exit 1
		fi
	fi
	if [ "$(mount | grep $1/ETC.IMG)" = "" ]; then
		mount -o loop -t ext2 "$1/ETC.IMG" "$SYSROOT/etc" >> "$1/.l1log" 2>&1
		STATUS="$?"
		if [ $STATUS -ne 0 ]; then
			echo "X"
			echo "ERROR: Se ha intentado montar la unidad $1/ETC.IMG en $SYSROOT/etc"
			echo "  y ha fallado, monta la unidad manualmente."
			exit 1
		fi
	fi
	echo "ok"
}

if [ $# -gt 2 ]; then
	usage
	exit 1
fi

if [ "$1" = "-f" ]; then
	FORCE="true"
	IMGDIR="$2"
else
	FORCE="false"
	IMGDIR="$1"
fi

CONFIRM="N"
# Si no hay argumento intentalo con el directorio actual
if [ "$IMGDIR" = "" ]; then
	DIR=`pwd`
else
	DIR=`echo $IMGDIR | awk '{l=length($0); if(substr($0,l,1) == "/"){cadena=substr($0,1,l-1)}else{cadena=$0};print cadena}'`
fi

# Confirma el directorio a imaginizar si no fuerza la ejecucion
if [ "$FORCE" = "false" ]; then
	echo "AVISO: Los cambios locales no seran eliminados."
	read -p "Actualizar la imagen de $DIR ? (s/N) " -t 20 CONFIRM
	if [ "$CONFIRM" != "s" -a "$CONFIRM" != "S" ]; then
		echo "No se hace nada, otra vez sera."
		exit 1
	fi
fi

# Existe realmente el directorio de trabajo?
if [ ! -d "$DIR" ]; then
	echo "ERROR: No existe el directorio de trabajo.
	Ejecute /projects/scripts/L1Inicializar.sh para crearlo."
	exit 1
fi

# Marca en el fichero de logs
echo L1UPDATE: `date` >> "$DIR/.l1log"

echo -n "Recogiendo datos de inicializacion... "
if [ ! -f "$DIR/.l1init" ]; then
	echo "X"
	echo "ERROR: Parece que el directorio $DIR no es un directorio"
	echo "  de imagen valido. Debe crearlo con L1Inicializar.sh"
	exit 1
fi
SIZE=`sed -n "1p" "$DIR/.l1init" | awk '{print $1}'`
DORIG=`sed -n "1p" "$DIR/.l1init" | awk '{print $2}'`
TT=`sed -n "2p" "$DIR/.l1init" | awk '{print $1}'`
CC=`sed -n "2p" "$DIR/.l1init" | awk '{print $2}'`
P01=`sed -n "2p" "$DIR/.l1init" | awk '{print $3}'`
P12=`sed -n "2p" "$DIR/.l1init" | awk '{print $4}'`
P23=`sed -n "2p" "$DIR/.l1init" | awk '{print $5}'`
SYSROOT=`sed -n "3p" "$DIR/.l1init" | awk '{print $1}'`
echo "Longitud: $SIZE" >> "$DIR/.l1log"
echo "SOBOrigen: $DORIG" >> "$DIR/.l1log"
echo "Cilindros: $CC" >> "$DIR/.l1log"
echo "TT: $TT" >> "$DIR/.l1log"
echo "P01: $P01" >> "$DIR/.l1log"
echo "P12: $P12" >> "$DIR/.l1log"
echo "P23: $P23" >> "$DIR/.l1log"
echo "SYSROOT: $SYSROOT" >> "$DIR/.l1log"
echo "ok"

# Montar unidades por si estuvieran desmontadas
montaje $DIR

# Sincronizar la imagen desde el sistema base
echo -n "Sincronizando el sistema con $DORIG... "
rsync --exclude="partimage/" -vau "$DORIG/" "$DIR/" >> "$DIR/.l1log" 2>&1
#git-pull "$DORIG/" . >> "$DIR/.l1log" 2>&1
STATUS="$?"
sync
if [ $STATUS -ne 0 ]; then
	echo "X"
	echo "ERROR: Ha fallado la sincronizacion, compruebe "
	echo "	el problema y vuelva a intentarlo."
	exit 1
fi

echo "ok"

echo "FIN: Se ha actualizado correctamente la imagen."
exit 0
