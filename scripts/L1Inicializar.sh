#!/bin/bash

STATUS="0"

usage() {
	echo "Uso: $0 [-f] <opcion> <longitud> <ruta origen> [<ruta destino>]"
	echo ""
	echo "Opcion: <ro|rw>"
	echo "	ro: Solo lectura, para construir imagenes en produccion."
	echo "	rw: Lectura-Escritura, para construir imagenes de desarrollo."
	echo "Longitud: Longitud en MB de la imagen flash. Puede ser 128,"
	echo "	256, 512 o 1024."
	echo "Ruta origen: Ruta del repositorio git desde donde se copia el"
	echo "	contenido de la imagen."
	echo "Ruta destino: Ruta donde se genera la imagen."
	echo ""
	echo "Ejemplo: $0 ro 128 /projects/dSOB1 /repositorio/LeT-1.0-XX"
}

if [ $# -gt 5 ]; then
	usage
	exit 1
fi

if [ "$1" = "-f" ]; then
	FORCE="true"
	OPTION="$2"
	LONG="$3"
	RORIG="$4"
	RDEST="$5"
else
	FORCE="false"
	OPTION="$1"
	LONG="$2"
	RORIG="$3"
	RDEST="$4"
fi

case "$OPTION" in
	ro)
		OPTION="ro"
		ROOTIMG="ROOT.CRAMFS"
		;;
	rw)
		OPTION="rw"
		ROOTIMG="ROOT.IMG"
		;;
	*)
		echo "ERROR: El parametro opcion no es correcto."
                echo ""
                usage
                exit 1
		;;
esac

# Creacion de los scripts de montaje y desmontaje
# $1 es la ruta de destino ($DDEST en el global)
# $2 es la variable $SYSROOT global
BasicScripts() {
	touch "$1/montaje.sh"
	STATUS="$?"
	if [ $STATUS -ne 0 ]; then
		return 1
	fi		

	echo "#!/bin/bash" > "$1/montaje.sh"
	if [ "$OPTION" == "rw" ]; then
		echo "mount -o loop -t ext2 $1/$ROOTIMG $SYSROOT/" >> "$1/montaje.sh"
	fi
cat << EOF >> "$1/montaje.sh"
mount -o loop -t ext2 $1/BOOT.IMG $SYSROOT/boot
mount -o loop -t ext2 $1/MNT.IMG $SYSROOT/mnt
mount -o loop -t ext2 $1/ETC.IMG $SYSROOT/etc
EOF
	chmod 755 "$1/montaje.sh"

	touch "$1/desmontaje.sh"
	STATUS="$?"
	if [ $STATUS -ne 0 ]; then
		return 1
	fi		

	echo "#!/bin/bash" > "$1/desmontaje.sh"
cat << EOF >> "$1/desmontaje.sh"
umount $2/etc
umount $2/mnt
umount $2/boot
EOF
	if [ "$OPTION" == "rw" ]; then
		echo "umount $2" >> "$1/desmontaje.sh"
	fi
	chmod 755 "$1/desmontaje.sh"
	return 0
}

case "$LONG" in
	128)
		SIZE="128"
		TT="250112"
		P01="63"
		P12="8192"	# 4MB /boot
		P23="141312"	# 68MB /
#		P12="6144"
##		P23="133376"	# 65MB
#		P23="139264"	# 68MB
		CC="977"
		;;
	256)
		SIZE="256"
#		TT="501760"
#		P01="63"
#		P12="6552"
#		P23="132552"
		TT="493920"
		P01="63"
		P12="8568"	# 4MB /boot
		P23="174888"	# 85MB /
#		P12="6552"	# 3MB /boot
##		P23="134064"	# 65MB /
##		P23="163296"	# 80MB /
#		P23="172872"	# 85MB /
		CC="980"
		;;
	512)
		SIZE="512"
		TT="1000944"
		P01="63"
		P12="7056"
#		P23="135072"	# 65MB
		P23="174384"	# 85MB
		CC="993"
		;;
	1024_test)
		SIZE="1024"
		TT="1967616"
		P01="63"
		P12="40320"
		P23="1018080"	# 500MB
		CC="976"
		;;
	1024)
		SIZE="1024"
		TT="1967616"
		P01="63"
		P12="40320"
		P23="237888"	# 100MB
		CC="976"
		;;
#	2048)
#		SIZE="2048"
#		TT=""
#		P01=""
#		P12=""
#		P23=""
#		CC=""
#		;;
	*)
		echo "ERROR: La longitud de la imagen debe ser 128, 256 o 512 MB."
		echo ""
		usage
		exit 1
		;;
esac

CONFIRM="N"
DORIG=`echo ${RORIG} | awk '{l=length($0); if(substr($0,l,1) == "/"){cadena=substr($0,1,l-1)}else{cadena=$0};print cadena}'`
# Si no hay argumento intentalo con el directorio actual
if [ "${RDEST}" = "" ]; then
	DDEST=`pwd`
else
	DDEST=`echo ${RDEST} | awk '{l=length($0); if(substr($0,l,1) == "/"){cadena=substr($0,1,l-1)}else{cadena=$0};print cadena}'`
fi

# Confirma el directorio a inicializar si no fuerza la ejecucion
if [ "$FORCE" = "false" ]; then
	read -p "Inicializar el directorio $DDEST con base $DORIG? (s/N) " -t 20 CONFIRM
	if [ "$CONFIRM" != "s" -a "$CONFIRM" != "S" ]; then
		echo "No se hace nada, otra vez sera."
		exit 1
	fi
fi

if [ ! -d "$DORIG" ]; then
	echo "ERROR: No existe el repositorio git de origen."
	exit 1
fi

if [ ! -d "$DDEST" ]; then
	mkdir "$DDEST"
fi

if [ -f "$DDEST/.l1init" ]; then
	echo "WARNING: Ya existe el directorio de imagen de destino."
	CONFIRM="N"
	### Actualizar ###
	read -p "Desea actualizarlo? (s/N) " -t 20 CONFIRM
	if [ "$CONFIRM" = "s" -o "$CONFIRM" = "S" ]; then
		cd "$DDEST"
		/usr/sbin/L1Actualizar.sh
		exit 0
	fi
	### Eliminar ###
	CONFIRM="N"
	read -p "Desea sobreescribirlo? (s/N) " -t 20 CONFIRM
	if [ "$CONFIRM" = "s" -o "$CONFIRM" = "S" ]; then
		/usr/sbin/L1Eliminar.sh "$DDEST"
#		echo -n "Desmontando unidades... "
#		if [ "$(mount | grep $DDEST/ETC.IMG)" != "" ]; then
#			umount "$DDEST/ETC.IMG"
#			STATUS="$?"
#			if [ $STATUS -ne 0 ]; then
#				echo "X"
#				echo "ERROR: Se ha intentado desmontar la unidad $DDEST/ETC.IMG "
#				echo "  y ha fallado, desmonta la unidad manualmente."
#				exit 1
#			fi
#		fi
#		if [ "$(mount | grep $DDEST/BOOT.IMG)" != "" ]; then
#			umount "$DDEST/BOOT.IMG"
#			STATUS="$?"
#			if [ $STATUS -ne 0 ]; then
#				echo "X"
#				echo "ERROR: Se ha intentado desmontar la unidad $DDEST/BOOT.IMG "
#				echo "  y ha fallado, desmonta la unidad manualmente."
#				exit 1
#		fi
#	fi
#	if [ "$(mount | grep $DDEST/MNT.IMG)" != "" ]; then
#		umount "$DDEST/MNT.IMG"
#		STATUS="$?"
#		if [ $STATUS -ne 0 ]; then
#			echo "X"
#			echo "ERROR: Se ha intentado desmontar la unidad $DDEST/MNT.IMG "
#			echo "  y ha fallado, desmonta la unidad manualmente."
#			exit 1
#		fi
#		fi
#		echo "ok"
#		echo -n "Eliminando el contenido del directorio $DDEST... "
#		rm -rf $DDEST/*
#		STATUS="$?"
#		if [ $STATUS -ne 0 ]; then
#			echo "X"
#			echo "ERROR: No se ha podido eliminar el directorio de trabajo"
#			echo "	$DDEST, verifica los permisos o unidades montadas y vuelve"
#			echo "	a intentarlo."
#			exit 1
#		else
#			echo "ok"
#		fi
#		sync
	else
		echo "No se hace nada, otra vez sera."
		exit 1
	fi
fi

echo "Se esta inicializando la imagen en $DDEST"
echo "Longitud: ${SIZE}"
#echo "TT: $TT"
#echo "P01: $P01"
#echo "P12: $P12"
#echo "P23: $P23"
#echo "Cilindros: $CC"

# Se crea el directorio de destino
echo -n "Creando carpeta de trabajo $DDEST y archivos base... "
if [ ! -d "$DDEST" ]; then
	mkdir "$DDEST"
fi
STATUS="$?"
if [ $STATUS -ne 0 ]; then
	echo "X"
	echo "ERROR: No tiene permisos para crear el directorio $DDEST."
	exit 1
else
	# Sistema de ficheros base para el montaje
	# Directorios donde se monta la imagen
	SYSROOT="$DDEST/ROOT"
	mkdir $SYSROOT
# Directorios para emerge
#	mkdir $DDEST/var
#	mkdir $DDEST/var/lib
#	mkdir $DDEST/var/db
#	mkdir $DDEST/include
# Fichero profile de la imagen sobre el directorio destino
cat << EOF > "$DDEST/.l1init"
${SIZE} ${DORIG} ${DDEST}
${TT} ${CC} ${P01} ${P12} ${P23}
${SYSROOT}
${OPTION}
EOF

#	touch "$DDEST/.l1log"
	echo L1INIT: `date` > "$DDEST/.l1log"

	BasicScripts $DDEST $SYSROOT
	STATUS="$?"
	if [ $STATUS -eq 1 ]; then
		echo "ERROR: No se puede escribir en $DDEST, resuelva el problema
		y vuelva a intentarlo."
		exit 1
	fi
	echo "ok"
fi

# Creacion del MBR segun la longitud de la imagen
# dd if=/repository/LeTSAS-v3.1-T5515/FLASH.IMG of=MBR128.IMG bs=512 count=4096
if [ -f "$DORIG/partimage/MBR${LONG}.IMG" ]; then
	echo -n "Creando $DDEST/MBR.IMG... "
	dd if="$DORIG/partimage/MBR${LONG}.IMG" of="$DDEST/MBR.IMG" bs=512 count=$P01 2>> "$DDEST/.l1log"
	STATUS="$?"
	sync
else
	echo "ERROR: No existe la imagen inicial $DORIG/partimage/MBR${LONG}.IMG, 
	resuelva el problema y vuelva a intentarlo."
	exit 1
fi

if [ $STATUS -ne 0 ]; then
	echo "X"
	echo "ERROR: Ha ocurrido un fallo con la copia del fichero 
	$DORIG/partimage/MBR${LONG}.IMG, resuelva el problema y vuelva
	a intentarlo."
	exit 1
else
	echo "ok"
fi

if [ "$OPTION" == "rw" ]; then
	# Creacion imagen root.img
	echo -n "Creando y formateando $DDEST/ROOT.IMG... "
	dd if=/dev/zero of="$DDEST/$ROOTIMG" bs=512 count=$(($P23-$P12)) 2>> "$DDEST/.l1log"
	STATUS="$?"
	sync
	if [ $STATUS -ne 0 ]; then
		echo "X"
		echo "ERROR: Ha fallado $DDEST/$ROOTIMG, verifique el espacio libre"
		echo "	del sistema."
		exit 1
	else
		mkfs.ext2 -F "$DDEST/$ROOTIMG" >> "$DDEST/.l1log" 2>&1
		tune2fs -c 0 -i 0 "$DDEST/$ROOTIMG" >> "$DDEST/.l1log"
		echo "ok"
	fi
fi

# Creacion imagen boot.img
echo -n "Creando y formateando $DDEST/BOOT.IMG... "
dd if=/dev/zero of="$DDEST/BOOT.IMG" bs=512 count=$(($P12-$P01)) 2>> "$DDEST/.l1log"
STATUS="$?"
sync
if [ $STATUS -ne 0 ]; then
	echo "X"
	echo "ERROR: Ha fallado $DDEST/BOOT.IMG, verifique el espacio libre"
	echo "	del sistema."
	exit 1
else
	mkfs.ext2 -F "$DDEST/BOOT.IMG" >> "$DDEST/.l1log" 2>&1
	tune2fs -c 0 -i 0 "$DDEST/BOOT.IMG" >> "$DDEST/.l1log"
	echo "ok"
fi

# Creacion imagen mnt.img
echo -n "Creando y formateando $DDEST/MNT.IMG... "
dd if=/dev/zero of="$DDEST/MNT.IMG" bs=512 count=$(($TT-$P23)) 2>> "$DDEST/.l1log"
STATUS="$?"
sync
if [ $STATUS -ne 0 ]; then
	echo "X"
	echo "ERROR: Ha fallado $DDEST/MNT.IMG, verifique el espacio libre"
	echo "	del sistema."
	exit 1
else
	mkfs.ext2 -F "$DDEST/MNT.IMG" >> "$DDEST/.l1log" 2>&1
	tune2fs -c 0 -i 0 "$DDEST/MNT.IMG" >> "$DDEST/.l1log"
	echo "ok"
fi

# Creacion imagen de 5MB para etc.img
echo -n "Creando y formateando $DDEST/ETC.IMG... "
dd if=/dev/zero of="$DDEST/ETC.IMG" bs=512 count=10000 2>> "$DDEST/.l1log"
STATUS="$?"
sync
if [ $STATUS -ne 0 ]; then
	echo "X"
	echo "ERROR: Ha fallado $DDEST/ETC.IMG, verifique el espacio libre"
	echo "	del sistema."
	exit 1
else
	mkfs.ext2 -F "$DDEST/ETC.IMG" >> "$DDEST/.l1log" 2>&1
	tune2fs -c 0 -i 0 "$DDEST/ETC.IMG" >> "$DDEST/.l1log"
	echo "ok"
fi

# Montaje de las unidades
if [ "$OPTION" == "rw" ]; then
	echo -n "Montando $DDEST/$ROOTIMG en $SYSROOT... "
	mount -o loop -t ext2 "$DDEST/$ROOTIMG" "$SYSROOT" >> "$DDEST/.l1log"
	STATUS="$?"
	if [ $STATUS -ne 0 ]; then
		echo "X"
		echo "ERROR: No se ha podido montar $DDEST/$ROOTIMG en $SYSROOT"
		exit 1
	else
		echo "ok"
	fi
fi
mkdir $SYSROOT/boot
mkdir $SYSROOT/mnt
mkdir $SYSROOT/etc
echo -n "Montando $DDEST/BOOT.IMG en $SYSROOT/boot... "
mount -o loop -t ext2 "$DDEST/BOOT.IMG" "$SYSROOT/boot" >> "$DDEST/.l1log"
STATUS="$?"
if [ $STATUS -ne 0 ]; then
	echo "X"
	echo "ERROR: No se ha podido montar $DDEST/BOOT.IMG en $SYSROOT/boot"
	exit 1
else
	echo "ok"
fi
echo -n "Montando $DDEST/MNT.IMG en $SYSROOT/mnt... "
mount -o loop -t ext2 "$DDEST/MNT.IMG" "$SYSROOT/mnt" >> "$DDEST/.l1log"
STATUS="$?"
if [ $STATUS -ne 0 ]; then
	echo "X"
	echo "ERROR: No se ha podido montar $DDEST/MNT.IMG en $SYSROOT/mnt"
	exit 1
else
	echo "ok"
fi
echo -n "Montando $DDEST/ETC.IMG en $SYSROOT/etc... "
mount -o loop -t ext2 "$DDEST/ETC.IMG" "$SYSROOT/etc" >> "$DDEST/.l1log"
STATUS="$?"
if [ $STATUS -ne 0 ]; then
	echo "X"
	echo "ERROR: No se ha podido montar $DDEST/ETC.IMG en $SYSROOT/etc"
	exit 1
else
	echo "ok"
fi

# Sincronizacion del sistema de archivos en las unidades montadas
# No nos importa las actualizaciones en el repositorio local (rsync -u)
echo -n "Sincronizando el sistema... "
#cp -Rpf $DORIG/ROOT/* $DDEST/ROOT/ >> "$DDEST/.l1log" 2>> "$DDEST/.l1log"
rsync --exclude="partimage/" --exclude=".git/" -va "$DORIG/" "$DDEST/" >> "$DDEST/.l1log" 2>> "$DDEST/.l1log"
#git-clone "$DORIG/" "$DDEST/." >> "$DDEST/.l1log" 2>> "$DDEST/.l1log"
STATUS="$?"
sync
if [ $STATUS -ne 0 ]; then
	echo "X"
	echo "ERROR: Ha fallado la copia de ficheros desde $DORIG/ROOT/ a $DDEST/ROOT/, "
	echo "	compruebe el espacio libre en las unidades y vuelva a intentarlo."
	exit 1
else
	echo "ok"
fi

echo "Ya se ha inicializado su imagen, puede comenzar a trabajar con ella."
echo "Recuerda que las unidades estan montadas, por lo que debera ejecutar
	el script $DDEST/desmontaje.sh o /projects/scripts/L1Imaginizar.sh para
	generar una nueva imagen."

exit 0
