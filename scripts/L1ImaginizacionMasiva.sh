#!/bin/bash

# Copyright (C) 2008
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published
# by the Free Software Foundation; version 2 only.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#

# GPL - Javier Muñoz Díaz
# Octubre 2008
#
# Se actualizan las imágenes de todos los terminales, ajustando la resolución de pantalla, y 
# recolocando los iconos, configurar altiris a pantalla completa
# Se crea una estructura en forma de arbol, siguiendo el esquema de los archivos del pendrive de maquetacion
# La idea es tener un archivo comprimido con las imagenes de todos los terminales, que sea facilmente 'volcable' dentro del pen, 
# con el script de instalación adecuado



# VARIABLES GLOBALES

# Rutas para cada uno de los tipos de terminal


DIRS="128_futros400_sis 256_futros400_sis 512_futros400_sis 128_futros300_sis 128_futros200_sis 128_futrob200_savage 128_hpt5515_radeon 256_hpt5710_radeon 512_hpt5725_sis 1024_hpt5735_radeonhd"
# Fecha de ejecucion: se utilizara para el nombre de las rutas
FECHA="`date +%Y%m%d%H%M%S`"
LETUSE=""
DEBUG="$1"


echo "s" > /tmp/yes
echo "n" > /tmp/no


# Preprocesado de opciones

pkg_setup() {

	# Comprobación de variable USE

	if [ "$USE" == "" ]; then
		echo "Variable USE no definida. Utilizando por defecto... "
		echo "LETUSE=\"letsas hp1320_lp wifi ntp_junta pluginflash complete pri_sev viaexpress primaria nolock\""
		echo "¿Continuar?(S/n)"
		read RESPUESTA
		if [ "$RESPUESTA" == 'n' ] || [ "$RESPUESTA" == 'N' ]; then
			echo "Por favor, indique unas nuevas variables de la forma: "
			echo "RUTA=\"...\" LETRES=\"...\" LETUSE=\"...\" $0 [--debug]"
			echo
			echo "Abortando..."
			exit 1
		elif [ "$RESPUESTA" == 'S' ] || [ "$RESPUESTA" == 's' ] || [ "$REPUESTA" == "" ]; then
			LETUSE="letsas hp1320_lp wifi ntp_junta pluginflash complete pri_sev viaexpress primaria nolock"
		else
			echo "Opción no válida"
			echo "Abortando..."
			exit 1
		fi
	else
		LETUSE=${USE}
	fi
	
	
	# Cálculo de la resolución de pantalla
		
	if [ "$LETRES" == "" ]; then
		echo "Resolución no fijada. Utilizando por defecto (800x600)"
		echo -n "¿Continuar?(S/n)"
		read RESPUESTA
		if [ "$RESPUESTA" == "S" ] || [ "$RESPUESTA" == 's' ] || [ "$RESPUESTA" == '' ]; then
			RES="800"
		elif [ "$RESPUESTA" == 'n' ] || [ "$RESPUESTA" =='N' ]; then
			echo
			echo "Elija la resolución deseada..."
			echo 
			echo "1. 800x600"
			echo "2. 1024x768"
			echo "3. 1280x1024"
			echo
			echo
			read RESPUESTA
			
			if [ "$RESPUESTA" == "1" ]; then
				RES="800"
			elif [ "$RESPUESTA" == "2" ]; then
				RES="1024"
			elif [ "$RESPUESTA" == "3" ]; then
				RES="1280"
			else
				echo "Error: respuesta no contemplada:"
				echo "RESPUESTA: ${RESPUESTA}"
				echo "Abortando ejecución..."
				exit 1
			fi
		else
			echo "Error: respuesta no válida:"
			echo "RESPUESTA: ${RESPUESTA}"
			echo "Abortando ejecución..."
			exit 1
		fi	
	fi
	
	# Calculo de la ruta original
	if [ "$RAIZ" == "" ]; then
		echo "Ruta raíz no encontrada. Se utilizará la ruta: "
		RAIZ="/tmp/LetSAS-$FECHA"
		echo "RAIZ=${RAIZ}"

	fi

	# Verificacion de debug
	if [ "$DEBUG" == "" ]; then
		echo
		echo "No se ha indicado la salida de debug. Si quiere activarla,"
		echo "llame al script con el parámetro '--debug'."
		echo
		echo "USO: RUTA=\"...\" LETRES=\"...\" USE=\"...\" $0 [--debug]"
	fi	
	
	echo
	echo
	echo -n "¿Continuar (S/n)?"
	read RESPUESTA
	if [ "$RESPUESTA" == "n" ] || [ "$RESPUESTA" == 'N' ] ; then
		echo "Abortando ejecución..."
		exit 1
		
	fi


	mkdir -p $RAIZ
}
	
pre_message() {

	clear
	echo "########################################################"
	echo
	echo "PROYECTO LETSAS"
	echo "Generación masiva de imágenes"
	echo "Octubre 2008"
	echo
	echo "#######################################################"
	echo
	echo
}

# Chequeo de variables recibidas

src_compile() {

	for dir in ${DIRS}; do
	
		echo "########################################"
	
		if [ ! -f $RAIZ/$dir ]; then
			echo "Eliminando datos anteriores..."
			/usr/sbin/L1Eliminar.sh $RAIZ/$dir < /tmp/yes && echo " ok"
		else
			echo "Directorio $RAIZ/$dir no existente"
		fi
		
		echo -n "Creando directorios..."
		mkdir -p $RAIZ/$dir && echo " ok"
		
		echo -n "Fijando memorias y drivers de video..."
		MEM=`echo $dir | awk -F'_' '{print $1}'` &&\
		LETDRIVER=`echo $dir | awk -F'_' '{print $3}'` &&\
		PATH_MODEL=`echo $dir | awk -F'_' '{print $2}'` &&\
		MODEL=`echo $PATH_MODEL | sed s/futro/""/ | sed s/hp/""/` &&\
		if [ $MODEL == "b200" ]; then
			MODEL='b220'
		fi &&\
	
		if [ $LETDRIVER == "radeon" ]; then
			VIDEO_DRIVER='ati'
		else
			VIDEO_DRIVER=$LETDRIVER
		fi &&\
		echo " ok"
		
	
	
		# LA RESOLUCION POR DEFECTO ES 800
	
		LETRES="$RES"
		LETDEPTH="24"
		ROOT="$RAIZ/$dir"
	
		# Variable use total
		if [[ "$MEM" == "128" ]]; then
			USE="${LETUSE}"
		fi

		if [[ "$MEM" == "256" ]]; then
			USE="${LETUSE} office"
		fi
		
		if [[ "$MEM" == "512" ]] || [[ "$MEM" == "1024" ]]; then
			USE="${LETUSE} office java sunjava python "
		fi

		USE="${MEM} ${VIDEO_DRIVER} ${MODEL} ${USE}"
		
		
		echo
		echo "#########################################"
		echo 
		echo "PARAMETROS DEL EQUIPO"
		echo "MODEL: $MODEL"
		echo "Memoria de sistema: $MEM"
		echo "LETDRIVER: $LETDRIVER"
		echo "VIDEO_DRIVER: $VIDEO_DRIVER"
		echo "LETRES: $LETRES"	
		echo "LETDEPTH: $LETDEPTH"
		echo "ROOT: $ROOT"
		echo "USE: $USE"
		echo 
		echo "#########################################"
	

		# Mostramos la salida
		echo -n "Compilando sistema base..."
		if [ $DEBUG=='--debug' ]; then
			ACCEPT_KEYWORDS="~x86" ROOT="${ROOT}" LETDRIVER="${LETDRIVER}" LETRES="${LETRES}" LETDEPTH="${LETDEPTH}" USE="${USE}" emerge -q lbaselayout < /tmp/yes
		else
			ACCEPT_KEYWORDS="~x86" ROOT="${ROOT}" LETDRIVER="${LETDRIVER}" LETRES="${LETRES}" LETDEPTH="${LETDEPTH}" USE="${USE}" emerge -q lbaselayout < /tmp/yes > /dev/null 2>/dev/null
		fi
		echo " ok"

		patching $ROOT $LETRES
	        cd $ROOT && /usr/sbin/L1Imaginizar.sh < /tmp/yes
		
	
		echo -n "Copiando imagen a la ruta por defecto..."
		mkdir -p "../images/$MEM/$PATH_MODEL" && cp FLASH.IMG.gz "../images/$MEM/$PATH_MODEL/" && echo " ok"
		

	#	echo -n "Desmontando discos virtuales..."
	#	sh ${ROOT}/desmontaje.sh && echo " ok"

		echo
		echo "IMAGEN FINALIZADA"
		echo

	done;
}

src_install(){

	echo -n "Comprimiendo arbol de imagenes..."
	cd $RAIZ && zip images-$FECHA -9 -r images && echo " ok"

	echo "Inicio del proceso: $FECHA"
	echo "Finalizacion del proceso: `date +%Y%m%d%H%M%S`"

#	echo -n "Subiendo imagen comprimida..."
#	cd $RAIZ && scp images-$FECHA.zip root@gm2:/repositorio/LeTSAS/IMAGEN-FLASH/ && echo " ok"
}



##########################################
# Mensajes postinstalacion

pkg_postinst() {

	echo
	echo
	echo "RESUMEN:"
	echo
	echo "Se ha generado el archivo comprimido $RAIZ/images-$FECHA.zip"
	echo "Este archivo contiene la estructura elemental de imagenes del pendrive instalación."
	echo
	echo "images\\"
	echo "      |128\\"
	echo "      |    \|futrob200"
	echo "      |     |futros200"
	echo "      |     |futros300"
	echo "      |     |futros400"
	echo "      |     |hpt5515"
	echo "      |"
	echo "      |256\\"
	echo "      |    \|futros400"
	echo "      |     |hpt5710"
	echo "      |"
	echo "      |512\\"
	echo "      |    \|hpt5725"
	echo "      |     |futros400"
	echo "      |1024\\"
	echo "            \|hpt5735"
	echo 
	echo
}


############################################
# Ajuste de parches

patching() {

	ROOT="$1"
	res="$2"
	
	#echo -n "Copiando archivo con la nueva posición de los iconos..."
	#cp "/home/javi/pb_Desktop.${res}" "${ROOT}/ROOT/opt/rox/rox.sourceforge.net/ROX-Filer/pb_Desktop" && echo "ok"
	
#	echo -n "Configuracion fullscreen..."
#	cp "/home/javi/Diraya Primaria.ini" "$ROOT/ROOT/mnt/opt.rw/ICAClient/config/" && echo " ok"
}

######################################3

pre_message 

pkg_setup

src_compile

src_install

pkg_postinst


exit 0

