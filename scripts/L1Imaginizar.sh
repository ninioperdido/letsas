#!/bin/bash

STATUS="0"

usage() {
	echo "Uso: $0 [-f] [<directorio imagen>]"
	echo ""
	echo "Directorio Imagen: Ruta de destino donde se inicio la imagen."
	echo ""
	echo "Ejemplo: $0 /repositorio/LeT-1.0-XX"
}

# $1 OPTION
# $2 ROOTIMG
# $3 DIR
montaje() {
	echo -n "Montando unidades... "
	if [ "$1" == "rw" ]; then
		if [ "$(mount | grep $3/$2)" = "" ]; then
			mount -o loop -t ext2 "$3/$2" "$SYSROOT" >> "$3/.l1log" 2>&1
			STATUS="$?"
			if [ $STATUS -ne 0 ]; then
				echo "X"
				echo "ERROR: Se ha intentado montar la unidad $3/$2 en $SYSROOT"
				echo "  y ha fallado, monta la unidad manualmente."
				exit 1
			fi
		fi
	fi
	if [ "$(mount | grep $3/BOOT.IMG)" = "" ]; then
		mount -o loop -t ext2 "$3/BOOT.IMG" "$SYSROOT/boot" >> "$3/.l1log" 2>&1
		STATUS="$?"
		if [ $STATUS -ne 0 ]; then
			echo "X"
			echo "ERROR: Se ha intentado montar la unidad $3/BOOT.IMG en $SYSROOT/boot"
			echo "  y ha fallado, monta la unidad manualmente."
			exit 1
		fi
	fi
	if [ "$(mount | grep $3/MNT.IMG)" = "" ]; then
		mount -o loop -t ext2 "$3/MNT.IMG" "$SYSROOT/mnt" >> "$3/.l1log" 2>&1
		STATUS="$?"
		if [ $STATUS -ne 0 ]; then
			echo "X"
			echo "ERROR: Se ha intentado montar la unidad $3/MNT.IMG en $SYSROOT/mnt"
			echo "  y ha fallado, monta la unidad manualmente."
			exit 1
		fi
	fi
	if [ "$(mount | grep $3/ETC.IMG)" = "" ]; then
		mount -o loop -t ext2 "$3/ETC.IMG" "$SYSROOT/etc" >> "$3/.l1log" 2>&1
		STATUS="$?"
		if [ $STATUS -ne 0 ]; then
			echo "X"
			echo "ERROR: Se ha intentado montar la unidad $3/ETC.IMG en $SYSROOT/etc"
			echo "  y ha fallado, monta la unidad manualmente."
			exit 1
		fi
	fi
	echo "ok"
}

if [ $# -gt 2 ]; then
	usage
	exit 1
fi

if [ "$1" = "-f" ]; then
	FORCE="true"
	IMGDIR="$2"
else
	FORCE="false"
	IMGDIR="$1"
fi

CONFIRM="N"
# Si no hay argumento intentalo con el directorio actual
if [ "$IMGDIR" = "" ]; then
	DIR=`pwd`
else
	DIR=`echo $IMGDIR | awk '{l=length($0); if(substr($0,l,1) == "/"){cadena=substr($0,1,l-1)}else{cadena=$0};print cadena}'`
fi

# Confirma el directorio a imaginizar si no fuerza la ejecucion
if [ "$FORCE" = "false" ]; then
	read -p "Imaginizar el directorio $DIR ? (s/N) " -t 20 CONFIRM
	if [ "$CONFIRM" != "s" -a "$CONFIRM" != "S" ]; then
		echo "No se hace nada, otra vez sera."
		exit 1
	fi
fi

# Existe realmente el directorio de trabajo?
if [ ! -d "$DIR" ]; then
	echo "ERROR: No existe el directorio de trabajo.
	Ejecute /projects/scripts/L1Inicializar.sh para crearlo."
	exit 1
fi

# Marca en el fichero de logs
echo L1IMAGIN: `date` >> "$DIR/.l1log"

echo -n "Recogiendo datos de inicializacion... "
if [ ! -f "$DIR/.l1init" ]; then
	echo "X"
	echo "ERROR: Parece que el directorio $DIR no es un directorio"
	echo "	de imagen valido. Debe crearlo con L1Inicializar.sh"
	exit 1
fi
SIZE=`sed -n "1p" "$DIR/.l1init" | awk '{print $1}'`
TT=`sed -n "2p" "$DIR/.l1init" | awk '{print $1}'`
CC=`sed -n "2p" "$DIR/.l1init" | awk '{print $2}'`
P01=`sed -n "2p" "$DIR/.l1init" | awk '{print $3}'`
P12=`sed -n "2p" "$DIR/.l1init" | awk '{print $4}'`
P23=`sed -n "2p" "$DIR/.l1init" | awk '{print $5}'`
SYSROOT=`sed -n "3p" "$DIR/.l1init" | awk '{print $1}'`
OPTION=`sed -n "4p" "$DIR/.l1init" | awk '{print $1}'`
echo "Longitud: $SIZE" >> "$DIR/.l1log"
echo "Cilindros: $CC" >> "$DIR/.l1log"
echo "TT: $TT" >> "$DIR/.l1log"
echo "P01: $P01" >> "$DIR/.l1log"
echo "P12: $P12" >> "$DIR/.l1log"
echo "P23: $P23" >> "$DIR/.l1log"
echo "SYSROOT: $SYSROOT" >> "$DIR/.l1log"
echo "OPTION: $OPTION" >> "$DIR/.l1log"
echo "ok"

case $OPTION in
	ro)
		ROOTIMG="ROOT.CRAMFS"
		;;
	rw)
		ROOTIMG="ROOT.IMG"
		;;
esac

# Montar unidades por si estuvieran desmontadas
montaje $OPTION $ROOTIMG $DIR

# Creacion del tarball con los dispositivos y copiarlo a /mnt/.restore/
echo -n "Creando restore de dispositivos... "
cd "$SYSROOT/dev"
tar zcfp "$DIR/dev.tgz" * >> "$DIR/.l1log" 2>&1
STATUS="$?"
sync
cd $DIR
if [ $STATUS -ne 0 ]; then
	echo "X"
	echo "ERROR: Ha fallado la creacion del tarball dev.tgz, compruebe la
	memoria libre del dispositivo."
	exit 1
else
	if [ ! -d  "$SYSROOT/mnt/.restore" ]; then
		mkdir "$SYSROOT/mnt/.restore" >> "$DIR/.l1log" 2>&1
		STATUS="$?"
		if [ $STATUS -ne 0 ]; then
			echo "X"
			echo "ERROR: No se ha podido crear el directorio $SYSROOT/mnt/.restore, "
			echo "	resuelva el problema y vuelva a intentarlo."
			exit 1
		fi
	fi
	cp -f "$DIR/dev.tgz" "$SYSROOT/mnt/.restore/dev.tgz" >> "$DIR/.l1log" 2>&1
	STATUS="$?"
	sync
	if [ $STATUS -ne 0 ]; then
		echo "X"
		echo "ERROR: Ha fallado la copia del tarball en $SYSROOT, "
		echo "	compruebe la memoria libre del dispositivo."
		exit 1
	fi
	rm -rf $SYSROOT/dev/* >> "$DIR/.l1log" 2>&1
	sync
	mknod -m 666 $SYSROOT/dev/hda b 3 0
	mknod -m 666 $SYSROOT/dev/hda1 b 3 1
	mknod -m 666 $SYSROOT/dev/hda2 b 3 2
	mknod -m 666 $SYSROOT/dev/hda3 b 3 3
	mknod -m 666 $SYSROOT/dev/hdb b 3 64
	mknod -m 666 $SYSROOT/dev/hdb1 b 3 65
	mknod -m 666 $SYSROOT/dev/hdb2 b 3 66
	mknod -m 666 $SYSROOT/dev/hdb3 b 3 67
	mknod -m 666 $SYSROOT/dev/hdc b 22 0
	mknod -m 666 $SYSROOT/dev/hdc1 b 22 1
	mknod -m 666 $SYSROOT/dev/hdc2 b 22 2
	mknod -m 666 $SYSROOT/dev/hdc3 b 22 3
	mknod -m 666 $SYSROOT/dev/sda b 8 0
	mknod -m 666 $SYSROOT/dev/sda1 b 8 1
	mknod -m 666 $SYSROOT/dev/sda2 b 8 2
	mknod -m 666 $SYSROOT/dev/sda3 b 8 3
	mknod -m 666 $SYSROOT/dev/fb0 c 29 0
	ln -sf fb0 $SYSROOT/dev/fb
	mknod -m 666 $SYSROOT/dev/fbsplash c 10 63
	mknod -m 666 $SYSROOT/dev/null c 1 3
	mknod -m 666 $SYSROOT/dev/tty c 5 0
	mknod -m 666 $SYSROOT/dev/tty0 c 4 0
	mknod -m 666 $SYSROOT/dev/tty1 c 4 1
	mknod -m 666 $SYSROOT/dev/tty2 c 4 2
	sync
fi

echo "ok"

# Desmontar ETC.IMG y copiarlo a /mnt/.restore/
echo -n "Creando restore de ETC.IMG... "
if [ "$(mount | grep $DIR/ETC.IMG)" != "" ]; then
	umount "$DIR/ETC.IMG" >> "$DIR/.l1log" 2>&1
	STATUS="$?"
	if [ $STATUS -ne 0 ]; then
		echo "X"
		echo "ERROR: Se ha intentado desmontar la unidad $DIR/ETC.IMG "
		echo "  y ha fallado, desmonta la unidad manualmente."
		exit 1
	fi
fi

# Enlace para splash cuando etc no esta montado
if [ ! -L "$SYSROOT/etc/splash" ]; then
	ln -sf "/splash" "$SYSROOT/etc/splash"
fi

cp -f "$DIR/ETC.IMG" "$SYSROOT/mnt/.restore/etc.img" >> "$DIR/.l1log" 2>&1
STATUS="$?"
sync
if [ $STATUS -ne 0 ]; then
	echo "X"
	echo "ERROR: Ha fallado la copia de ETC.IMG en $SYSROOT/mnt/.restore/,"
	echo " compruebe el espacio libre de la unidad $SYSROOT/mnt/."
	exit 1
fi

echo "ok"

# Desmontar el resto de unidades si estuvieran montadas
echo -n "Desmontando unidades... "
if [ "$(mount | grep $DIR/BOOT.IMG)" != "" ]; then
	umount "$DIR/BOOT.IMG" >> "$DIR/.l1log" 2>&1
	STATUS="$?"
	if [ $STATUS -ne 0 ]; then
		echo "X"
		echo "ERROR: Se ha intentado desmontar la unidad $DIR/BOOT.IMG "
		echo "  y ha fallado, desmonta la unidad manualmente."
		exit 1
	fi
fi
if [ "$(mount | grep $DIR/MNT.IMG)" != "" ]; then
	umount "$DIR/MNT.IMG" >> "$DIR/.l1log" 2>&1
	STATUS="$?"
	if [ $STATUS -ne 0 ]; then
		echo "X"
		echo "ERROR: Se ha intentado desmontar la unidad $DIR/MNT.IMG "
		echo "  y ha fallado, desmonta la unidad manualmente."
		exit 1
	fi
fi
if [ "$OPTION" == "rw" ]; then
	if [ "$(mount | grep $DIR/$ROOTIMG)" != "" ]; then
		umount "$DIR/$ROOTIMG" >> "$DIR/.l1log" 2>&1
		STATUS="$?"
		if [ $STATUS -ne 0 ]; then
			echo "X"
			echo "ERROR: Se ha intentado desmontar la unidad $DIR/$ROOTIMG "
			echo "  y ha fallado, desmonta la unidad manualmente."
			exit 1
		fi
	fi
fi
echo "ok"

#### TEST ####
#echo "### 5 seg para imaginizar ###"
#sleep 5s

# Crear imagen ROOT
if [ "$OPTION" == "ro" ]; then
	echo -n "Creando imagen $ROOTIMG... "
	mkcramfs "$DIR/ROOT" "$DIR/$ROOTIMG" >> "$DIR/.l1log" 2>&1
	STATUS="$?"
	sync
	if [ $STATUS -ne 0 ]; then
		echo "X"
		echo "ERROR: Ha fallado la generacion de la imagen $ROOTIMG, "
		echo "	solucione el problema y vuelva a intentarlo."
		exit 1
	fi
	echo "ok"
fi

# Verificacion tama�o de la imagen R ROOT
MAXSIZE=$(( $(($P23-$P12))*512 ))
REALSIZE=$(( `du -b "$DIR/$ROOTIMG" | awk '{print $1}'` ))
#echo ---
#echo :$P23:
#echo :$P12:
#echo :$MAXSIZE:
#echo :$REALSIZE:
#echo ---

if [ "${REALSIZE}" -le "${MAXSIZE}" ]; then

	# Incrustar composicion imagen FLASH.IMG
	echo -n "Componiendo imagen FLASH.IMG... MBR.IMG "
	dd if="$DIR/MBR.IMG" of="$DIR/FLASH.IMG" bs=512 >> "$DIR/.l1log" 2>&1
	STATUS="$?"
	sync
	if [ $STATUS -ne 0 ]; then
		echo "X"
		echo "ERROR: Ha fallado la copia de $DIR/MBR.IMG a $DIR/FLASH.IMG, "
		echo "	solucione el problema y vuelva a intentarlo."
		exit 1
	fi
	echo -n "BOOT.IMG "
	dd if="$DIR/BOOT.IMG" of="$DIR/FLASH.IMG" bs=512 seek=$P01 >> "$DIR/.l1log" 2>&1
	STATUS="$?"
	sync
	if [ $STATUS -ne 0 ]; then
		echo "X"
		echo "ERROR: Ha fallado la copia de $DIR/BOOT.IMG a $DIR/FLASH.IMG, "
		echo "	solucione el problema y vuelva a intentarlo."
		exit 1
	fi
	echo -n "$ROOTIMG "
	dd if="$DIR/$ROOTIMG" of="$DIR/FLASH.IMG" bs=512 seek=$P12 >> "$DIR/.l1log" 2>&1
	STATUS="$?"
	sync
	if [ $STATUS -ne 0 ]; then
		echo "X"
		echo "ERROR: Ha fallado la copia de $DIR/$ROOTIMG a $DIR/FLASH.IMG, "
		echo "	solucione el problema y vuelva a intentarlo."
		exit 1
	fi
	echo -n "MNT.IMG "
	dd if="$DIR/MNT.IMG" of="$DIR/FLASH.IMG" bs=512 seek=$P23 >> "$DIR/.l1log" 2>&1
	STATUS="$?"
	sync
	if [ $STATUS -ne 0 ]; then
		echo "X"
		echo "ERROR: Ha fallado la copia de $DIR/MNT.IMG a $DIR/FLASH.IMG, "
		echo "	solucione el problema y vuelva a intentarlo."
		exit 1
	fi

	echo "ok"

	if [ "$OPTION" == "ro" ]; then
		# Compresion de FLASH.IMG.gz
		echo -n "Comprimiendo imagen FLASH... "
		if [ -f "$DIR/FLASH.IMG.gz" ]; then
			rm -f "$DIR/FLASH.IMG.gz"
		fi
		gzip -v9 "$DIR/FLASH.IMG" >> "$DIR/.l1log" 2>&1
		STATUS="$?"
		sync
		if [ $STATUS -ne 0 ]; then
			echo "X"
			echo "ERROR: Ha fallado la compresion del fichero $DIR/FLASH.IMG, "
			echo "	solucione el problema y vuelva a intentarlo."
			exit 1
		fi

		echo "ok"
	fi
else
	echo "ERROR: La imagen creada es mayor que la particion $ROOTIMG, "
	echo "	solucione el problema y vuelva a intentarlo."
fi

### Restablecer imagen

# Montar unidades
montaje $OPTION $ROOTIMG $DIR

# Restaurar devices desde dev.tgz
echo -n "Restaurando devices... "
if [ ! -f "$DIR/dev.tgz" ]; then
	echo "X"
	echo "ERROR: No existe el fichero $DIR/dev.tgz a restaurar,"
	echo "	solucione el problema y vuelva a intentarlo."
	exit 1
fi
rm -f $SYSROOT/dev/*
sync
tar zxfp "$DIR/dev.tgz" -C "$SYSROOT/dev/" >> "$DIR/.l1log" 2>&1
STATUS="$?"
sync
if [ $STATUS -ne 0 ]; then
	echo "X"
	echo "ERROR: Ha fallado la restauracion de dispositivos $DIR/dev.tgz en $SYSROOT/etc,"
	echo "	solucione el problema y vuelva a intentarlo."
	exit 1
fi
echo "ok"
rm -f $SYSROOT/mnt/.restore/* >> "$DIR/.l1log" 2>&1
STATUS="$?"
sync
if [ $STATUS -ne 0 ]; then
	echo "AVISO: Ha ocurrido un fallo al eliminar el contenido del directorio"
	echo "	$SYSROOT/mnt/.restore/, no es critico pero deberia revisar este problema."
fi

echo "FIN: Ya puede continuar editando la imagen."
echo "	Cuando finalice puede volver a ejecutar $0."
exit 0
