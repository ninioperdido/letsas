#!/bin/sh

# CopyLeft (C) 2009 Laura Garcia Liebana - Equipo LeTSAS
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published
# by the Free Software Foundation; version 2 only.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#

#DISKS="128 256 512 1024"
DISKS="512"

#MODELS_128="futrob200 futros200 futros300 futros400 hpt5515"
#MODELS_256="hpt5710 futros400"
#MODELS_512="hpt5725 futros400"
MODELS_512="futros400"
#MODELS_1024="hpt5735"

VDRIVER_futrob200="savage"
VDRIVER_futros200="sis"
VDRIVER_futros300="sis"
VDRIVER_futros400="sis"
VDRIVER_hpt5515="radeon"
VDRIVER_hpt5710="radeon"
VDRIVER_hpt5725="sis"
VDRIVER_hpt5735="radeonhd"

USE_128="128"
USE_256="256 java office sunjava1.4.2"
USE_512="512 java office sunjava1.4.2"
USE_1024="1024 java office sunjava"

USE_futrob200="savage"
USE_futros200="sis"
USE_futros300="sis"
USE_futros400="sis"
USE_hpt5515="ati"
USE_hpt5710="ati"
USE_hpt5725="sis "
USE_hpt5735="radeonhd"

if [ "${DATE}" = "" ]; then
	DATE="`date +%Y%m%d`"
fi
SOURCEDIR="/tmp/MOU-${DATE}"

if [ "${IMGNAME}" = "" ]; then
	IMGNAME="FLASH"
fi
echo Nombre de Masters IMGNAME=${IMGNAME}

#if [ -d "${SOURCEDIR}" ]; then
#	rm -rf "${SOURCEDIR}"
#fi
echo Ruta de trabajo SOURCEDIR=${SOURCEDIR}

if [ "${ACCEPT_KEYWORDS}" = "" ]; then
	ACCEPT_KEYWORDS="~x86"
fi
echo Version ACCEPT_KEYWORDS=${ACCEPT_KEYWORDS}

if [ "${LETRES}" = "" ]; then
	LETRES="800"
fi
echo Resolucion LETRES=${LETRES}

echo Parametros USE adicionales USE=${USE}

echo "--- Masters Of Universe - Try 1 ---"

USEBASE="force complete letsas wifi pluginflash samba python ntp_junta ${USE}"

### Try it
for DISK in `echo ${DISKS}`; do
	eval CURMODELS='$MODELS_'$DISK
#	echo ${CURMODELS}
	for MODEL in `echo ${CURMODELS}`; do
		eval CURUSEDISK='$USE_'$DISK
		eval CURUSEMODEL='$USE_'$MODEL
		eval CURVDRIVER='$VDRIVER_'$MODEL
		BROOT="${SOURCEDIR}/builds/${DISK}_${MODEL}"
		IROOT="${SOURCEDIR}/${DISK}/${MODEL}"
		USE="${USEBASE} ${MODEL} ${CURUSEMODEL} ${CURUSEDISK}"
		echo 'ROOT="'$BROOT'"' 'ACCEPT_KEYWORDS="'$ACCEPT_KEYWORDS'"' 'LETDRIVER="'$CURVDRIVER'"' 'LETRES="'$LETRES'"' 'USE="'$USE'"' emerge -atv lbaselayout
		echo cp "${BROOT}/FLASH.IMG.gz" "${IROOT}/${IMGNAME}.IMG.gz"
	done
done

echo "--- Masters Of Universe - Do it! ---"

read -p "Quieres construir los Masters? (s/N) " -t 20 CONFIRM
if [ "$CONFIRM" != "s" -a "$CONFIRM" != "S" ]; then
	echo "You Loose! Bye."
	exit 1
fi

echo "Be water my friend!"
### Do it!
mkdir -p "${SOURCEDIR}"
for DISK in `echo ${DISKS}`; do
	eval CURMODELS='$MODELS_'$DISK
#	echo ${CURMODELS}
	for MODEL in `echo ${CURMODELS}`; do
		eval CURUSEDISK='$USE_'$DISK
		eval CURUSEMODEL='$USE_'$MODEL
		eval CURVDRIVER='$VDRIVER_'$MODEL
		BROOT="${SOURCEDIR}/builds/${DISK}_${MODEL}"
		IROOT="${SOURCEDIR}/${DISK}/${MODEL}"
		echo mkdir -p "${BROOT}" >> "${SOURCEDIR}/mastersofuniverse.log"
		mkdir -p "${BROOT}"
		echo mkdir -p "${IROOT}" >> "${SOURCEDIR}/mastersofuniverse.log"
		mkdir -p "${IROOT}"
		USE="${USEBASE} ${MODEL} ${CURUSEMODEL} ${CURUSEDISK}"
		echo 'ROOT="'$BROOT'"' 'ACCEPT_KEYWORDS="'$ACCEPT_KEYWORDS'"' 'LETDRIVER="'$CURVDRIVER'"' 'LETRES="'$LETRES'"' 'USE="'$USE'"' emerge -atv lbaselayout
		echo 'ROOT="'$BROOT'"' 'ACCEPT_KEYWORDS="'$ACCEPT_KEYWORDS'"' 'LETDRIVER="'$CURVDRIVER'"' 'LETRES="'$LETRES'"' 'USE="'$USE'"' emerge -atv lbaselayout >> "${SOURCEDIR}/mastersofuniverse.log"
		ROOT="$BROOT" ACCEPT_KEYWORDS="$ACCEPT_KEYWORDS" LETDRIVER="$CURVDRIVER" LETRES="$LETRES" USE="$USE" emerge -v lbaselayout
		# Establece fecha en version
		VIMG="`cat ${BROOT}/ROOT/etc/sysconfig/image.id`_${DATE}"
		echo "${VIMG}" > "${BROOT}/ROOT/etc/sysconfig/image.id"
		echo L1Imaginizar.sh -f ${BROOT} >> "${SOURCEDIR}/mastersofuniverse.log"
		L1Imaginizar.sh -f ${BROOT}
		echo cp "${BROOT}/FLASH.IMG.gz" "${IROOT}/${IMGNAME}.IMG.gz" >> "${SOURCEDIR}/mastersofuniverse.log"
		cp "${BROOT}/FLASH.IMG.gz" "${IROOT}/${IMGNAME}.IMG.gz"
		sync
		echo L1Eliminar.sh -f ${BROOT} >> "${SOURCEDIR}/mastersofuniverse.log"
		L1Eliminar.sh -f ${BROOT}
	done
done

exit 0

