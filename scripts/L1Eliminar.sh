#!/bin/bash

STATUS="0"

usage() {
	echo "Uso: $0 [-f] [<directorio imagen>]"
	echo ""
	echo "Directorio Imagen: Directorio imagen a eliminar."
	echo ""
	echo "Ejemplo: $0 /repositorio/LeT-1.0-XX"
}

if [ $# -gt 2 ]; then
	usage
	exit 1
fi

if [ "$1" = "-f" ]; then
	FORCE="true"
	IMGDIR="$2"
else
	FORCE="false"
	IMGDIR="$1"
fi

CONFIRM="N"
DIR=`echo $IMGDIR | awk '{l=length($0); if(substr($0,l,1) == "/"){cadena=substr($0,1,l-1)}else{cadena=$0};print cadena}'`
ADIR=`pwd`
# Comprueba que el directorio a eliminar no pertenece al actual
if [ "`echo $ADIR | grep $DIR`" != "" ]; then
	echo "ERROR: Ahora estas en el directorio $ADIR,"
	echo "	debes salirte del directorio de imagen a eliminar."
	exit 1
fi

# Confirma el directorio a imaginizar si no fuerza la ejecucion
if [ "$FORCE" = "false" ]; then
	read -p "ELIMINAR el directorio imagen $DIR ? (s/N) " -t 20 CONFIRM
	if [ "$CONFIRM" != "s" -a "$CONFIRM" != "S" ]; then
		echo "No se hace nada, otra vez sera."
		exit 1
	fi
fi

# Existe realmente el directorio de trabajo?
if [ ! -d "$DIR" ]; then
	echo "ERROR: No existe el directorio de trabajo."
	echo "	No se hace nada."
	exit 1
fi

# Desmontar unidades
echo -n "Desmontando unidades... "
if [ "$(mount | grep $DIR/ETC.IMG)" != "" ]; then
	umount "$DIR/ETC.IMG"
	STATUS="$?"
	if [ $STATUS -ne 0 ]; then
		echo "X"
		echo "ERROR: Se ha intentado desmontar la unidad $DIR/ETC.IMG "
		echo "	y ha fallado, desmonta la unidad manualmente."
		exit 1
	fi
fi
if [ "$(mount | grep $DIR/BOOT.IMG)" != "" ]; then
	umount "$DIR/BOOT.IMG"
	STATUS="$?"
	if [ $STATUS -ne 0 ]; then
		echo "X"
		echo "ERROR: Se ha intentado desmontar la unidad $DIR/BOOT.IMG "
		echo "	y ha fallado, desmonta la unidad manualmente."
		exit 1
	fi
fi
if [ "$(mount | grep $DIR/MNT.IMG)" != "" ]; then
	umount "$DIR/MNT.IMG"
	STATUS="$?"
	if [ $STATUS -ne 0 ]; then
		echo "X"
		echo "ERROR: Se ha intentado desmontar la unidad $DIR/MNT.IMG "
		echo "	y ha fallado, desmonta la unidad manualmente."
		exit 1
	fi
fi
echo "ok"

# Eliminar directorio imagen
echo -n "Eliminando contenido... "
rm -rf "$DIR"
STATUS="$?"
sync
if [ $STATUS -ne 0 ]; then
	echo "X"
	echo "ERROR: Ha ocurrido un error eliminando el directorio imagen,"
	echo "	compruebalo por favor."
	exit 1
fi
echo "ok"

echo "FIN: Se ha eliminado correctamente el directorio imagen."
exit 0
