#!/bin/sh
#    LeTSAS Minimal target creator
#    This program make a DEST directory with the contents needed to run 
#    a minimal distribution of LeTSAS (funtoo / metro derived).
#
#    Copyright (C) 2013 Gerardo Puerta Cardelles <gerardo.puerta@juntadeandalucia.es>
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

## General Variables
N=$'\x1b[0;0m'
B=$'\x1b[0;01m'
R=$'\x1b[31;01m'
G=$'\x1b[00;32m'
B=$'\x1b[34;01m'


## Config variables
CBUILD="i586-pc-linux-uclibc"
BOOT_PACKAGE="/root/letsas_boot_package.tar.xz"
KERNEL_CONFIG="/root/kernel_config"
SYSROOT_SYSTEM="busybox libiconv sys-libs/zlib =dev-lang/python-2.7.5-r3 xcb-proto bigreqsproto compositeproto randrproto resourceproto scrnsaverproto videoproto xcmiscproto xextproto xf86dgaproto xineramaproto libXft xorg-server"
BASE_SYSTEM="baselayout cross-i586-pc-linux-uclibc/uclibc mingetty busybox libiconv sys-libs/zlib xinetd dropbear"
TARGET=""
EMERGE=""
KERNEL_DIR=""
USE="uclibc crossdev xvfb xcomposite -ssl -udev -suid -cups -uuid"
#MIDORI
USE="${USE} -geoloc -gstreamer -introspection -webgl"
INPUT_DEVICES="keyboard mouse"
VIDEO_CARDS="fbdev" 
DIST_PACKAGES="/root/letsas_world"
ICA_CLIENT="icaclient_13.0.0.256735_i386.deb"
COLLECTD_PLUGINS="snmp"
SLIM_THEME="/root/slim_theme"

## Check for superuser access
if [ "$UID" -ne  0 ]
then
        echo "Error! You are not a superuser."
        exit 1
fi

## Argument proccesing
while getopts ":t:e:b:k:c:w:d" optname
  do
    case "$optname" in
      "t")
        TARGET="$OPTARG"
        ;;
      "e")
        EMERGE="$OPTARG"
        ;;
      "b")
        KERNEL_CONFIG="$OPTARG"
        ;;
      "C")
        CBUILD="$OPTARG"
        ;;
      "b")
        BOOT_PACKAGE="$OPTARG"
        ;;
      "w")
        DIST_PACKAGES="$OPTARG"
        ;;
      "d")
        echo -n "I am going to remove existing TARGET, are you sure? (${G}Y${N}/${R}N${N})? "
        read DELETE
	;;
      "?")
        echo "Unknown option $OPTARG"
        ;;
      *)
      # Should not occur
        echo "Unknown error while processing options"
        ;;
    esac
done

# Set default target
if [ "$TARGET" == "" ]
then
  TARGET="./letsas_${CBUILD}_`date +%F`"
fi
PWD="readlink -f ${TARGET}"
LDFLAGS="${LDFLAGS} -L${PWD}/lib -L${PWD}/usr/lib"

# Remove existing target
if [ "${DELETE}" == "Y" ] || [ "${DELETE}" == "y" ]
then
  rm -rf ${TARGET}
fi

if [ ! -d ${TARGET} ]
then
  echo "Creating target directories"
  mkdir -p "${TARGET}"/{boot,dev,proc,root,sys,etc,usr}
  mkdir -p "${TARGET}"/var/{lock,log}
  mkdir -p "${TARGET}"/usr/local
  mkdir -p "${TARGET}"/etc/init.d
  mkdir -p "${TARGET}"/usr/src/linux
  KERNEL_DIR="${TARGET}/usr/src/linux"

  echo "Writting /etc/init.d/rcS ..."
  cat << END > ${TARGET}/etc/init.d/rcS
#!/bin/ash
mount -o remount,rw /
mount -t devtmpfs none /dev
mount -t proc none /proc
mount -t sysfs none /sys
mkdir /dev/pts
mount -t devpts none /dev/pts
xinetd
dropbear
END

  echo "Addind +x to rcS ..."
  chmod +x ${TARGET}/etc/init.d/rcS

  echo "Writting /etc/inittab ..."
  cat << END > ${TARGET}/etc/inittab
::sysinit:/etc/init.d/rcS
::respawn:/usr/bin/slim
::restart:/sbin/reboot
#::respawn:/sbin/mingetty --noclear tty1
END

  echo "Touching /etc/fstab ..."
  touch ${TARGET}/etc/fstab

  echo "Writting /etc/mtab ..."
  cat << END > ${TARGET}/etc/mtab
rootfs / rootfs 0 0
END

  echo "Touching /var/log/lastlog ..."
  touch ${TARGET}/var/log/lastlog

  echo "Touching /var/log/wtmp ..."
  touch ${TARGET}/var/log/wtmp

  echo "Touching /etc/resolv.conf ..."
  touch ${TARGET}/etc/resolv.conf

  echo "Setting boot environment ..."
  tar xpf ${BOOT_PACKAGE} -C ${TARGET}/

  echo "Copying kernel configuration ..."
  cp ${KERNEL_CONFIG} ${KERNEL_DIR}/.config
 
  echo "Emerging sysroot system ..."
  KERNEL_DIR="${KERNEL_DIR}" \
  USE="${USE}" \
  ROOT="/usr/${CBUILD}/" \
  INPUT_DEVICES="${INPUT_DEVICES}" \
  VIDEO_CARDS="${VIDEO_CARDS}" \
  ${CBUILD}-emerge -uDN --usepkg --binpkg-respect-use=y $SYSROOT_SYSTEM

  echo "Emerging base system ..."
  KERNEL_DIR="${KERNEL_DIR}" \
  USE="${USE}" \
  ROOT="${TARGET}" \
  INPUT_DEVICES="${INPUT_DEVICES}" \
  VIDEO_CARDS="${VIDEO_CARDS}" \
  LDFLAGS="${LDFLAGS}" \
  ${CBUILD}-emerge -uDN --usepkg --with-bdeps=n --binpkg-respect-use=y $BASE_SYSTEM

  echo "Changing baselayout passwd to ash shell ..."
  sed -ie 's/\/bin\/bash/\/bin\/ash/' ${TARGET}/etc/passwd
  echo "Setting ash shell as valid shell ..."
  echo "/bin/ash" >> ${TARGET}/etc/shells

  echo "Copying libgcc ..."
  cp /usr/lib/gcc/${CBUILD}/*/libgcc_s.so.1 "${TARGET}/usr/lib"
  cp /usr/lib/gcc/${CBUILD}/*/libgcc_s.so.1 "/usr/${CBUILD}/usr/lib"
  cp /usr/lib/gcc/${CBUILD}/*/libstdc++*.so* "${TARGET}/usr/lib"
  cp /usr/lib/gcc/${CBUILD}/*/libstdc++*.so* "/usr/${CBUILD}/usr/lib"
  
  echo "Copying dgaproc.h ..."
  mkdir -p ${TARGET}/usr/include/xorg
  cp /usr/include/xorg/dgaproc.h ${TARGET}/usr/include/xorg/
  
  echo "Linking install ..."
  ln -sf /usr/bin/install /usr/${CBUILD}/usr/bin/


  echo "Setting uClibc timezone info ..."
  echo "CET-1CEST-2,M3.5.0/02:00:00,M10.5.0/03:00:00" >> "${TARGET}/etc/TZ"
  
  echo "Copying /dev/urandom ..."
  cp -a /dev/urandom "${TARGET}/dev/urandom"
  echo "Generating RSA host key ..."
  mkdir ${TARGET}/etc/dropbear
  chroot "${TARGET}" /bin/ash -c "/usr/bin/dropbearkey -t rsa -f /etc/dropbear/dropbear_rsa_host_key"
  echo "Generating DSS host key ..."
  chroot "${TARGET}" /bin/ash -c "/usr/bin/dropbearkey -t dss -f /etc/dropbear/dropbear_dss_host_key"
  echo "Deleting /dev/urandom ..."
  rm "${TARGET}/dev/urandom"

  echo "Setting xinetd for ssh server ..."
  cat << END > ${TARGET}/etc/xinetd.d/dropbear
service ssh
{
	socket_type	= stream
	wait		= no
	user		= root
	protocol	= tcp
	server		= /usr/sbin/dropbear
	server_args	= -i -g
	disable		= no
}
END
  
  echo "Setting xinetd for vnc server ..."
  cat << END > ${TARGET}/etc/xinetd.d/x11vnc
service x11vnc
{
	socket_type	= stream
	type		= UNLISTED
	wait		= no
	user		= root
	protocol	= tcp
	port		= 5900
	server		= /usr/bin/x11vnc
	server_args	= -inetd -o /var/log/x11vnc.log -display :0 -auth /var/run/slim.auth -forever -bg -rfbauth /etc/x11vnc_passwd
	disable		= no
}
END

  WORLD=""
  echo "Emerging world packages ..."
  while read line
  do
    WORLD="${WORLD} ${line}"
  done < ${DIST_PACKAGES} 
  INPUT_DEVICES="${INPUT_DEVICES}" \
  VIDEO_CARDS="${VIDEO_CARDS}" \
  KERNEL_DIR="${KERNEL_DIR}" \
  USE="${USE}" \
  ROOT="/usr/${CBUILD}" \
  LDFLAGS="${LDFLAGS}" \
  ${CBUILD}-emerge -uvDNt --with-bdeps=n --usepkg --binpkg-respect-use=y ${WORLD}
 INPUT_DEVICES="${INPUT_DEVICES}" \
  VIDEO_CARDS="${VIDEO_CARDS}" \
  KERNEL_DIR="${KERNEL_DIR}" \
  USE="${USE}" \
  ROOT="${TARGET}" \
  LDFLAGS="${LDFLAGS}" \
  ${CBUILD}-emerge -uvDNt --with-bdeps=n --usepkg --binpkg-respect-use=y ${WORLD}

  echo "Setting .xinitrc for root"
  cat << END > ${TARGET}/root/.xinitrc
jwm
END

  echo "Installing ICA client ..."
  mkdir /usr/"${CBUILD}"/icaclient
  cp "${ICA_CLIENT}" /usr/${CBUILD}/icaclient/
  cd /usr/"${CBUILD}"/icaclient/
  ar x "${ICA_CLIENT}"
  tar xvf data.tar.gz -C "${TARGET}"/

  echo "Installing slim theme ..."
  cp -rp ${SLIM_THEME}/{background.jpg,panel.png,slim.theme} \
  ${TARGET}/usr/share/slim/themes/default/

  echo "Copying script to target ..."
  sed -n '/#######/,$p' "$0" | sed -n '/bin\/ash/,$p' > "${TARGET}/usr/local/${0#./}"
  chmod 700 "${TARGET}/usr/local/${0#./}"
  
  echo "Entering target ..."
  chroot ${TARGET} /usr/local/${0#./}

  echo "Back from target ..."
fi

if [ "$EMERGE" != "" ]
then
  echo "Emerging packages in SYSROOT ..."
  USE="${USE}" \
  INPUT_DEVICES="${INPUT_DEVICES}" \
  VIDEO_CARDS="${VIDEO_CARDS}" \
  KERNEL_DIR="${KERNEL_DIR}" \
  ROOT="/usr/${CBUILD}" ${CBUILD}-emerge -uavtDN --with-bdeps=y --usepkg --binpkg-respect-use=y $EMERGE
  if [ $? == 0 ] 
  then
    echo "Emerging packages in TARGET ..."
    USE="${USE}" \
    INPUT_DEVICES="${INPUT_DEVICES}" \
    VIDEO_CARDS="${VIDEO_CARDS}" \
    KERNEL_DIR="${KERNEL_DIR}" \
    LDFLAGS="${LDFLAGS}" \
    ROOT="${TARGET}" ${CBUILD}-emerge -uvDN --with-bdeps=y --usepkg --binpkg-respect-use=y $EMERGE
  fi

else
  echo "Nothing more to emerge (Did you remember to use -e something?)"
fi

exit

############################ Do not modify the content of this and the following line! ############################
#!/bin/ash

PASSWORD="Passw0rd"
echo "Setting root password to ${PASSWORD}"
echo "root:${PASSWORD}" | chpasswd

echo "Setting vnc password to ${PASSWORD}"
x11vnc -storepasswd ${PASSWORD} /etc/x11vnc_passwd

exit

